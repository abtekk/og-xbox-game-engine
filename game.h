/***************************************************************************/
/*                                                                         */
/*  game.h                                                                 */
/*                                                                         */
/***************************************************************************/

/***************************************************************************/
/*                                                                         */
/*  File: game.h                                                           */
/*                                                                         */
/*  Details: Game Start Code                                               */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/


#ifndef _XF_GAME_H
#define _XF_GAME_H

// You don't need cube.h or gamepad.h or camera.h as we dont' use them in this
// example...but I've left them in anyhow :)
#include "code/graphics/graphics.h"
#include "code/cube/cube.h"
#include "code/gamepad/gamepad.h"
#include "code/camera/camera.h"

#include "code/text/text.h"
#include "code/timer/timer.h"

#include "code/object/object.h"

#include "code/xloader/xloader.h"
#include "code/screenshot/screenshot.h"

#include "code/bullet/bullet.h"
#include "code/collisionreaction/collisionreaction.h"
#include "code/sprite/sprite.h"
#include "code/graphics_helpers/graphics_helpers.h"

#include "code/light/light.h"

#include "code/world/world.h"

// need this for swprintf function
#include <stdio.h>


class CGame
{
protected:
	// Lets create an instance of our CGraphics class!...m for member, and
	// I chose g for graphics.....not a very descriptive name but I like it.
	CGraphics m_g; 

	CCamera m_camera;

	CGamePad m_gamepad;

	// Simple timer
	CTimer timerA;

	// not forgetting "D:\\media\\font\\Arial18Normal.bmf"
	CText textA;
	CText textB;

	// not forgetting "D:\\media\\model\\warrior.x"
	CMesh      mymesh;
	CObject    myobject;
	CAnimation myanimation;

	CXLoader   m_world;

	IDirect3DDevice8 *m_pD3DDevice;


	CText textFPS;
	CTimer timerFPS;

	CBullet	   m_Bullet;

	CSprite    m_explosion;

	CLight     m_Light;

	CWorld     m_OctreeWorld;

public:
	// Initilisation called only once in the constructor
	CGame();

	// GameLoop, gets called repeatedly while it returns 1;
	bool GameLoop();

	// Cleanup process, all done in our destructor
	~CGame();
};


#endif // _XF_GAME_H