/***************************************************************************/
/*                                                                         */
/*  File: debug.h                                                          */
/*                                                                         */
/*  Details: Debugging Code                                                */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#ifndef _XF_DEBUG_H
#define _XF_DEBUG_H


#include <stdio.h>  // used for fopen, fprintf and fclose


void debug(char *str);


#endif // _XF_DEBUG_H

