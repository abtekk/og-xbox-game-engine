# Microsoft Developer Studio Project File - Name="code" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Xbox Application" 0x0b01

CFG=code - Xbox Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "code.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "code.mak" CFG="code - Xbox Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "code - Xbox Release" (based on "Xbox Application")
!MESSAGE "code - Xbox Debug" (based on "Xbox Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe

!IF  "$(CFG)" == "code - Xbox Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "_XBOX" /D "NDEBUG" /YX /FD /G6 /Ztmp /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "_XBOX" /D "NDEBUG" /YX /FD /G6 /Ztmp /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 xapilib.lib d3d8.lib d3dx8.lib xgraphics.lib dsound.lib dmusic.lib xnet.lib xboxkrnl.lib /nologo /machine:I386 /subsystem:xbox /fixed:no /TMP /OPT:REF
# ADD LINK32 xapilib.lib d3d8.lib d3dx8.lib xgraphics.lib dsound.lib dmusic.lib xnet.lib xboxkrnl.lib /nologo /machine:I386 /subsystem:xbox /fixed:no /TMP /OPT:REF
XBE=imagebld.exe
# ADD BASE XBE /nologo /stack:0x10000
# ADD XBE /nologo /stack:0x10000
XBCP=xbecopy.exe
# ADD BASE XBCP /NOLOGO
# ADD XBCP /NOLOGO

!ELSEIF  "$(CFG)" == "code - Xbox Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_XBOX" /D "_DEBUG" /YX /FD /G6 /Ztmp /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_XBOX" /D "_DEBUG" /FR /YX /FD /G6 /Ztmp /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 xapilibd.lib d3d8d.lib d3dx8d.lib xgraphicsd.lib dsoundd.lib dmusicd.lib xnetd.lib xboxkrnl.lib /nologo /incremental:no /debug /machine:I386 /subsystem:xbox /fixed:no /TMP
# ADD LINK32 xapilibd.lib d3d8d.lib d3dx8d.lib xgraphicsd.lib dsoundd.lib dmusicd.lib xnetd.lib xboxkrnl.lib /nologo /incremental:no /debug /machine:I386 /subsystem:xbox /fixed:no /TMP
XBE=imagebld.exe
# ADD BASE XBE /nologo /stack:0x10000 /debug
# ADD XBE /nologo /stack:0x10000 /debug
XBCP=xbecopy.exe
# ADD BASE XBCP /NOLOGO
# ADD XBCP /NOLOGO

!ENDIF 

# Begin Target

# Name "code - Xbox Release"
# Name "code - Xbox Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\code\3ds\3ds.cpp
# End Source File
# Begin Source File

SOURCE=.\code\bullet\bullet.cpp
# End Source File
# Begin Source File

SOURCE=.\code\camera\camera.cpp
# End Source File
# Begin Source File

SOURCE=.\code\collision\collision.cpp
# End Source File
# Begin Source File

SOURCE=.\code\collision\collision_functions.cpp
# End Source File
# Begin Source File

SOURCE=.\code\collisionreaction\collisionreaction.cpp
# End Source File
# Begin Source File

SOURCE=.\code\cube\cube.cpp
# End Source File
# Begin Source File

SOURCE=.\debug.cpp
# End Source File
# Begin Source File

SOURCE=.\game.cpp
# End Source File
# Begin Source File

SOURCE=.\code\gamepad\gamepad.cpp
# End Source File
# Begin Source File

SOURCE=.\code\graphics\graphics.cpp
# End Source File
# Begin Source File

SOURCE=.\code\graphics_helpers\graphics_helpers.cpp
# End Source File
# Begin Source File

SOURCE=.\code\light\light.cpp
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# Begin Source File

SOURCE=.\code\object\object.cpp
# End Source File
# Begin Source File

SOURCE=.\code\octree\octree.cpp
# End Source File
# Begin Source File

SOURCE=.\code\screenshot\screenshot.cpp
# End Source File
# Begin Source File

SOURCE=.\code\sprite\sprite.cpp
# End Source File
# Begin Source File

SOURCE=.\code\text\text.cpp
# End Source File
# Begin Source File

SOURCE=.\code\timer\timer.cpp
# End Source File
# Begin Source File

SOURCE=.\code\world\world.cpp
# End Source File
# Begin Source File

SOURCE=.\code\xloader\xloader.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\code\3ds\3ds.h
# End Source File
# Begin Source File

SOURCE=.\code\bullet\bullet.h
# End Source File
# Begin Source File

SOURCE=.\code\camera\camera.h
# End Source File
# Begin Source File

SOURCE=.\code\collision\collision.h
# End Source File
# Begin Source File

SOURCE=.\code\collision\collision_functions.h
# End Source File
# Begin Source File

SOURCE=.\code\collisionreaction\collisionreaction.h
# End Source File
# Begin Source File

SOURCE=.\code\cube\cube.h
# End Source File
# Begin Source File

SOURCE=.\debug.h
# End Source File
# Begin Source File

SOURCE=.\game.h
# End Source File
# Begin Source File

SOURCE=.\code\gamepad\gamepad.h
# End Source File
# Begin Source File

SOURCE=.\code\graphics\graphics.h
# End Source File
# Begin Source File

SOURCE=.\code\graphics_helpers\graphics_helpers.h
# End Source File
# Begin Source File

SOURCE=.\code\light\light.h
# End Source File
# Begin Source File

SOURCE=.\main.h
# End Source File
# Begin Source File

SOURCE=.\code\object\object.h
# End Source File
# Begin Source File

SOURCE=.\code\octree\octree.h
# End Source File
# Begin Source File

SOURCE=.\code\screenshot\screenshot.h
# End Source File
# Begin Source File

SOURCE=.\code\sprite\sprite.h
# End Source File
# Begin Source File

SOURCE=.\code\text\text.h
# End Source File
# Begin Source File

SOURCE=.\code\timer\timer.h
# End Source File
# Begin Source File

SOURCE=.\code\world\world.h
# End Source File
# Begin Source File

SOURCE=.\code\xloader\xloader.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
