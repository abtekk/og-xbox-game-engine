/***************************************************************************/
/*                                                                         */
/*  File:    debug.cpp                                                     */
/*                                                                         */
/*  Details: Debugging Code                                                */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#include "debug.h"

// Notice the xbox debug information file...in this case it will be put to
// the same location as the xbe, when you put it to cdrw or dvdr later on, 
// be sure to change this!

#define DEBUG_FILE "D:\\DebugInfo.txt"

void debug(char *str)
{
	// Used during debugging of the program
	
	FILE *fp;
	fp=fopen(DEBUG_FILE, "a+");
	fprintf(fp, "%s\n", str);
	fclose(fp);
	
}

// Don't forget now, if you want to exclude your debug information, just
// comment out the code inside the debug function using /* and */