/***************************************************************************/
/*                                                                         */
/*  timer.cpp                                                              */
/*                                                                         */
/*                                                                         */
/*  Details: CTimer class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/


#include "timer.h"



float CTimer::m_ftime = 0.0f;