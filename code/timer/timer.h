/***************************************************************************/
/*                                                                         */
/*  timer.h                                                                */
/*                                                                         */
/*                                                                         */
/*  Details: CTimer class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/
/*
  This class will get used all over the place, and is an extremely simple
  class.  We need to keep track of time...we need to know has 3 seconds
  passed or 10 seconds etc.

  This CTimer class accomplishes all this.

  How it works:

  -1- We create an instance of our class:

  CTimer timer;

  -2- Update the CTimer internal clock

  timer.UpdateAll();

  -3- Check if a delay time has passed.

  if ( timer.GetTime() > 3000 ) // has 3 seconds passed?
  {
    // yes
	.. do code

    // Set the timer back to zero.
	timer.Reset();
  }

  THATS IT!...can't be more simple :)

  Also, you only need to call UpdateAll() to update the time in all your
  CTimer instances...as it uses a static variable to keep the time...saves
  you calling Update again and again for all your CTimer's...just call it
  once then check all your timers using GetTime().

/***************************************************************************/

#include <xtl.h>

class CTimer
{
public:
	// Constructor, set our member to 0.0f.
	CTimer() { m_ftime = 0.0f; m_fElapsedTime=0.0f; };


	void Reset() { m_fElapsedTime = m_ftime; }

	const float GetTime() { return m_ftime - m_fElapsedTime; m_fElapsedTime = m_ftime; };
	

	void UpdateAll()
	{
		m_ftime = (float)GetTickCount();
	}

	float m_fElapsedTime;

	static float m_ftime;
};










