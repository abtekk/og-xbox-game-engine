/***************************************************************************/
/*                                                                         */
/*  octree.cpp                                                             */
/*                                                                         */
/*                                                                         */
/*  Details: COctree class                                                 */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/

#include "octree.h"


stTex g_Textures[100] = {0};
int       g_iTextures = 0;



IDirect3DDevice8* COctree::m_pD3DDevice;

float x_pos = 0.0f;
float z_pos = 0.0f;

D3DXVECTOR3 g_vPosFrustum = D3DXVECTOR3(0,0,0);;

int iNodeCount;
int COctree::m_iCurrentNodeLevel = 0;


COctree::COctree(void)
{
	m_iTriangles    = 0;
	m_iVertices	    = 0;
	m_bAnyTriangles = false;
	m_fDiameter     = 0.0f;

	m_iMeshes       = 0;

	m_vCenter		= D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	m_pVerts        = NULL;

	ZeroMemory(m_pOctreeNodes, sizeof(m_pOctreeNodes));
	memset(m_pOctreeNodes, 0, sizeof(m_pOctreeNodes));
};

COctree::~COctree(void){};

//------------------------------------------------------------------


void COctree::Create(IDirect3DDevice8** pDevice, stVERTEX* pVerts, int iNumVertices)
{
	m_pD3DDevice = *pDevice;

	m_iCurrentNodeLevel = 0;

	SetData(pVerts, iNumVertices);
	CreateNode(pVerts, iNumVertices, m_vCenter, m_fDiameter);
}
	

void COctree::Release()
{
	if(m_pVerts)
	{
		delete[] m_pVerts;
		m_pVerts = NULL;
	}
	for(int i=0; i<8; i++)
	{
		if( m_pOctreeNodes[i] ) 
		{
			m_pOctreeNodes[i]->Release();
			m_pOctreeNodes[i] = NULL;
		}
	}
	
	for(int i=0; i<g_iTextures; i++)
		g_Textures[i].pTexture->Release();
}




// Each node has some information which describes it, for example
// how many triangles, width etc.
void COctree::SetData(stVERTEX* pVerts, int iNumVertices)
{
	if( (pVerts==NULL) || (iNumVertices<=0)) return;

	//<1> Lets find the center
	for(int i=0; i<iNumVertices; i++)
	{
		D3DXVECTOR3 vPos = pVerts[i].vPos;

		m_vCenter.x = m_vCenter.x + vPos.x;
		m_vCenter.y = m_vCenter.y + vPos.y;
		m_vCenter.z = m_vCenter.z + vPos.z;
	}
	m_vCenter.x = m_vCenter.x / (float)iNumVertices;
	m_vCenter.y = m_vCenter.y / (float)iNumVertices;
	m_vCenter.z = m_vCenter.z / (float)iNumVertices;
	//   /|\
	//    |
	//    +-----Center of our CUBE (e.g. each node is a cube shape)

	float fWidthMax  = 0.0f;
	float fHeightMax = 0.0f;
	float fDepthMax  = 0.0f;

	for(int i=0; i<iNumVertices; i++)
	{
		D3DXVECTOR3 vPos = pVerts[i].vPos;
		float fWidth  = abs_f(vPos.x - m_vCenter.x);
		float fHeight = abs_f(vPos.y - m_vCenter.y);
		float fDepth  = abs_f(vPos.z - m_vCenter.z);

		if( fWidth > fWidthMax )  fWidthMax=fWidth;
		if( fHeight > fHeightMax) fHeightMax=fHeight;
		if( fDepth > fDepthMax)   fDepthMax=fDepth;
	}
		
	// Now fWidthMax, fHeightMax and not forgetting fDepthMax
	// contain the largest radius's...not diameter..so we'll
	// fix that now.
	fWidthMax  *= 2;
	fHeightMax *= 2;
	fDepthMax  *= 2;

	// Lets find out which is the largest...and will be our
	// cubes diameter.
	m_fDiameter = fWidthMax;
	if( fHeightMax > m_fDiameter ) m_fDiameter=fHeightMax;
	if( fDepthMax  > m_fDiameter ) m_fDiameter=fDepthMax;

	//char buf[200];
	//sprintf(buf, "fDiameter: %f \n", m_fDiameter);
	//abc(buf);
	//sprintf(buf, "vCenter x, y, z: %f, %f, %f \n", m_vCenter.x, m_vCenter.y, m_vCenter.z);
	//abc(buf);
}
	
// Helper function, as abs uses int...so can cause rounding.
float COctree::abs_f(float f)
{
	if( f<0.0f) return -f;
	return f;
}


void COctree::CreateNode(stVERTEX* pVerts, int iNumVertices, D3DXVECTOR3 vCenter, float fDiameter)
{
	m_vCenter = vCenter;

	int iNumTriangles = iNumVertices/3;

	m_fDiameter = fDiameter;

	int iMaxNodes = MAXNODELEVEL;
	
	if( (iNumTriangles < POLYGONS) || ( m_iCurrentNodeLevel >= iMaxNodes) )
	{
		SetNode(pVerts, iNumVertices);
		//m_iCurrentNodeLevel++;
		//iNodeCount++;
		m_bAnyTriangles = true;
		
	}
	else
	{
		//iNodeCount++;
		// static variable
		//m_iCurrentNodeLevel++;

		m_bAnyTriangles = false;

		bool* pBoolArray1 = new bool[iNumTriangles];
		bool* pBoolArray2 = new bool[iNumTriangles];
		bool* pBoolArray3 = new bool[iNumTriangles];
		bool* pBoolArray4 = new bool[iNumTriangles];
		bool* pBoolArray5 = new bool[iNumTriangles];
		bool* pBoolArray6 = new bool[iNumTriangles];
		bool* pBoolArray7 = new bool[iNumTriangles];
		bool* pBoolArray8 = new bool[iNumTriangles];
			
		ZeroMemory(pBoolArray1, iNumTriangles);
		ZeroMemory(pBoolArray2, iNumTriangles);
		ZeroMemory(pBoolArray3, iNumTriangles);
		ZeroMemory(pBoolArray4, iNumTriangles);
		ZeroMemory(pBoolArray5, iNumTriangles);
		ZeroMemory(pBoolArray6, iNumTriangles);
		ZeroMemory(pBoolArray7, iNumTriangles);
		ZeroMemory(pBoolArray8, iNumTriangles);

		D3DXVECTOR3 vCtr = vCenter;
		
		// Loop through all our vertices, and allocate to the appropriate
		// cube area.
		for(int i=0; i< iNumVertices; i++)
		{
			D3DXVECTOR3 vPoint = pVerts[i].vPos;
			
			// TOP_FRONT_LEFT
			if( (vPoint.y >= vCtr.y) && (vPoint.x <= vCtr.x) && (vPoint.z >= vCtr.z) )
				pBoolArray1[i/3] = true;
			// TOP_FRONT_RIGHT
			if( (vPoint.y >= vCtr.y) && (vPoint.x >= vCtr.x) && (vPoint.z >= vCtr.z) )
				pBoolArray2[i/3] = true;
			// TOP_BACK_LEFT
			if( (vPoint.y >= vCtr.y) && (vPoint.x <= vCtr.x) && (vPoint.z <= vCtr.z) )
				pBoolArray3[i/3] = true;
			// TOP_BACK_RIGHT
			if( (vPoint.y >= vCtr.y) && (vPoint.x >= vCtr.x) && (vPoint.z <= vCtr.z) )
				pBoolArray4[i/3] = true;
				
			// BOTTOM_FRONT_LEFT
			if( (vPoint.y <= vCtr.y) && (vPoint.x <= vCtr.x) && (vPoint.z >= vCtr.z) )
				pBoolArray5[i/3] = true;
			// BOTTOM_FRONT_RIGHT
			if( (vPoint.y <= vCtr.y) && (vPoint.x >= vCtr.x) && (vPoint.z >= vCtr.z) )
				pBoolArray6[i/3] = true;
			// BOTTOM_BACK_LEFT
			if( (vPoint.y <= vCtr.y) && (vPoint.x <= vCtr.x) && (vPoint.z <= vCtr.z) )
				pBoolArray7[i/3] = true;
			// BOTTOM_BACK_RIGHT
			if( (vPoint.y <= vCtr.y) && (vPoint.x >= vCtr.x) && (vPoint.z <= vCtr.z) )
				pBoolArray8[i/3] = true;
		}
		// Shall we see how many triangles is in each space partition node?
		int iCount1 = 0;		int iCount5 = 0;
		int iCount2 = 0;		int iCount6 = 0;
		int iCount3 = 0;		int iCount7 = 0;
		int iCount4 = 0;		int iCount8 = 0;

		for(int i=0; i<iNumTriangles; i++)
		{
			if(pBoolArray1[i]==true) iCount1++;
			if(pBoolArray2[i]==true) iCount2++;
			if(pBoolArray3[i]==true) iCount3++;
			if(pBoolArray4[i]==true) iCount4++;
			if(pBoolArray5[i]==true) iCount5++;
			if(pBoolArray6[i]==true) iCount6++;
			if(pBoolArray7[i]==true) iCount7++;
			if(pBoolArray8[i]==true) iCount8++;
		}
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray1, vCenter, fDiameter, iCount1,TOP_FRONT_LEFT);
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray2, vCenter, fDiameter, iCount2,TOP_FRONT_RIGHT);
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray3, vCenter, fDiameter, iCount3,TOP_BACK_LEFT);
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray4, vCenter, fDiameter, iCount4,TOP_BACK_RIGHT);

		CreateNodeEnd(pVerts, iNumVertices, pBoolArray5, vCenter, fDiameter, iCount5,BOTTOM_FRONT_LEFT);
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray6, vCenter, fDiameter, iCount6,BOTTOM_FRONT_RIGHT);
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray7, vCenter, fDiameter, iCount7,BOTTOM_BACK_LEFT);
		CreateNodeEnd(pVerts, iNumVertices, pBoolArray8, vCenter, fDiameter, iCount8,BOTTOM_BACK_RIGHT);
	}
}


D3DXVECTOR3 COctree::GetNodeCenter(D3DXVECTOR3 vCurrentCenter, float fDiameter, int iWhichNode)
{
	D3DXVECTOR3 vCtr = vCurrentCenter;
	D3DXVECTOR3 vNewCtr = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	float fDia = fDiameter;

	switch( iWhichNode )
	{
	case TOP_FRONT_LEFT:      // 0
		vNewCtr = D3DXVECTOR3( vCtr.x - fDia/4, vCtr.y + fDia/4, vCtr.z + fDia/4 );
		break;
	case TOP_FRONT_RIGHT:     // 1
		vNewCtr = D3DXVECTOR3( vCtr.x + fDia/4, vCtr.y + fDia/4, vCtr.z + fDia/4 );
		break;
	case TOP_BACK_LEFT:       // 2
		vNewCtr = D3DXVECTOR3( vCtr.x - fDia/4, vCtr.y + fDia/4, vCtr.z - fDia/4 );
		break;
	case TOP_BACK_RIGHT:      // 3
		vNewCtr = D3DXVECTOR3( vCtr.x + fDia/4, vCtr.y + fDia/4, vCtr.z - fDia/4 );
		break;
	case BOTTOM_FRONT_LEFT:   // 4
		vNewCtr = D3DXVECTOR3( vCtr.x - fDia/4, vCtr.y - fDia/4, vCtr.z + fDia/4 );
		break;
	case BOTTOM_FRONT_RIGHT:  // 5
		vNewCtr = D3DXVECTOR3( vCtr.x + fDia/4, vCtr.y - fDia/4, vCtr.z + fDia/4 );
		break;
	case BOTTOM_BACK_LEFT:    // 6
		vNewCtr = D3DXVECTOR3( vCtr.x - fDia/4, vCtr.y - fDia/4, vCtr.z - fDia/4 );
		break;
	case BOTTOM_BACK_RIGHT:   // 7 
		vNewCtr = D3DXVECTOR3( vCtr.x + fDia/4, vCtr.y - fDia/4, vCtr.z - fDia/4 );
		break;
	}
	return vNewCtr;
}

void COctree::CreateNodeEnd(stVERTEX* pTotalVerts, int iNumTotalVertices, bool* pBools,
					   D3DXVECTOR3 vCenter, float fDiameter,
					   int iTriangles, int iWhichNode)
{
	// Is there any triangles in this node?
	if( iTriangles == 0 )
		return;

	stVERTEX* pNodeVerts = new stVERTEX[iTriangles*3];
	int iCount=0;
	for(int i=0; i<iNumTotalVertices; i++)
	{
		if( pBools[i/3] )
		{
			pNodeVerts[iCount] = pTotalVerts[i];
			iCount++;
		}
	}

	m_pOctreeNodes[iWhichNode] = new COctree;

	D3DXVECTOR3 vNewCenter = GetNodeCenter(vCenter, fDiameter, iWhichNode);

	//char buf[300];
	//sprintf(buf, "node: %d vCenter: %.4f, %.4f, %.4f", iWhichNode, vNewCenter.x, vNewCenter.y, vNewCenter.z);
	//abc(buf);

	m_iCurrentNodeLevel++;

	m_pOctreeNodes[iWhichNode]->CreateNode(pNodeVerts, iTriangles*3, vNewCenter, fDiameter/2);

	m_iCurrentNodeLevel--;

	delete[] pNodeVerts;
}


bool InArray(int* pArray, int iSizeArray, int iValue)
{
	for( int i=0; i< iSizeArray; i++)
	{
		if( pArray[i] == iValue )
			return true;
	}
	return false;
}
	
void COctree::SetNode(stVERTEX* pVerts, int iNumVertices)
{
	m_bAnyTriangles = true;

	m_iTriangles = iNumVertices/3;
	m_iVertices  = iNumVertices;

	m_pVerts = new stVERTEX[iNumVertices];
	ZeroMemory(m_pVerts, sizeof(stVERTEX)*m_iVertices);

	memcpy(m_pVerts, pVerts, sizeof(stVERTEX)*iNumVertices);

	//----------------------------------------------------------//
	// See how many vertices don't have texturing
	int iNonTextured = 0;
	for( int i=0; i<m_iVertices; i++)
		if( !pVerts[i].bTexture ) iNonTextured++;

	// See how many are textured
	int iTotalTextured = 0;
	for( int i=0; i<m_iVertices; i++)
		if( pVerts[i].bTexture ) iTotalTextured++;

	// How many different textures there are
	int iManyTextures = 0;
	int* TexIDs = new int[iTotalTextured];
	for( int i=0; i<m_iVertices; i++)
	{
		if(pVerts[i].bTexture)
		{
			// Is it in our list of textures
			if( InArray( TexIDs, iManyTextures, pVerts[i].iTexID ) == false )
			{
				TexIDs[iManyTextures] = pVerts[i].iTexID;
				iManyTextures++;
			}	 
		}
	}

	// Lets see how many of each there are
	int* iHowManyOfEach =  new int[iTotalTextured];
	memset(iHowManyOfEach, 0, sizeof(int)*iTotalTextured);
	if( iTotalTextured > 0 ) // Check that there are textured vertices
		for( int i=0; i<m_iVertices; i++) // Check all! vertices
		{
			for(int tt=0; tt<iTotalTextured; tt++) // For each vertice, compare it to all our TextureID's
			{
				if( pVerts[i].iTexID == tt )
					iHowManyOfEach[tt]++;
			}
		}

	delete[] TexIDs;

	// Non Textured Version
	if( iNonTextured > 0 )
	{
		int iTriangles = iNonTextured/3;
		stMeshOctree newMesh;
		newMesh.bTexture = false;
		newMesh.D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_DIFFUSE;
		newMesh.iNumTriangles = iTriangles;
		struct CUSTOMVERTEX
		{
			D3DXVECTOR3 vPos;
			D3DXVECTOR3 vNormal;
			DWORD colour; // The vertex colour.
		};
		newMesh.iSizeCustomVertex = sizeof(CUSTOMVERTEX);

		IDirect3DVertexBuffer8* pVertexBuffer;
		UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE;
		
		m_pD3DDevice->CreateVertexBuffer( iNonTextured*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
									D3DPOOL_DEFAULT, &pVertexBuffer);
		CUSTOMVERTEX* pV;
		
		pVertexBuffer->Lock(0, iNonTextured*sizeof(CUSTOMVERTEX), (BYTE**)&pV, 0);
		int indx=0;
		for(int i=0; i<m_iVertices; i++)
		{
			if( m_pVerts[i].bTexture == false )
			{
				pV[indx].vPos    = m_pVerts[i].vPos;
				pV[indx].vNormal = m_pVerts[i].vNormal;
				pV[indx].colour  = m_pVerts[i].colour;
				indx++;
			}
		}  
		pVertexBuffer->Unlock();
		newMesh.pVertBuffer = pVertexBuffer;
		m_iMeshes++;
		m_pMeshs.push_back(newMesh);

	}

	if( iTotalTextured > 0 )
	{
		for( int p=0; p<iManyTextures; p++)
		{
			int iTexVertices = iHowManyOfEach[p];
			if(iTexVertices == 0)continue;

			stMeshOctree newMesh;
			newMesh.bTexture = true;
			newMesh.D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_TEX1;
			newMesh.iNumTriangles = iTexVertices/3;
			struct CUSTOMVERTEX
			{
				D3DXVECTOR3 vPos;
				float tu, tv;
			};
			newMesh.iSizeCustomVertex = sizeof(CUSTOMVERTEX);

			IDirect3DVertexBuffer8* pVertexBuffer;
			UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_TEX1;
			
			HRESULT e = m_pD3DDevice->CreateVertexBuffer( iTexVertices*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
										D3DPOOL_DEFAULT, &pVertexBuffer);

			CUSTOMVERTEX* pV;
			pVertexBuffer->Lock(0, iTexVertices*sizeof(CUSTOMVERTEX), (BYTE**)&pV, 0);
			
			int indx=0;
			for(int i=0; i<m_iVertices; i++)
			{
				if( m_pVerts[i].bTexture == true )
				{
					if( p == m_pVerts[i].iTexID )
					{
						pV[indx].vPos    = m_pVerts[i].vPos;
						pV[indx].tu      = m_pVerts[i].tu;
						pV[indx].tv      = m_pVerts[i].tv;
						indx++;

						newMesh.iTexID   = m_pVerts[i].iTexID;
					}
				}
			}  
			
			pVertexBuffer->Unlock();
			
			newMesh.pVertBuffer = pVertexBuffer;
			m_iMeshes++;
			m_pMeshs.push_back(newMesh);
		}
	}
	
}


//------------------------------------------------------------------
// Drawing code.
void COctree::RenderOctree(COctree* pNode)
{	
	if( pNode == NULL ) return;

	if( pNode->m_bAnyTriangles )
	{
		RenderSingleNode(pNode);
	}
	else
	{
		// We go on to call the nested nodes
		RenderOctree(pNode->m_pOctreeNodes[TOP_FRONT_LEFT]);
		RenderOctree(pNode->m_pOctreeNodes[TOP_FRONT_RIGHT]);
		RenderOctree(pNode->m_pOctreeNodes[TOP_BACK_LEFT]);
		RenderOctree(pNode->m_pOctreeNodes[TOP_BACK_RIGHT]);

		RenderOctree(pNode->m_pOctreeNodes[BOTTOM_FRONT_LEFT]);
		RenderOctree(pNode->m_pOctreeNodes[BOTTOM_FRONT_RIGHT]);
		RenderOctree(pNode->m_pOctreeNodes[BOTTOM_BACK_LEFT]);
		RenderOctree(pNode->m_pOctreeNodes[BOTTOM_BACK_RIGHT]);
	}
		
}



void COctree::RenderSingleNode(COctree* pNode)
{
	IDirect3DDevice8* pDevice =  m_pD3DDevice;

	if( pNode == NULL ) return;

	if( pNode->m_bAnyTriangles == false )return;

	if(pNode->m_pVerts == NULL) return;

	int iNumTriangles = pNode->m_iTriangles;
	int iNumVertices = pNode->m_iTriangles*3;
			

	float fDiameter = pNode->m_fDiameter;	
	D3DXVECTOR3 vMin = pNode->m_vCenter;
	vMin.x -= fDiameter/2.0f;
	vMin.y -= fDiameter/2.0f;
	vMin.z -= fDiameter/2.0f;

	D3DXVECTOR3 vMax = pNode->m_vCenter;
	vMax.x += fDiameter/2.0f;
	vMax.y += fDiameter/2.0f;
	vMax.z += fDiameter/2.0f;

	// VIEW_AREA - top of octree.h	
	if( TestFrustum( g_vPosFrustum, VIEW_AREA,  pNode) == false )
		return;
	//if( TestBox( x_pos, 0.0f, z_pos, pNode) == true )
	{
		// DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
		wire_cube(m_pD3DDevice, (float*)&vMin, (float*)&vMax);
	}


	D3DXMATRIX matWorld;
	D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	m_pD3DDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(20,20,20));
	
	for( int i=0; i< pNode->m_iMeshes; i++)
	{
		stMeshOctree*pMesh = &(pNode->m_pMeshs[i]);
		if( pMesh->bTexture == false )
		{
			m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

			m_pD3DDevice->SetTexture(0, NULL );
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);

			int iSizeCustomVertex = pMesh->iSizeCustomVertex;
			UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE;
			m_pD3DDevice->SetStreamSource(0, pMesh->pVertBuffer, iSizeCustomVertex);
			m_pD3DDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);

			m_pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, pMesh->iNumTriangles);
		}
		else
		{
			m_pD3DDevice->SetTexture(0, g_Textures[pMesh->iTexID].pTexture);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);

			int iSizeCustomVertex = pMesh->iSizeCustomVertex;
			UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_TEX1;
			m_pD3DDevice->SetStreamSource(0, pMesh->pVertBuffer, iSizeCustomVertex);
			m_pD3DDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);

			m_pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, pMesh->iNumTriangles);
		}
	}

}

bool COctree::TestFrustum(D3DXVECTOR3 vPos, float radius, COctree* pNode)
{
	D3DXVECTOR3 sA  = pNode->m_vCenter;
	float radiusA   = pNode->m_fDiameter/2.0f;

	D3DXVECTOR3 sB  = vPos;
	float radiusB   = radius;

	return bSphereCollision(sA, radiusA, sB, radiusB);
}


////////////////////////////////////////////////////////////////////////
//                                                                    //
//               Sphere Collision Detection                           //
//                                                                    //
////////////////////////////////////////////////////////////////////////

inline float abs_f(float f)
{
	if( f<0 )
		return -f;
	return f;
}

bool COctree::bSphereCollision(D3DXVECTOR3 sphere1, float radius1, D3DXVECTOR3 sphere2, float radius2)
{
	float delta_x = abs_f(sphere1.x - sphere2.x);
	float delta_y = abs_f(sphere1.y - sphere2.y);
	float delta_z = abs_f(sphere1.z - sphere2.z);
	float sum = (delta_x)*(delta_x) + (delta_y)*(delta_y) + (delta_z)*(delta_z);

	if(sum <= (radius1 + radius2)*(radius1 + radius2) )
		return true;  // A collision has occured
	else
		return false; // No collision
}


/***************************************************************************/
/*                                                                         */
/*                       BOUNDING BOX COLLISION                            */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
// Returns true if there was a collision, false for no collision.

bool PointInBox(	D3DXVECTOR3 minA, D3DXVECTOR3 maxA, 
					D3DXVECTOR3 vPoint)
{

	D3DXVECTOR3 vMinA = minA;
	D3DXVECTOR3 vMaxA = maxA;


	if( (vPoint.x>vMinA.x) && ( vPoint.x < vMaxA.x )  )
	{
			if( (vPoint.z > vMinA.z) && (vPoint.z < vMaxA.z) )
				{
					// There was a collision return true;
					return true;
				}
	}
	
	// No Collison
	return false;
}

bool TestFrustumB(D3DXVECTOR3 vPos, float radius, COctree* pNode)
{
	D3DXVECTOR3 sA  = pNode->m_vCenter;
	float radiusA   = pNode->m_fDiameter/2.0f;

	D3DXVECTOR3 BOXAmin;
	BOXAmin.x = sA.x - radiusA;
	BOXAmin.y = sA.y - radiusA;
	BOXAmin.z = sA.z - radiusA;
	D3DXVECTOR3 BOXAmax;
	BOXAmax.x = sA.x + radiusA;
	BOXAmax.y = sA.y + radiusA;
	BOXAmax.z = sA.z + radiusA;


	D3DXVECTOR3 sB  = vPos;
	float radiusB   = radius;

	D3DXVECTOR3 BOXBmin;
	BOXBmin.x = sB.x - radiusB;
	BOXBmin.y = sB.y - radiusB;
	BOXBmin.z = sB.z - radiusB;
	D3DXVECTOR3 BOXBmax;
	BOXBmax.x = sB.x + radiusB;
	BOXBmax.y = sB.z + radiusB;
	BOXBmax.z = sB.y + radiusB;

	bool bCollision = false;
	bCollision |= PointInBox( BOXAmin, BOXAmax, vPos );
	bCollision |= PointInBox( BOXAmin, BOXAmax, D3DXVECTOR3(vPos.x + radius, vPos.y, vPos.z));
	bCollision |= PointInBox( BOXAmin, BOXAmax, D3DXVECTOR3(vPos.x - radius, vPos.y, vPos.z));
	bCollision |= PointInBox( BOXAmin, BOXAmax, D3DXVECTOR3(vPos.x,          vPos.y, vPos.z + radius));
	bCollision |= PointInBox( BOXAmin, BOXAmax, D3DXVECTOR3(vPos.x,          vPos.y, vPos.z - radius));
	
	return bCollision;
}



bool COctree::CollisionCheck(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir)
{
	if( pNode == NULL ) return false;

	D3DXMATRIX* pMatrix = &(pOther->m_matWorld);
	D3DXVECTOR3 vCenter = pOther->m_center;
	D3DXVec3TransformCoord( &vCenter, &vCenter, pMatrix);

	g_vPosFrustum = vCenter;
	g_vPosFrustum.y = 0.0f;

	                        //  +--- Set to constant for this GAME ONLYE!!!!!!!! top of octree.h
                            //  |
			                // \|/
	if( (TestFrustumB(vCenter, COLLISION_CHECK_RADIUS, pNode) == false) )
	{
		return false;
	}
	else
	{
		if( pNode->m_bAnyTriangles > 0 )
		{
			return CheckNodeCollision(pNode, pOther, vDir);
		}
		else
		{
			bool bCollision = false;
			// We go on to call the nested nodes
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[TOP_FRONT_LEFT], pOther, vDir);
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[TOP_FRONT_RIGHT], pOther, vDir);
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[TOP_BACK_LEFT], pOther, vDir);
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[TOP_BACK_RIGHT], pOther, vDir);

			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[BOTTOM_FRONT_LEFT], pOther, vDir);
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[BOTTOM_FRONT_RIGHT], pOther, vDir);
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[BOTTOM_BACK_LEFT], pOther, vDir);
			bCollision |= CollisionCheck(pNode->m_pOctreeNodes[BOTTOM_BACK_RIGHT], pOther, vDir);

			return bCollision;
		}

	}
	

	return false;
}

bool COctree::CheckNodeCollision(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir)
{
	if( pNode == NULL ) return false;

	if( pNode->m_bAnyTriangles == false )return false;

	if(pNode->m_pVerts == NULL) return false;


	bool bCollision = false;
		
		// Do these setups here, before we go into the loop and check all the triangle's 
		//if it collides with our sphere...for optimisation.

		D3DXMATRIX* pMatrix = &pOther->m_matWorld;
		D3DXVECTOR3 vCenter = pOther->m_center;
		D3DXVec3TransformCoord( &vCenter, &vCenter, pMatrix);

		D3DXMATRIX matWorld;
		D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
		m_pD3DDevice->SetTransform( D3DTS_WORLD, &matWorld );

		int iNumFaces = pNode->m_iVertices/3;
		int index = 0;
		for( int i=0; i<iNumFaces; i++)
		{
			D3DXVECTOR3  p1 =  pNode->m_pVerts[index++].vPos; 
			D3DXVECTOR3  p2 =  pNode->m_pVerts[index++].vPos; 
			D3DXVECTOR3  p3 =  pNode->m_pVerts[index++].vPos; 

			D3DXVec3TransformCoord( &p1, &p1, &matWorld);
			D3DXVec3TransformCoord( &p2, &p2, &matWorld);
			D3DXVec3TransformCoord( &p3, &p3, &matWorld);
                                                            //  +--- Set to constant for this GAME ONLYE!!!!!!!! IMPROTANT
                                                            //  |
			                                                // \|/
			bCollision = SphereToTriangleCollision( &vCenter, 30.0f,  &p1,&p2,&p3);

			if(bCollision == true)
			{
				D3DXVECTOR3 vNewDir = CollisionReaction(*vDir, (float*)&p1, (float*)&p2, (float*)&p3, m_pD3DDevice);
				*vDir = vNewDir;

				arrow(m_pD3DDevice, D3DXVECTOR3(p1.x, 10.0f, p1.z), 
		                vNewDir);
			
				return true;
			}

		}

	return false;

}

bool COctree::CheckNodeHeightCollision(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir)
{
	if( pNode == NULL ) return false;

	if( pNode->m_bAnyTriangles == false )return false;

	if(pNode->m_pVerts == NULL) return false;


	bool bCollision = false;
		
		// Do these setups here, before we go into the loop and check all the triangle's 
		//if it collides with our sphere...for optimisation.

		D3DXMATRIX* pMatrix = &pOther->m_matWorld;
		D3DXVECTOR3 vCenter = pOther->m_center;
		D3DXVec3TransformCoord( &vCenter, &vCenter, pMatrix);

		D3DXMATRIX matWorld;
		D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
		m_pD3DDevice->SetTransform( D3DTS_WORLD, &matWorld );

		int iNumFaces = pNode->m_iVertices/3;
		int index = 0;
		for( int i=0; i<iNumFaces; i++)
		{
			D3DXVECTOR3  p1 =  pNode->m_pVerts[index++].vPos; 
			D3DXVECTOR3  p2 =  pNode->m_pVerts[index++].vPos; 
			D3DXVECTOR3  p3 =  pNode->m_pVerts[index++].vPos; 

			D3DXVec3TransformCoord( &p1, &p1, &matWorld);
			D3DXVec3TransformCoord( &p2, &p2, &matWorld);
			D3DXVec3TransformCoord( &p3, &p3, &matWorld);
                                                            //  +--- Set to constant for this GAME ONLYE!!!!!!!! IMPROTANT
                                                            //  |
			                                                // \|/
			//bCollision = SphereToTriangleCollision( &vCenter, 30.0f,  &p1,&p2,&p3);
			D3DXVECTOR3 vCenterDown = vCenter;
			vCenterDown.y -= 40.0f;
			bool bCollision = TriangeToLineCollision( (float*)&p1, (float*)p2, (float*)p3,
					(float*)&vCenter, (float*)&vCenterDown );

			if(bCollision == true)
			{
				
				D3DXVECTOR3 vNewDir = CollisionHeightReaction(vCenter, (float*)&p1, (float*)&p2, (float*)&p3, m_pD3DDevice);
				*vDir = vNewDir;

				//arrow(m_pD3DDevice, D3DXVECTOR3(p1.x, 10.0f, p1.z), 
		        //        vNewDir);
				
				return true;
			}

		}

	return false;

}

bool COctree::UpdateHeight(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir)
{
	if( pNode == NULL ) return false;

	D3DXMATRIX* pMatrix = &(pOther->m_matWorld);
	D3DXVECTOR3 vCenter = pOther->m_center;
	D3DXVec3TransformCoord( &vCenter, &vCenter, pMatrix);

	g_vPosFrustum = vCenter;
	g_vPosFrustum.y = 0.0f;

	                        //  +--- Set to constant for this GAME ONLYE!!!!!!!! top of octree.h
                            //  |
			                // \|/
	if( (TestFrustumB(vCenter, COLLISION_CHECK_RADIUS, pNode) == false) )
	{
		return false;
	}
	else
	{
		if( pNode->m_bAnyTriangles > 0 )
		{
			//return false ;//CheckNodeCollision(pNode, pOther, vDir);
			return CheckNodeHeightCollision(pNode, pOther, vDir);
		}
		else
		{
			bool bCollision = false;
			// We go on to call the nested nodes
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[TOP_FRONT_LEFT],     pOther, vDir);
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[TOP_FRONT_RIGHT],    pOther, vDir);
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[TOP_BACK_LEFT],      pOther, vDir);
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[TOP_BACK_RIGHT],     pOther, vDir);

			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[BOTTOM_FRONT_LEFT],  pOther, vDir);
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[BOTTOM_FRONT_RIGHT], pOther, vDir);
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[BOTTOM_BACK_LEFT],   pOther, vDir);
			bCollision |= UpdateHeight(pNode->m_pOctreeNodes[BOTTOM_BACK_RIGHT],  pOther, vDir);

			return bCollision;
		}

	}
	

	return false;
}
