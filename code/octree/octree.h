/***************************************************************************/
/*                                                                         */
/*  octree.h                                                               */
/*                                                                         */
/*                                                                         */
/*  Details: COctree class                                                 */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/

// This is a class which will generate a octree for a simple set of vertices
// then break it up!

// int COctree::m_iCurrentNodeLevel=0; is a static variable!....





#pragma once

#include <xtl.h>

#include <stdio.h>


#include <vector>
using namespace std ;

#include "../collision/collision.h"
#include "../collision/collision_functions.h"
#include "../collisionreaction/collisionreaction.h"
#include "../graphics_helpers/graphics_helpers.h"


// ****DEFINE MAX POLYS PER CUBE***DEFINE MAX POLYS PER CUBE****
#define POLYGONS 250
// ****DEFINE MAX POLYS PER CUBE***DEFINE MAX POLYS PER CUBE****

#define VIEW_AREA 500
#define COLLISION_CHECK_RADIUS 100
#define MAXNODELEVEL 15

#define MAXTEXTURES 100

struct stTex
{
	IDirect3DTexture8* pTexture;
	char szTexFileName[256];
};

extern stTex g_Textures[MAXTEXTURES]; // top of octree.cpp
extern g_iTextures;

struct stMeshOctree
{
	IDirect3DVertexBuffer8* pVertBuffer;
	int iNumTriangles;
	UINT D3DFVF_CUSTOMVERTEX;
	bool bTexture;
	int  iTexID;
	int iSizeCustomVertex;
	stMeshOctree()
	{
		bTexture=false;
	}
};

struct stVERTEX
{
	D3DXVECTOR3 vPos;
	D3DXVECTOR3 vNormal;
	DWORD colour;
	bool bTexture;
	int  iTexID;
	float tu, tv;
};

class COctree
{
	//--------------Basic Constructor/Destructor------------------------
public:
	COctree(void);
	~COctree(void);

	//------------------------------------------------------------------
	static int m_iCurrentNodeLevel;

	static IDirect3DDevice8* m_pD3DDevice;

	void Create( IDirect3DDevice8** pDevice, stVERTEX* pVerts, int iNumVertices);
	void Release();

protected:
	// Each Octree node is split up into 8...yup 8..not 4 equal squares.
	// Now we'll have to give each square a nice goochi name ;)
	enum OctreeParts
	{
		TOP_FRONT_LEFT,      // 0
		TOP_FRONT_RIGHT,     // 1
		TOP_BACK_LEFT,       // 2
		TOP_BACK_RIGHT,      // 3
		BOTTOM_FRONT_LEFT,   // 4
		BOTTOM_FRONT_RIGHT,  // 5
		BOTTOM_BACK_LEFT,    // 6
		BOTTOM_BACK_RIGHT    // 7
	};
public:

	// Each node has some information which describes it, for example
	// how many triangles, width etc.
	int		m_iTriangles;
	int	    m_iVertices;
	int		m_bAnyTriangles;
	float   m_fDiameter;

	stVERTEX* m_pVerts;


	vector<stMeshOctree> m_pMeshs;
	int            m_iMeshes;

	D3DXVECTOR3  m_vCenter;

	COctree *m_pOctreeNodes[8];

	void SetData(stVERTEX* pVerts, int iNumVertices);
	
	// Helper function, as abs uses int...so can cause rounding.
	float abs_f(float f);


	void CreateNode(stVERTEX* pVerts, int iNumVertices, D3DXVECTOR3 vCenter, float fDiameter);

	D3DXVECTOR3 GetNodeCenter(D3DXVECTOR3 vCurrentCenter, float fDiameter, int iWhichNode);
	void CreateNodeEnd(stVERTEX* pTotalVerts, int iNumTotalVertices, bool* pBools,
					   D3DXVECTOR3 vCenter, float fDiameter,
					   int iTriangles, int iWhichNode);
	void SetNode(stVERTEX* pVerts, int iNumVertices);


	//------------------------------------------------------------------
	// Drawing code.
public:

	void RenderOctree(COctree* pNode);
	void RenderSingleNode(COctree* pNode);

	bool TestFrustum(D3DXVECTOR3 vPos, float radius, COctree* pNode);
	bool bSphereCollision(D3DXVECTOR3 sphere1, float radius1, D3DXVECTOR3 sphere2, float radius2);


	bool CollisionCheck(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir=NULL);
	bool CheckNodeCollision(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir);


	bool UpdateHeight(COctree* pNode, CCollision* pOther, D3DXVECTOR3* vDir=NULL);
	bool CheckNodeHeightCollision(COctree* pNode, CCollision* pOther,  D3DXVECTOR3* vDir);
};

