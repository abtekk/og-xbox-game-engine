/***************************************************************************/
/*                                                                         */
/*  File: bullet.h                                                         */
/*                                                                         */
/*  Details: Bullet Class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/
/*
   Well I was gonig to call it CProjectile, but bullet just sounded more 
   snazzy ;)  
   Hopefully the name should give you a brief description of what this 
   basic class does.
   Now I'm sure you'll have a gun, or something that shoots objects...it
   can be anything you want...shoots bullets...bricks...houses...etc

   To keep it simple it will work like this...we'll create an instance of
   a CBullet class once...and call create with our .x mesh object from file.

   We just have to call Add(..) member function when ever we want to add
   a bullet...of course we'll have to pass in details like direction and
   starting position....but I'm sure that's not to bad.

   So the object moves..we'll call Update(..) with a time variable...the 
   larger the time varialbe the faster the bullet shoots off into the horizon.


   <1>
   CButtet b;
   b.Create(&pDevice, "D:\\MeshBullet.x");

   <2> GameLoop

   // Details:
   // b.Add( xPos, yPos, zPos,  xDir,yDir,zDir,  MaxDistance);

   b.Add( 0.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   100.0f.);
   b.Update(0.001f);


  <3> Tidy Up
  b.Release();


*/
/***************************************************************************/



#ifndef _XF_BULLET_H
#define _XF_BULLET_H


#include <xtl.h>

// We'll use the basic CXLoader class to draw our bullets, as there not animated
// and very basic.
#include "../xloader/xloader.h"


#define MAX_BULLETS 10

class CBullet
{

protected:

	IDirect3DDevice8* m_pD3DDevice;
	CXLoader m_Bullet;
public:
	bool Create(IDirect3DDevice8 ** pDevice, char* szFileName, char* szTextureDirectory = ".\\")
	{
		m_pD3DDevice = *pDevice;
		m_Bullet.Create( pDevice, szFileName, szTextureDirectory );
		return true;
	}
	void Release()
	{
		m_Bullet.Release();
	}

public:

	struct stData
	{
		bool valid;
		D3DXVECTOR3 vPos;
		D3DXVECTOR3 vVelocity;
		float fMaxDistance;
		float fCurrentDistance;
	};
	stData m_Data[MAX_BULLETS];

	CBullet()
	{
		ZeroMemory(m_Data, sizeof(m_Data));
		for(int i=0; i<MAX_BULLETS; i++)
		{
			m_Data[i].valid = false;
		}
	}

	void Update(float k)
	{
		for(int i=0; i<MAX_BULLETS; i++)
		{
			if( m_Data[i].valid == true)
			{
				m_Data[i].fCurrentDistance += k;

				D3DXVECTOR3 vPosition = m_Data[i].vPos;
				D3DXVECTOR3 vVelocity = m_Data[i].vVelocity;
				D3DXVec3Scale(&vVelocity, &vVelocity, k);

				D3DXVec3Add(&vPosition, &vPosition, &vVelocity);

				m_Data[i].vPos = vPosition;

				if( m_Data[i].fCurrentDistance > m_Data[i].fMaxDistance )
					m_Data[i].valid = false;
			}
		}
	}
	void Render()
	{
		
		for(int i=0; i<MAX_BULLETS; i++)
		{
			if( m_Data[i].valid == true)
			{
				
				float k = 1.0f;
				D3DXVECTOR3 vPosition = m_Data[i].vPos;
				
				
				D3DXVECTOR3 vDir = m_Data[i].vVelocity;
				D3DXVECTOR3 vForward = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

				D3DXVec3Normalize(&vDir, &vDir);
				D3DXVec3Normalize(&vForward, &vForward);

				float angle = D3DXVec3Dot(&vDir,&vForward);
				angle = acosf(angle);

				if( vDir.x < 0 )
					angle = -angle;

				m_Bullet.Rotate(0.0f, angle, 0.0f);

				m_Bullet.Move( vPosition.x, vPosition.y, vPosition.z);
				// This is the single line that renders our bullet ( projectile)
				m_Bullet.Render();
			}

		}

	}
	void Add(D3DXVECTOR3 vPosition, D3DXVECTOR3 vVelocity, float fMaxDistance)
	{
		for(int i=0; i<MAX_BULLETS; i++)
		{
			if( m_Data[i].valid == false)
			{
				m_Data[i].valid = true;
				m_Data[i].vPos = vPosition;
				m_Data[i].vVelocity = vVelocity;

				m_Data[i].fCurrentDistance = 0.0f;
				m_Data[i].fMaxDistance     = fMaxDistance;
				break;
			}
		}
	}

};








#endif // _XF_BULLET_H