/***************************************************************************/
/*                                                                         */
/*  world.h                                                                */
/*                                                                         */
/*                                                                         */
/*  Details: CWorld class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/

#pragma once



#include <xtl.h>


#include "../collision/collision.h"
#include "../octree/octree.h"
#include "../3ds/3ds.h"


class CWorld 
{
protected:
	COctree m_Octree;

	IDirect3DDevice8* m_pD3DDevice;
	

	int       m_iNumVerts;
	stVERTEX* m_pVertices; //defined at the top of octree.h

public:
	bool Create(IDirect3DDevice8** pDevice, char* szFileName, char* szTexLocation)
	{
		
		m_pD3DDevice = *pDevice;
	
		ReadVerticesIn(szFileName, szTexLocation);
		
		m_Octree.Create( &m_pD3DDevice, m_pVertices, m_iNumVerts );

		delete[] m_pVertices;
		

		return true;
	}
	void Release()
	{
		m_Octree.Release();
	}


	void Render()
	{
		m_Octree.RenderOctree( &m_Octree );
	}

protected:

	void ReadVerticesIn(char* szFileName, char* szTexLocation)
	{
		C3DS world;
		world.Create(szFileName);

		//=====================================================================
		
		
		g_iTextures = 0;
		for( int i=0; i< world.m_iNumMaterials; i++ )
		{
			if( world.m_pMaterials[i].bTexFile )
			{
				char szTexFile[256];
				strcpy( szTexFile, world.m_pMaterials[i].szTextureFile);
				strcpy(g_Textures[g_iTextures].szTexFileName, szTexFile);

				// Add in the folder location!
				char szTemp[256];
				strcpy(szTemp, szTexLocation);
				strcat(szTemp, szTexFile);


				D3DXCreateTextureFromFile(m_pD3DDevice, szTemp, &(g_Textures[g_iTextures].pTexture));

				g_iTextures++;
			}
		}
		

		m_iNumVerts = 0;
		int iNumMeshs = world.m_iNumMeshs;
		for(int i=0; i<iNumMeshs; i++)
		{
			int iNumFaces = world.m_pMeshs[i].iNumFaces;
			m_iNumVerts += iNumFaces*3;
		}

		m_pVertices = new stVERTEX[m_iNumVerts];
		
		// Copy over our vertex positions, into our array
		int index = 0;
		for(int cc=0; cc<iNumMeshs; cc++)
		{
			C3DS::stMesh* pMesh = &(world.m_pMeshs[cc]);
			for(int i =0; i< pMesh->iNumFaces; i++)
			{
				for(int corn=0; corn<3; corn++) // 3 sides to a triangle
				{
					int a = pMesh->pFaces[i].corner[corn];

					m_pVertices[index].vPos.x = pMesh->pVerts[a].x;
					m_pVertices[index].vPos.y = pMesh->pVerts[a].y;
					m_pVertices[index].vPos.z = pMesh->pVerts[a].z;
					if( pMesh->bTextCoords )
					{
						m_pVertices[index].tu     = pMesh->pTexs[a].tu;
						m_pVertices[index].tv     = pMesh->pTexs[a].tv;
					}
					m_pVertices[index].bTexture = false;
					index++;
				}
			}
		}
		// Check for any textures and colour, and copy them across.
		index = 0;
		for(int cc=0; cc<iNumMeshs; cc++)
		{
			C3DS::stMesh* pMesh = &(world.m_pMeshs[cc]);
			for(int i =0; i< pMesh->iNumFaces; i++)
			{
				if( world.m_iNumMaterials == false ) continue;
					
				int MatID = pMesh->pFaces[i].MaterialID;

				DWORD dwColr = D3DCOLOR_XRGB(   world.m_pMaterials[MatID].Colour.r,
												world.m_pMaterials[MatID].Colour.g,
												world.m_pMaterials[MatID].Colour.b);
				m_pVertices[index  ].colour = dwColr;
				m_pVertices[index+1].colour = dwColr;
				m_pVertices[index+2].colour = dwColr;

				if( world.m_pMaterials[MatID].bTexFile )
				{
					m_pVertices[index  ].bTexture = true;
					m_pVertices[index+1].bTexture = true;
					m_pVertices[index+2].bTexture = true;
					// See if we have it already loaded
					char szTexFile[256];
					strcpy(szTexFile, world.m_pMaterials[MatID].szTextureFile);
						
					for( int iTex=0; iTex<g_iTextures; iTex++)
					{
						if( strcmp(g_Textures[iTex].szTexFileName, szTexFile) == 0 )
						{ // we already have it - set it and break from further checking
							m_pVertices[index  ].iTexID=iTex;
							m_pVertices[index+1].iTexID=iTex;
							m_pVertices[index+2].iTexID=iTex;
						
							break;
						}
					}
				}
				index+=3;
			}
		}
	

		
		m_iNumVerts;
		m_pVertices;

		for( int i=0; i< m_iNumVerts; i+=3)
		{
			D3DXVECTOR3 vA = m_pVertices[i].vPos;
			D3DXVECTOR3 vB = m_pVertices[i+1].vPos;
			D3DXVECTOR3 vC = m_pVertices[i+2].vPos;

			D3DXVECTOR3 v1 = vB - vA;
			D3DXVECTOR3 v2 = vC - vA;

			D3DXVec3Normalize( &v1, &v1 );
			D3DXVec3Normalize( &v2, &v2 );

			D3DXVECTOR3 vNormal;
			D3DXVec3Cross(&vNormal, &v1, &v2);
			
			m_pVertices[i].vNormal = vNormal;
			m_pVertices[i+1].vNormal = vNormal;
			m_pVertices[i+2].vNormal = vNormal;
		}

		//=======================================================================
		
		world.Release();
	}

public:

	bool CollisionCheck(CCollision* pOther, D3DXVECTOR3* vDir)
	{
		return m_Octree.CollisionCheck(&m_Octree, pOther, vDir );
	}

	// So our character or object stays on top of the mesh!
	void UpdateHeight( CCollision* pOther, D3DXVECTOR3* vDir )
	{
		// Chose a GRAVITY effect of -2.0f, ....also the gravity effect is
		// set in (collisionreaction.cpp) in the setheightcollision(..) function
		// if its a far fall...out of range of a triangle. (default -1)

		D3DXVECTOR3 vDirGravity = *vDir;
		vDirGravity.y = -2.0f;

		m_Octree.UpdateHeight( &m_Octree, pOther, &vDirGravity);
		
		*vDir = vDirGravity;
		
	}

};









