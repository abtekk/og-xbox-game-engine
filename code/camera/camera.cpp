/***************************************************************************/
/*                                                                         */
/*  File: camera.cpp                                                       */
/*                                                                         */
/*  Details: Camera class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#include "camera.h"


void CCamera::Create( IDirect3DDevice8** pDevice )
{
	m_pD3DDevice = *pDevice;

	
	// I'll set an initial position to a little back away from the origin
	m_PosX = m_PosY = 0.0f;
	m_PosZ = -4.0f;
	// Our intial rotational values
	m_RotX = m_RotY = m_RotZ = 0.0f;
	
	D3DXMatrixTranslation(&m_matTranslation, -m_PosX, -m_PosY, -m_PosZ);
	
	D3DXMATRIX mRX, mRY, mRZ;
	D3DXMatrixRotationX(&mRX, -m_RotX);
	D3DXMatrixRotationY(&mRY, -m_RotY);
	D3DXMatrixRotationZ(&mRZ, -m_RotZ);

	m_matRotation = mRZ;
	D3DXMatrixMultiply(&m_matRotation, &m_matRotation, &mRY);
	D3DXMatrixMultiply(&m_matRotation, &m_matRotation, &mRX);


	UpdateCamera();


	D3DXMATRIX  matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, //Result Matrix
					D3DX_PI/4,//Field of View, in radians. (PI/4) is typical
					((float)600 / (float)400),     //Aspect ratio
					1.0f,     //Near view plane
					1000.0f ); // Far view plane 

	m_pD3DDevice->SetTransform( D3DTS_PROJECTION, &matProj );
}

void CCamera::Release()
{
	// Tidy up if we allocate anything.
}

void CCamera::UpdateCamera()
{
	// Combine it all
	D3DXMatrixMultiply(&m_matWorld, &m_matTranslation, &m_matRotation);
	m_pD3DDevice->SetTransform(D3DTS_VIEW,&m_matWorld); 
}

void CCamera::Move( float x, float y, float z)
{
		m_PosX = x;
		m_PosY = y;
		m_PosZ = z;

	D3DXMatrixTranslation(&m_matTranslation, -m_PosX, -m_PosY, -m_PosZ);

	UpdateCamera();
	
}

void CCamera::MoveRel( float x, float y, float z)
{
	Move( m_PosX + x,
		  m_PosY + y,
		  m_PosZ + z );
}

void CCamera::Rotate(float x, float y, float z)
{
	// Save our new rotation value.
	m_RotX = x;
	m_RotY = y;
	m_RotZ = z;

	D3DXMATRIX mRX, mRY, mRZ;
	D3DXMatrixRotationX(&mRX, -m_RotX);
	D3DXMatrixRotationY(&mRY, -m_RotY);
	D3DXMatrixRotationZ(&mRZ, -m_RotZ);

	m_matRotation = mRZ;
	D3DXMatrixMultiply(&m_matRotation, &m_matRotation, &mRY);
	D3DXMatrixMultiply(&m_matRotation, &m_matRotation, &mRX);

	UpdateCamera();

}

void CCamera::RotateRel(float x, float y, float z)
{
	Rotate( m_RotX + x,
		    m_RotY + y,
			m_RotZ + z );
}