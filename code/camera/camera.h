/***************************************************************************/
/*                                                                         */
/*  File: camera.h                                                         */
/*                                                                         */
/*  Details: Camera class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#ifndef _XF_CAMERA_H
#define _XF_CAMERA_H

#include <xtl.h>

class CCamera
{
	IDirect3DDevice8* m_pD3DDevice;

	// This is used to set our VIEW in ->SetTransform(D3DTS_VIEW,&m_matWorld)
	D3DXMATRIX m_matWorld;
	D3DXMATRIX m_matTranslation;
	D3DXMATRIX m_matRotation;

	// Camera's position in 3D world.
	float m_PosX, m_PosY, m_PosZ;

	// The point in 3D space where looking at.
	float m_RotX, m_RotY, m_RotZ;


public:
	// Called once for the creation.
	void Create( IDirect3DDevice8** pDevice );
	void Release();

	// Called if we want to move our camera around.
	void Move   (float x, float y, float z);
	void MoveRel(float x, float y, float z);

	void Rotate   (float x, float y, float z);
	void RotateRel(float x, float y, float z);
public:
	void UpdateCamera(); //Used internally

	// So the camera sort of rotates and moves naturally we have to add a
	// couple more member functions...so that it faces the same direction
	// as the character etc...but still remains behind him
	// Now you'll use these instead of Move and Rotate above....and we'll
	// aim these at 3rd person
public:
	float m_EyeX, m_EyeY, m_EyeZ;
	float m_AtX, m_AtY, m_AtZ;
	float m_AngleX, m_AngleY, m_AngleZ;
	float m_NewAtX, m_NewAtY, m_NewAtZ;

	// Call this once at the begginning of the game...
	void StartPos(float angle_x, float angle_y, float angle_z, float radius)
	{
	};

	// This gets set repeately as the character moves!...always want to be looking at
	// our character..not at the wall next to him!
	void SetPoint(float EyeX, float EyeY, float EyeZ, float AtX, float AtY, float AtZ)
	{
		m_EyeX = EyeX;
		m_EyeY = EyeY;
		m_EyeZ = EyeZ;

		m_AtX = AtX;
		m_AtY = AtY;
		m_AtZ = AtZ;
	};

	// We update this as we move around...and change direction..
	void NewPos(float AtX, float AtY, float AtZ, float angle_x, float angle_y, float angle_z)
	{
		m_NewAtX = AtX;
		m_NewAtY = AtY;
		m_NewAtZ = AtZ;
		m_AngleX = angle_x;
		m_AngleY = angle_y;
		m_AngleZ = angle_z;
	};

	void UpdatePos(float t)
	{
		D3DXVECTOR3 vAt  = D3DXVECTOR3(m_NewAtX,m_NewAtY,m_NewAtZ);
		D3DXVECTOR3 vEye = D3DXVECTOR3(m_EyeX,m_EyeY,m_EyeZ);

		D3DXVECTOR3 vDir = vEye;
		//D3DXVec3Normalize(&vDir, &vEye);

		D3DXMATRIX mRX, mRY, mRZ;
		D3DXMatrixRotationX(&mRX, -m_AngleX);
		D3DXMatrixRotationY(&mRY, -m_AngleY);
		D3DXMatrixRotationZ(&mRZ, -m_AngleZ);

		D3DXMATRIX matRot = mRX;
		D3DXMatrixMultiply(&matRot, &matRot, &mRY);
		D3DXMatrixMultiply(&matRot, &matRot, &mRZ);

		D3DXVec3TransformCoord(&vDir, &vDir, &matRot);

		//vDir = vDir*100.0f;
		D3DXVec3Add(&vDir, &vDir, &vAt);
		

		D3DXVECTOR3 vUp = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

		D3DXMATRIX vOut;
		D3DXMatrixLookAtLH(&vOut,&vDir,&vAt,&vUp); 
		m_pD3DDevice->SetTransform( D3DTS_VIEW, &vOut );

	}

};


#endif // _XF_CAMERA_H





