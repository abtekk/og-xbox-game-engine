/***************************************************************************/
/*                                                                         */
/*  graphics_helpers.h                                                     */
/*                                                                         */
/*                                                                         */
/*  Details: DirectX graphics Init container class                         */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/



#ifndef _XF_GRAPHICS_HELPERS_H
#define _XF_GRAPHICS_HELPERS_H

#include <xtl.h>


void line(IDirect3DDevice8* pDevice, float* start, float* end, D3DXMATRIX* m=NULL);

void triangle(IDirect3DDevice8* pDevice, float* p1, float*p2, float* p3, D3DXMATRIX* m=NULL);

void wire_cube(IDirect3DDevice8* pDevice, float* min, float*max, D3DXMATRIX* m=NULL);

void circle(IDirect3DDevice8* pDevice, float c_x, float c_y, float c_z, float r, D3DXMATRIX* m =NULL);



void arrow(IDirect3DDevice8* pDevice, D3DXVECTOR3 vPos, D3DXVECTOR3 vDir, D3DXMATRIX* m =NULL);





#endif // _XF_GRAPHICS_HELPERS_H