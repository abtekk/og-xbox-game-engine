/***************************************************************************/
/*                                                                         */
/*  graphics_helpers.cpp                                                   */
/*                                                                         */
/*                                                                         */
/*  Details: DirectX graphics Init container class                         */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/

#include "graphics_helpers.h"



void line(IDirect3DDevice8* pDevice, float* start, float* end, D3DXMATRIX* m/*=NULL*/)
{
	D3DXMATRIX matWorld;
	if( m == NULL )	
		D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
	else
		matWorld = *m;

	struct CUSTOMVERTEX
	{
		FLOAT x, y, z; // The transformed position for the vertex.
		DWORD colour; // The vertex colour.
	};
	UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_DIFFUSE;

	IDirect3DVertexBuffer8* pVertexBuffer = NULL;
	pDevice->CreateVertexBuffer(2 * sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
									D3DPOOL_DEFAULT, &pVertexBuffer);
	CUSTOMVERTEX pVertices[2];

	pVertices[0].x = start[0];			pVertices[1].x = end[0];
	pVertices[0].y = start[1];			pVertices[1].y = end[1];
	pVertices[0].z = start[2];			pVertices[1].z = end[2];
	pVertices[0].colour = 0xffff0000;	pVertices[1].colour = 0xffffffff;
	

	void* p;
	pVertexBuffer->Lock(0, 2 * sizeof(CUSTOMVERTEX), (BYTE**)&p, 0);
	
	memcpy(p, pVertices, sizeof(pVertices));
   

	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	pVertexBuffer->Unlock();
	pDevice->SetStreamSource(0, pVertexBuffer, sizeof(CUSTOMVERTEX));
    pDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
    pDevice->DrawPrimitive(D3DPT_LINELIST, 0, 1);

	pVertexBuffer->Release();
}



void triangle(IDirect3DDevice8* pDevice, float* p1, float*p2, float* p3, D3DXMATRIX* m/*=NULL*/)
{
	D3DXMATRIX matWorld;
	if( m == NULL )	
		D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
	else
		matWorld = *m;

	struct CUSTOMVERTEX
	{
		FLOAT x, y, z; // The transformed position for the vertex.
		DWORD colour; // The vertex colour.
	};
	UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_DIFFUSE;

	IDirect3DVertexBuffer8* pVertexBuffer = NULL;
	pDevice->CreateVertexBuffer(3 * sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
									D3DPOOL_DEFAULT, &pVertexBuffer);
	CUSTOMVERTEX pVertices[3];
	pVertices[0].x = p1[0];		pVertices[1].x = p2[0];		pVertices[2].x = p3[0];
	pVertices[0].y = p1[1];		pVertices[1].y = p2[1];		pVertices[2].y = p3[1];
	pVertices[0].z = p1[2];		pVertices[1].z = p2[2];		pVertices[2].z = p3[2];
	pVertices[0].colour=0x00ffff00;
	pVertices[1].colour=0x00ff00ff;
	pVertices[2].colour=0xff00ffff;

	void* p;
	pVertexBuffer->Lock(0, 3 * sizeof(CUSTOMVERTEX), (BYTE**)&p, 0);
	
	memcpy(p, pVertices, sizeof(pVertices));
   
	pVertexBuffer->Unlock();

	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	pDevice->SetStreamSource(0, pVertexBuffer, sizeof(CUSTOMVERTEX));
    pDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
    pDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

	pVertexBuffer->Release();
}


void wire_cube(IDirect3DDevice8* pDevice, float* minxyz, float*maxxyz, D3DXMATRIX* m /*=NULL*/)
{
	D3DXMATRIX matWorld;
	if( m == NULL )	
		D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
	else
		matWorld = *m;

	struct CUSTOMVERTEX
	{
		FLOAT x, y, z; // The transformed position for the vertex.
		DWORD colour; // The vertex colour.
	};
	UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_DIFFUSE;

	struct stVert {float x, y, z;};

	stVert* min = (stVert*)minxyz;
	stVert* max = (stVert*)maxxyz;


	float width  = max->x - min->x;
	float height = max->y - min->y;
	float depth  = max->z - min->z;
		
	float x = min->x;
	float y = min->y;
	float z = min->z;


	IDirect3DVertexBuffer8* pVertexBuffer = NULL;
	//** BUG ** BUG ** //
	// Should be 24 * sizeof(CUSTOMVERTEX)
	pDevice->CreateVertexBuffer(24 * sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
									D3DPOOL_DEFAULT, &pVertexBuffer);
	
	CUSTOMVERTEX pVertices[24] = 
	{
		{x,      y        ,z      ,0xff00ffff}, { x+width ,y        ,z      ,0xff00ffff},
		{x,      y+height ,z      ,0xff00ffff}, { x+width ,y+height ,z      ,0xff00ffff},

		{x+width,y        ,z      ,0xff00ffff}, {x+width  ,y+height ,z      ,0xff00ffff},
		{x,      y        ,z      ,0xff00ffff}, {x        ,y+height ,z      ,0xff00ffff},

		{x,      y        ,z+depth,0xff00ffff}, { x+width ,y        ,z+depth,0xff00ffff},
		{x,      y+height ,z+depth,0xff00ffff}, { x+width ,y+height ,z+depth,0xff00ffff},
		{x+width,y        ,z+depth,0xff00ffff}, {x+width  ,y+height ,z+depth,0xff00ffff},
		{x,      y        ,z+depth,0xff00ffff}, {x        ,y+height ,z+depth,0xff00ffff},


		{x,      y        ,z      ,0xff00ffff}, { x       ,y        ,z+depth,0xff00ffff},
		{x,      y+height ,z      ,0xff00ffff}, { x       ,y+height ,z+depth,0xff00ffff},

		{x+width,y        ,z      ,0xff00ffff}, { x+width ,y        ,z+depth,0xff00ffff},
		{x+width,y+height ,z      ,0xff00ffff}, { x+width ,y+height ,z+depth,0xff00ffff},
	};
	

	void* p;
	pVertexBuffer->Lock(0, 24 * sizeof(CUSTOMVERTEX), (BYTE**)&p, 0);
	
	memcpy(p, pVertices, sizeof(pVertices));
   

	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	pVertexBuffer->Unlock();
	pDevice->SetStreamSource(0, pVertexBuffer, sizeof(CUSTOMVERTEX));
    pDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
    pDevice->DrawPrimitive(D3DPT_LINELIST, 0, 12);

	pVertexBuffer->Release();
	
}




// Draws a primitive circle in the x and y axis....you only need to
// provide a center and radius.

void circle(IDirect3DDevice8* pDevice, float c_x, float c_y, float c_z, float r, D3DXMATRIX* m /*=NULL*/)
{
	D3DXMATRIX matWorld;
	if( m == NULL )	
		D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
	else
		matWorld = *m;


	struct CUSTOMVERTEX
	{
		FLOAT x, y, z; // The transformed position for the vertex.
		DWORD colour;  // The vertex colour.
	};
	UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_DIFFUSE;

	unsigned int iDetail = 20;

	IDirect3DVertexBuffer8* pVertexBuffer = NULL;
	pDevice->CreateVertexBuffer(iDetail * sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
									D3DPOOL_DEFAULT, &pVertexBuffer);

	CUSTOMVERTEX* pV = new CUSTOMVERTEX[iDetail];
	float c=0.0f;
	float step = (2*3.14f)/(iDetail-1);

	for(unsigned int i=0; i< iDetail; i++ )
	{
		pV[i].x = c_x + r*sinf(c);
		pV[i].y = c_y + r*cosf(c);
		pV[i].z = c_z;	
		pV[i].colour = 0xff0f00ff;

		c += step;
	}
	// Join the last line to the first line to close our circle
	pV[iDetail-1].x = c_x + r*sinf(0.0f);	
	pV[iDetail-1].y = c_y + r*cosf(0.0f);	
	pV[iDetail-1].z = c_z;	
	pV[iDetail-1].colour = 0xff0f00ff;
	

	void* p;
	pVertexBuffer->Lock(0, iDetail * sizeof(CUSTOMVERTEX), (BYTE**)&p, 0);
	
	memcpy(p, pV, iDetail * sizeof(CUSTOMVERTEX));
   
	pVertexBuffer->Unlock();
	
	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	pDevice->SetStreamSource(0, pVertexBuffer, sizeof(CUSTOMVERTEX));
    pDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
    pDevice->DrawPrimitive(D3DPT_LINESTRIP, 0, iDetail-1);

	delete[] pV;
	pVertexBuffer->Release();

}



//--------------------------------------------------------------------------------//

double arrow_array[] =
{

 0.129958,0.151901,-9.464804,1.413031,1.734449,-9.464804,2.187846,0.151901,-9.464804
,0.129958,0.151901,-9.464804,-0.327965,2.125306,-9.464804,1.413031,1.734449,-9.464804
,0.129958,0.151901,-9.464804,-1.724135,1.030149,-9.464804,-0.327965,2.125306,-9.464804
,0.129958,0.151901,-9.464804,-1.724135,-0.726346,-9.464804,-1.724135,1.030149,-9.464804
,0.129958,0.151901,-9.464804,-0.327965,-1.821503,-9.464804,-1.724135,-0.726346,-9.464804
,0.129958,0.151901,-9.464804,1.413031,-1.430646,-9.464804,-0.327965,-1.821503,-9.464804
,0.129958,0.151901,-9.464804,2.187846,0.151901,-9.464804,1.413031,-1.430646,-9.464804
,2.187846,0.151901,-9.464804,1.413031,1.734449,-6.131471,2.187846,0.151901,-6.131471
,2.187846,0.151901,-9.464804,1.413031,1.734449,-9.464804,1.413031,1.734449,-6.131471
,1.413031,1.734449,-9.464804,-0.327965,2.125306,-6.131471,1.413031,1.734449,-6.131471
,1.413031,1.734449,-9.464804,-0.327965,2.125306,-9.464804,-0.327965,2.125306,-6.131471
,-0.327965,2.125306,-9.464804,-1.724135,1.030149,-6.131471,-0.327965,2.125306,-6.131471
,-0.327965,2.125306,-9.464804,-1.724135,1.030149,-9.464804,-1.724135,1.030149,-6.131471
,-1.724135,1.030149,-9.464804,-1.724135,-0.726346,-6.131471,-1.724135,1.030149,-6.131471
,-1.724135,1.030149,-9.464804,-1.724135,-0.726346,-9.464804,-1.724135,-0.726346,-6.131471
,-1.724135,-0.726346,-9.464804,-0.327965,-1.821503,-6.131471,-1.724135,-0.726346,-6.131471
,-1.724135,-0.726346,-9.464804,-0.327965,-1.821503,-9.464804,-0.327965,-1.821503,-6.131471
,-0.327965,-1.821503,-9.464804,1.413031,-1.430646,-6.131471,-0.327965,-1.821503,-6.131471
,-0.327965,-1.821503,-9.464804,1.413031,-1.430646,-9.464804,1.413031,-1.430646,-6.131471
,1.413031,-1.430646,-9.464804,2.187846,0.151901,-6.131471,1.413031,-1.430646,-6.131471
,1.413031,-1.430646,-9.464804,2.187846,0.151901,-9.464804,2.187846,0.151901,-6.131471
,2.187846,0.151901,-6.131471,1.413031,1.734449,-2.798137,2.187846,0.151901,-2.798137
,2.187846,0.151901,-6.131471,1.413031,1.734449,-6.131471,1.413031,1.734449,-2.798137
,1.413031,1.734449,-6.131471,-0.327965,2.125306,-2.798137,1.413031,1.734449,-2.798137
,1.413031,1.734449,-6.131471,-0.327965,2.125306,-6.131471,-0.327965,2.125306,-2.798137
,-0.327965,2.125306,-6.131471,-1.724135,1.030149,-2.798137,-0.327965,2.125306,-2.798137
,-0.327965,2.125306,-6.131471,-1.724135,1.030149,-6.131471,-1.724135,1.030149,-2.798137
,-1.724135,1.030149,-6.131471,-1.724135,-0.726346,-2.798137,-1.724135,1.030149,-2.798137
,-1.724135,1.030149,-6.131471,-1.724135,-0.726346,-6.131471,-1.724135,-0.726346,-2.798137
,-1.724135,-0.726346,-6.131471,-0.327965,-1.821503,-2.798137,-1.724135,-0.726346,-2.798137
,-1.724135,-0.726346,-6.131471,-0.327965,-1.821503,-6.131471,-0.327965,-1.821503,-2.798137
,-0.327965,-1.821503,-6.131471,1.413031,-1.430646,-2.798137,-0.327965,-1.821503,-2.798137
,-0.327965,-1.821503,-6.131471,1.413031,-1.430646,-6.131471,1.413031,-1.430646,-2.798137
,1.413031,-1.430646,-6.131471,2.187846,0.151901,-2.798137,1.413031,-1.430646,-2.798137
,1.413031,-1.430646,-6.131471,2.187846,0.151901,-6.131471,2.187846,0.151901,-2.798137
,2.187846,0.151901,-2.798137,1.413031,1.734449,0.535196,2.187846,0.151901,0.535196
,2.187846,0.151901,-2.798137,1.413031,1.734449,-2.798137,1.413031,1.734449,0.535196
,1.413031,1.734449,-2.798137,-0.327965,2.125306,0.535196,1.413031,1.734449,0.535196
,1.413031,1.734449,-2.798137,-0.327965,2.125306,-2.798137,-0.327965,2.125306,0.535196
,-0.327965,2.125306,-2.798137,-1.724135,1.030149,0.535196,-0.327965,2.125306,0.535196
,-0.327965,2.125306,-2.798137,-1.724135,1.030149,-2.798137,-1.724135,1.030149,0.535196
,-1.724135,1.030149,-2.798137,-1.724135,-0.726346,0.535196,-1.724135,1.030149,0.535196
,-1.724135,1.030149,-2.798137,-1.724135,-0.726346,-2.798137,-1.724135,-0.726346,0.535196
,-1.724135,-0.726346,-2.798137,-0.327965,-1.821503,0.535196,-1.724135,-0.726346,0.535196
,-1.724135,-0.726346,-2.798137,-0.327965,-1.821503,-2.798137,-0.327965,-1.821503,0.535196
,-0.327965,-1.821503,-2.798137,1.413031,-1.430646,0.535196,-0.327965,-1.821503,0.535196
,-0.327965,-1.821503,-2.798137,1.413031,-1.430646,-2.798137,1.413031,-1.430646,0.535196
,1.413031,-1.430646,-2.798137,2.187846,0.151901,0.535196,1.413031,-1.430646,0.535196
,1.413031,-1.430646,-2.798137,2.187846,0.151901,-2.798137,2.187846,0.151901,0.535196
,0.129958,0.151901,0.535196,2.187846,0.151901,0.535196,1.413031,1.734449,0.535196
,0.129958,0.151901,0.535196,1.413031,1.734449,0.535196,-0.327965,2.125306,0.535196
,0.129958,0.151901,0.535196,-0.327965,2.125306,0.535196,-1.724135,1.030149,0.535196
,0.129958,0.151901,0.535196,-1.724135,1.030149,0.535196,-1.724135,-0.726346,0.535196
,0.129958,0.151901,0.535196,-1.724135,-0.726346,0.535196,-0.327965,-1.821503,0.535196
,0.129958,0.151901,0.535196,-0.327965,-1.821503,0.535196,1.413031,-1.430646,0.535196
,0.129958,0.151901,0.535196,1.413031,-1.430646,0.535196,2.187846,0.151901,0.535196
,0.205180,0.068081,0.670792,3.524361,3.387262,0.670792,4.899211,0.068081,0.670792
,0.205180,0.068081,0.670792,0.205180,4.762112,0.670792,3.524361,3.387262,0.670792
,0.205180,0.068081,0.670792,-3.114001,3.387262,0.670792,0.205180,4.762112,0.670792
,0.205180,0.068081,0.670792,-4.488851,0.068080,0.670792,-3.114001,3.387262,0.670792
,0.205180,0.068081,0.670792,-3.114001,-3.251101,0.670792,-4.488851,0.068080,0.670792
,0.205180,0.068081,0.670792,0.205181,-4.625950,0.670792,-3.114001,-3.251101,0.670792
,0.205180,0.068081,0.670792,3.524361,-3.251100,0.670792,0.205181,-4.625950,0.670792
,0.205180,0.068081,0.670792,4.899211,0.068081,0.670792,3.524361,-3.251100,0.670792
,4.899211,0.068081,0.670792,1.864770,1.727671,5.670792,2.552195,0.068081,5.670792
,4.899211,0.068081,0.670792,3.524361,3.387262,0.670792,1.864770,1.727671,5.670792
,3.524361,3.387262,0.670792,0.205180,2.415096,5.670792,1.864770,1.727671,5.670792
,3.524361,3.387262,0.670792,0.205180,4.762112,0.670792,0.205180,2.415096,5.670792
,0.205180,4.762112,0.670792,-1.454411,1.727671,5.670792,0.205180,2.415096,5.670792
,0.205180,4.762112,0.670792,-3.114001,3.387262,0.670792,-1.454411,1.727671,5.670792
,-3.114001,3.387262,0.670792,-2.141835,0.068081,5.670792,-1.454411,1.727671,5.670792
,-3.114001,3.387262,0.670792,-4.488851,0.068080,0.670792,-2.141835,0.068081,5.670792
,-4.488851,0.068080,0.670792,-1.454410,-1.591510,5.670792,-2.141835,0.068081,5.670792
,-4.488851,0.068080,0.670792,-3.114001,-3.251101,0.670792,-1.454410,-1.591510,5.670792
,-3.114001,-3.251101,0.670792,0.205180,-2.278934,5.670792,-1.454410,-1.591510,5.670792
,-3.114001,-3.251101,0.670792,0.205181,-4.625950,0.670792,0.205180,-2.278934,5.670792
,0.205181,-4.625950,0.670792,1.864771,-1.591509,5.670792,0.205180,-2.278934,5.670792
,0.205181,-4.625950,0.670792,3.524361,-3.251100,0.670792,1.864771,-1.591509,5.670792
,3.524361,-3.251100,0.670792,2.552195,0.068081,5.670792,1.864771,-1.591509,5.670792
,3.524361,-3.251100,0.670792,4.899211,0.068081,0.670792,2.552195,0.068081,5.670792
,2.552195,0.068081,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,2.552195,0.068081,5.670792,1.864770,1.727671,5.670792,0.205180,0.068081,10.670792
,1.864770,1.727671,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,1.864770,1.727671,5.670792,0.205180,2.415096,5.670792,0.205180,0.068081,10.670792
,0.205180,2.415096,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,2.415096,5.670792,-1.454411,1.727671,5.670792,0.205180,0.068081,10.670792
,-1.454411,1.727671,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,-1.454411,1.727671,5.670792,-2.141835,0.068081,5.670792,0.205180,0.068081,10.670792
,-2.141835,0.068081,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,-2.141835,0.068081,5.670792,-1.454410,-1.591510,5.670792,0.205180,0.068081,10.670792
,-1.454410,-1.591510,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,-1.454410,-1.591510,5.670792,0.205180,-2.278934,5.670792,0.205180,0.068081,10.670792
,0.205180,-2.278934,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,-2.278934,5.670792,1.864771,-1.591509,5.670792,0.205180,0.068081,10.670792
,1.864771,-1.591509,5.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,1.864771,-1.591509,5.670792,2.552195,0.068081,5.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792
,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792,0.205180,0.068081,10.670792

};

void arrow(IDirect3DDevice8* pDevice, D3DXVECTOR3 vPos, D3DXVECTOR3 vDir, D3DXMATRIX* m/*=NULL*/)
{

	D3DXMATRIX matScale;
	D3DXMatrixScaling(&matScale, 1.0f, 1.0f, 1.0f);
	D3DXMATRIX matPos;
	D3DXMatrixTranslation(&matPos, vPos.x, vPos.y, vPos.z);
	D3DXMATRIX matRotY;


	D3DXVec3Normalize(&vDir, &vDir);
	D3DXVECTOR3 vRefDirection(0.0f, 0.0f, 1.0f);
	float yAngle = D3DXVec3Dot(&vRefDirection, &vDir);
	yAngle = acosf(yAngle);

	//yAngle -= 3.14f/4.0f;
	if( vDir.x < 0 )
			yAngle = -yAngle;

	D3DXMatrixRotationY(&matRotY, yAngle);

	D3DXMATRIX matWorld = matScale;
	D3DXMatrixMultiply(&matWorld, &matWorld, &matRotY);
	D3DXMatrixMultiply(&matWorld, &matWorld, &matPos);
	 

	struct CUSTOMVERTEX
	{
		FLOAT x, y, z; // The transformed position for the vertex.
		DWORD colour; // The vertex colour.
	};
	UINT D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_DIFFUSE;

	int iNumVertices = sizeof(arrow_array)/sizeof(double);
	iNumVertices /= 3;
	int iNumTriangles = iNumVertices/3;


	IDirect3DVertexBuffer8* pVertexBuffer = NULL;
	pDevice->CreateVertexBuffer(iNumVertices * sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
									D3DPOOL_DEFAULT, &pVertexBuffer);

	CUSTOMVERTEX* p;
	pVertexBuffer->Lock(0, iNumVertices * sizeof(CUSTOMVERTEX), (BYTE**)&p, 0);
	

	int index = 0;
	for(int i=0; i<iNumVertices; i++)
	{
		p[i].x = (float)arrow_array[index++];
		p[i].y = (float)arrow_array[index++];
		p[i].z = (float)arrow_array[index++];
		p[i].colour = 0xffff0000 + index ;
	}

   
	pVertexBuffer->Unlock();

	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);


	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	pDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	pDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE );

	pDevice->SetStreamSource(0, pVertexBuffer, sizeof(CUSTOMVERTEX));
    pDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
    pDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, iNumTriangles);

	pVertexBuffer->Release();
}









