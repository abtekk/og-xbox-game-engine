/***************************************************************************/
/*                                                                         */
/*  File: screenshot.cpp                                                   */
/*  URL: www.xfactordev.net                                                */
/*  Author: Ben_3D@xfactordev.net                                          */
/*  version: 1.0.0                                                         */
/*                                                                         */
/***************************************************************************/
/*

  So how does this screensaver functions work?

  It can't be simpler...you include the header file in your code...e.g:
  #include "screenshot.h"

  Then part two, you call it...and it saves a file for you on the hard drive
  a .bmp file...e.g.

  screenshot( g_pD3DDevice, "D:\\Demo.bmp");

  Thats it ;)



*/
/***************************************************************************/

#include "screenshot.h"


#include <xgraphics.h>
#pragma comment(lib, "xgraphics.lib")



void screenshot( IDirect3DDevice8* pD3DDevice, char* szFileName )
{
	if( szFileName == NULL )
		szFileName = "D:\\Demo.bmp";

	static float delay = 0.0f;

	float nowTime = (float)GetTickCount();


	// This nowTime variable is so there is a 5 second delay between possible image
	// grabs...
	if( nowTime > (delay + 5000))
	{
		delay = nowTime;


		IDirect3DSurface8* pSurface;
	
		pD3DDevice->GetBackBuffer(-1,D3DBACKBUFFER_TYPE_MONO,&pSurface);

		/********************method -1-*********************************/
		
		XGWriteSurfaceToFile( pSurface, szFileName );

		pSurface->Release();
		
		/***************************************************************/


	}

}

