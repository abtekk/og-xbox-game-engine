/***************************************************************************/
/*                                                                         */
/*  File: screenshot.h                                                     */
/*  URL: www.xfactordev.net                                                */
/*  Author: Ben_3D@xfactordev.net                                          */
/*  version: 1.0.0                                                         */
/*                                                                         */
/***************************************************************************/

#ifndef _XF_SCREENSHOT_H_
#define _XF_SCREENSHOT_H_


#include <xtl.h>
#include <stdio.h>




void screenshot( IDirect3DDevice8* pD3DDevice, char* szFileName = NULL );





#endif // _XF_SCREENSHOT_H_