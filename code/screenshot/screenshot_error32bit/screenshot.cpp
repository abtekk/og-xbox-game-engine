/***************************************************************************/
/*                                                                         */
/*  File: screenshot.cpp                                                   */
/*  URL: www.xfactordev.net                                                */
/*  Author: Ben_3D@xfactordev.net                                          */
/*  version: 1.0.0                                                         */
/*                                                                         */
/***************************************************************************/
/*

  So how does this screensaver functions work?

  It can't be simpler...you include the header file in your code...e.g:
  #include "screenshot.h"

  Then part two, you call it...and it saves a file for you on the hard drive
  a .bmp file...e.g.

  screenshot( g_pD3DDevice, "D:\\Demo.bmp");

  Thats it ;)



*/
/***************************************************************************/

#include "screenshot.h"


#include <xgraphics.h>
#pragma comment(lib, "xgraphics.lib")

struct BITMAPINFOHEADERX{
    unsigned int  biSize;
    unsigned int   biWidth;
    unsigned int   biHeight;
    unsigned short   biPlanes;
    unsigned short   biBitCount;
    unsigned int  biCompression;
    unsigned int  biSizeImage;
    unsigned int   biXPelsPerMeter;
    unsigned int   biYPelsPerMeter;
    unsigned int  biClrUsed;
    unsigned int  biClrImportant; // 40
};

struct BITMAPFILEHEADERX {
  unsigned short    bfType;
  unsigned int      bfSize;
  unsigned short    bfReserved1;
  unsigned short    bfReserved2;
  unsigned int   bfOffBits;
}; // 14


void screenshot( IDirect3DDevice8* pD3DDevice, char* szFileName )
{
	if( szFileName == NULL )
		szFileName = "D:\\Demo.bmp";

	static float delay = 0.0f;

	float nowTime = (float)GetTickCount();


	if( nowTime > (delay + 5000))
	{
		delay = nowTime;


		D3DDISPLAYMODE dm;
		dm.Width = dm.Height = 0;
		dm.Width = 640;
		dm.Height = 480;


		IDirect3DSurface8* pSurface;
		pD3DDevice->CreateImageSurface( dm.Width,dm.Height,D3DFMT_LIN_X8R8G8B8,&pSurface); //D3DFMT_X8R8G8B8, D3DFMT_A8R8G8B8


		pD3DDevice->GetBackBuffer(-1,D3DBACKBUFFER_TYPE_MONO,&pSurface);

		/********************method -1-*********************************/
		/*
		XGWriteSurfaceToFile( pSurface, "D:\\demo.bmp" );

		pSurface->UnlockRect();
		pSurface->Release();
		*/
		/********************mothod -2-*********************************/

		LPBYTE Bits = NULL;
		//Bits = new BYTE[dm.Width * dm.Height * 3 + 1];
		Bits = new BYTE[dm.Width * dm.Height * 4 + 1];


		D3DLOCKED_RECT lockedrect;
		::ZeroMemory(&lockedrect, sizeof(lockedrect));

		

		pSurface->LockRect(&lockedrect, NULL, D3DLOCK_READONLY );



		struct stRGB { unsigned char r,g,b,a;};

		stRGB* lpDest = (stRGB*)Bits;
		//unsigned char* lpDest = (unsigned char*)Bits;

		unsigned char* lpSrc;
		lpSrc = (unsigned char*)lockedrect.pBits;

		int width  = dm.Width;
		int height = dm.Height;

		int index = 0;
		int temp = 0;
		for (int y=height-1; y>=0; y--)
		{
			//for ( int x = 0; x < width-1; x++)
			for ( int x = 0; x <= width-1; x++)
			{
					lpDest[(y*width) + x].r = lpSrc[index++];
					lpDest[(y*width) + x].g = lpSrc[index++];
					lpDest[(y*width) + x].b = lpSrc[index++];
					lpDest[(y*width) + x].a = lpSrc[index++];	

			}
			//index += lockedrect.Pitch - (dm.Width*4);
		}


		pSurface->UnlockRect();
		pSurface->Release();




		// prepare the bitmap info header
		BITMAPINFOHEADERX bmih;
		bmih.biSize = sizeof(bmih);
		bmih.biWidth = dm.Width;
		bmih.biHeight = dm.Height;
		bmih.biPlanes = 1;
		//bmih.biBitCount = 24;
		bmih.biBitCount = 32;
		bmih.biCompression = 0; // Non Compressed
 		//bmih.biSizeImage = dm.Width * dm.Height * 3;
		bmih.biSizeImage = dm.Width * dm.Height * 4;
		bmih.biXPelsPerMeter = 0;
		bmih.biYPelsPerMeter = 0;
		bmih.biClrUsed = 0;
		bmih.biClrImportant = 0;

		// Saving to file (bmp)
		// prepare the bitmap file header
		BITMAPFILEHEADERX bmfh;
		bmfh.bfType = 'MB';//BITMAP_FILE_SIGNATURE;
		bmfh.bfSize = sizeof(bmfh) + sizeof(bmih) + bmih.biSizeImage;
		bmfh.bfReserved1 = bmfh.bfReserved2 = 0;
		bmfh.bfOffBits = 14 /*sizeof(bmfh)*/ + sizeof(bmih);



		// create the BMP file
		FILE* f = fopen( szFileName, "wb");


		// dump the file header
		fwrite(reinterpret_cast<void*>(&bmfh.bfType),      1, sizeof(bmfh.bfType), f);
		fwrite(reinterpret_cast<void*>(&bmfh.bfSize),      1, sizeof(bmfh.bfSize), f);
		fwrite(reinterpret_cast<void*>(&bmfh.bfReserved1), 1, sizeof(bmfh.bfReserved1), f);
		fwrite(reinterpret_cast<void*>(&bmfh.bfReserved2), 1, sizeof(bmfh.bfReserved2), f);
		fwrite(reinterpret_cast<void*>(&bmfh.bfOffBits),   1, sizeof(bmfh.bfOffBits), f);
		// dump the info header
		unsigned char* pHeader = (unsigned char*)&bmih;
		for(int ii=0; ii< sizeof(bmih); ii++)
		{
			fwrite( pHeader, 1, 1, f);
			pHeader++;
		}
		
		unsigned char* pBits = Bits;
		// dump the bitmap bits
		for(int ii=0; ii< bmih.biSizeImage; ii++)
		{
		fwrite( pBits , 1,1, f);
		pBits++;
		}

		// close the file
		fclose(f);

		delete[] Bits;
	}


}

