/***************************************************************************/
/*                                                                         */
/*  File:    collision_functions.h                                         */
/*                                                                         */
/*  Details: Put the collision algorithms in here                          */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/



#ifndef _XF_COLLISION_FUNCTIONS_H
#define _XF_COLLISION_FUNCTIONS_H

#include <xtl.h>

#include "../graphics_helpers/graphics_helpers.h"


bool SpherePlaneIntersect(D3DXVECTOR3* vCenter, float fRadius, D3DXVECTOR3 vPoly[],
						  D3DXVECTOR3* vNormal, float* fDistance);

bool PointInsideTriangle(D3DXVECTOR3* vPoint, D3DXVECTOR3 vPoly[], D3DXVECTOR3 vNormal, float fDistance);

D3DXVECTOR3 ClosestPoint(D3DXVECTOR3 vA, D3DXVECTOR3 vB, D3DXVECTOR3 vPoint);

bool SphereEdgeCollision(float* center, float radius,float* v1, float* v2, float* v3);

bool SphereToTriangleCollision(D3DXVECTOR3* center, float radius, D3DXVECTOR3* p1, D3DXVECTOR3* p2, D3DXVECTOR3* p3);



bool TriangeToLineCollision( float* v1, float* v2, float* v3,
							 float* p1, float* p2);



bool BoundingBox(D3DXVECTOR3* minA, D3DXVECTOR3* maxA, D3DXVECTOR3* minB, D3DXVECTOR3* maxB, D3DXMATRIX* matrixA, D3DXMATRIX* matrixB);




#endif // _XF_COLLISION_FUNCTIONS_H



