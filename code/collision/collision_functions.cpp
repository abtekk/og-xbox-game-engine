/***************************************************************************/
/*                                                                         */
/*  File:    collision_functions.cpp                                       */
/*                                                                         */
/*  Details: Put the collision algorithms in here                          */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#include "collision_functions.h"


/***************************************************************************/
/*                                                                         */
/*                  SPHERE VS TRIANGLE COLLISION                           */
/*                                                                         */
/*                                                                         */
/***************************************************************************/

float abs_float(float p)
{
	if(p > 0.0f)
		return p;
	if(p < 0.0f)
		return -p;
	return 0.0f;
}



bool SpherePlaneIntersect(D3DXVECTOR3* vCenter, float fRadius, D3DXVECTOR3 vPoly[],
						  D3DXVECTOR3* vNormal, float* fDistance)
{
	D3DXVECTOR3 a = vPoly[2] - vPoly[0];
	D3DXVECTOR3 b = vPoly[1] - vPoly[0];

	D3DXVec3Normalize(&a, &a);
	D3DXVec3Normalize(&b, &b);

	D3DXVec3Cross( vNormal, &a, &b);

	// Use Ax + By + Cz + D = 0
	// ABC is Normal, xyz is the point.
	float A = vNormal->x; 
	float B = vNormal->y; 
	float C = vNormal->z; 
	// Just select any point on the triangle plane, e.g. the first one.
	float x = vPoly[0].x; 
	float y = vPoly[0].y; 
	float z = vPoly[0].z; 

	// Ax + By + Cz + D = 0...re-arrange this to get: D= -(Ax+By+Cz);
	// use vPoly[0] as the reference for x,y,z in the equation
	float DistanceFromOrigin = (-A*x - B*y - C*z);

	*fDistance = A*vCenter->x + B*vCenter->y + C*vCenter->z + DistanceFromOrigin;

	if( abs_float(*fDistance) < fRadius )
		return true;

	return false;
}

float Magnitude_f(D3DXVECTOR3* vVec)
{
	return sqrtf(vVec->x*vVec->x + vVec->y*vVec->y + vVec->z*vVec->z);
}

// This function, see's if the sphere center is inside the triangle (where the center point
// must be on the same plane).  (A good way to picture this is do a sketch in 2D with a
// triangle and a X point.

bool PointInsideTriangle(D3DXVECTOR3* vPoint, D3DXVECTOR3 vPoly[], D3DXVECTOR3 vNormal, float fDistance)
{
	// fDisance is the distance between our point and our plane...we multiply this by our Plane
	// normal to get a vector of length from the point to the plane.
	D3DXVECTOR3 vOffsetPoint = vNormal * fDistance;;
	
	// Now our 3d point is past our plane, as its intersecting with it...so we need to cut of the
	// overflow...so we take away this offset value...so our point, is exactly on the plane.
	D3DXVECTOR3 vPointOnPlane; 
	D3DXVec3Subtract(&vPointOnPlane, vPoint, &vOffsetPoint);

	double Angle = 0.0;

	for(int i=0; i<3; i++)
	{
		D3DXVECTOR3 vA = vPoly[i] - vPointOnPlane;
		D3DXVECTOR3 vB = vPoly[(i+1)%3] - vPointOnPlane;

		float d = D3DXVec3Dot(&vA, &vB);

		float mag = Magnitude_f(&vA) * Magnitude_f(&vB);
		
		double tempAngle = acos( d / mag );

		if( _isnan(tempAngle) )
			tempAngle = 0;

		Angle += tempAngle;
	}

	if( Angle >= (2 * 3.14) )
		return true;


	return false;
}

// We are given two points... now we also have a 3rd point...but which is the
// closest?  Well we
D3DXVECTOR3 ClosestPoint(D3DXVECTOR3 vA, D3DXVECTOR3 vB, D3DXVECTOR3 vPoint)
{
	D3DXVECTOR3 v1 = vPoint - vA;
	D3DXVECTOR3 v2 = vB - vA;
	D3DXVec3Normalize(&v2, &v2);

	// Distance = sqrt(  (P2.x - P1.x)^2 + (P2.y - P1.y)^2 + (P2.z - P1.z)^2 )
	float distance = sqrtf( (vB.x-vA.x)*(vB.x-vA.x) + 
							(vB.y-vA.y)*(vB.y-vA.y) +
							(vB.z-vA.z)*(vB.z-vA.z) );

	float t = D3DXVec3Dot(&v2, &v1);

	if (t <= 0) 
		return vA;

	if (t >= distance) 
		return vB;

	D3DXVECTOR3 v3 = v2 * t;

	D3DXVECTOR3 vClosestPoint = vA + v3;

	return vClosestPoint;

}

// This is a great function... "sphere_edge_collision(..)"....it sort of does what the
// name says on the packet... Its job is to see if the edge of the sphere is inside
// the triangle.
// First we do combinations of the triangle sides...picking 2 at a time....then we
// determine the closest point between those to triangle points (using the
// ClosestPoint(..) function.
// Once we know the closest point, we just determine the distance between it and our
// sphere center, and if its less than the radius.

bool SphereEdgeCollision(D3DXVECTOR3* vCenter, float fRadius, D3DXVECTOR3* v1, D3DXVECTOR3* v2, D3DXVECTOR3* v3)
{
	D3DXVECTOR3 vPoly[3] = {*v1, *v2, *v3 };

	for(int i=0; i<3; i++)
	{
		D3DXVECTOR3 vPoint = ClosestPoint(vPoly[i], vPoly[(i + 1)%3], *vCenter);

		// Distance = sqrt(  (P2.x - P1.x)^2 + (P2.y - P1.y)^2 + (P2.z - P1.z)^2 )
		float d = sqrtf( (vCenter->x-vPoint.x)*(vCenter->x-vPoint.x)+
						 (vCenter->y-vPoint.y)*(vCenter->y-vPoint.y)+
						 (vCenter->z-vPoint.z)*(vCenter->z-vPoint.z) );

		if( d < fRadius)
			return true;
	}
	return false;
}

// "SphereToTriangleCollision(..)" Function
// Well its made up of 3 essential parts!...we first see if our sphere is colliding
// with the triangles plane, if so, we go to the next step, else no collision.
// If its intersecting our plane, we go onto the next step...step 2...which
// will see if your center (the middle of the sphere) is inside the triangle's
// plane.
// Finally...the ultimate test...if we got here, we need to see if the triangles
// edge intersected the sphere's edge.
bool SphereToTriangleCollision(D3DXVECTOR3* vCenter, float fRadius, D3DXVECTOR3* p1, D3DXVECTOR3* p2, D3DXVECTOR3* p3)
{
	D3DXVECTOR3 vPoly[3] = {*p1, *p2, *p3 };
	D3DXVECTOR3 vNormal;

	float distance = 0.0f;
	bool bIntersect = SpherePlaneIntersect(vCenter, fRadius, vPoly, &vNormal, &distance);

	if( bIntersect	)
	{
		if( PointInsideTriangle(vCenter, vPoly, vNormal, distance) )
		{
			return true;
		}
		else
		{
			if( SphereEdgeCollision(vCenter, fRadius,  p1, p2, p3) )
				return true;
		}
	}

	return false;
}








/***************************************************************************/
/*                                                                         */
/*                    LINE VS TRIANGLE COLLISION                           */
/*                                                                         */
/*                                                                         */
/***************************************************************************/



// 3 arrays of xyz for the 3 triangle points, start and end float array xyz
	
bool TriangeToLineCollision( float* v1, float* v2, float* v3,
					float* p1, float* p2)
	{
		float distance1=0, distance2=0;

		D3DXVECTOR3 vPoly[3] = {D3DXVECTOR3(v1[0], v1[1], v1[2]), 
								D3DXVECTOR3(v2[0], v2[1], v2[2]),
								D3DXVECTOR3(v3[0], v3[1], v3[2]) };
		D3DXVECTOR3 a = vPoly[2] - vPoly[0];
		D3DXVECTOR3 b = vPoly[1] - vPoly[0];

		D3DXVec3Normalize(&a, &a);
		D3DXVec3Normalize(&b, &b);

		D3DXVECTOR3 cross_a_b;
		D3DXVec3Cross(&cross_a_b, &a, &b);

		// use Ax + By + Cz + D = 0
		float A = cross_a_b.x;
		float B = cross_a_b.y;
		float C = cross_a_b.z;

		float x = v1[0];
		float y = v1[1];
		float z = v1[2];

		// use vPoly[0] as the reference for x,y,z in the equation
		float DistanceFromOrigin = (-A*x - B*y - C*z);

		distance1 = A*p1[0] + B*p1[1] + C*p1[2] + DistanceFromOrigin;

		distance2 = A*p2[0] + B*p2[1] + C*p2[2] + DistanceFromOrigin;

		if( distance1 * distance2 >= 0 )
			return false;

		//return true;
		
		D3DXVECTOR3 vLineDirection = D3DXVECTOR3(p1[0],p1[1],p1[2])-D3DXVECTOR3(p2[0],p2[1],p2[2]);
		D3DXVec3Normalize(&vLineDirection, &vLineDirection);

		float Numerator = -(A*p1[0] + B*p1[1] + C*p1[2] + DistanceFromOrigin);

		float Denominator = D3DXVec3Dot( &cross_a_b, &vLineDirection );

		if( Denominator == 0.0f )
			return false;

		float dist = Numerator / Denominator;

		D3DXVECTOR3 vPoint;
		vPoint.x = p1[0] + (vLineDirection.x*dist);
		vPoint.y = p1[1] + (vLineDirection.y*dist);
		vPoint.z = p1[2] + (vLineDirection.z*dist);

		double Angle = 0.0;

		for(int i=0; i<3; i++)
		{
			D3DXVECTOR3 vA = vPoly[i] - vPoint;
			D3DXVECTOR3 vB = vPoly[(i+1)%3] - vPoint;

			float d = D3DXVec3Dot(&vA, &vB);

			float mag = (float)sqrtf((vA.x*vA.x + vA.y*vA.y + vA.z*vA.z)) * sqrtf((vB.x*vB.x + vB.y*vB.y + vB.z*vB.z));
			
			double tempAngle = acos( d / mag );

			if( _isnan(tempAngle) )
				tempAngle = 0;

			Angle += tempAngle;
		}

		if( Angle > (2 * 3.13) )
			return true;

		return false;

	}







/***************************************************************************/
/*                                                                         */
/*                       BOUNDING BOX COLLISION                            */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
// Returns true if there was a collision, false for no collision.

bool BoundingBox(	D3DXVECTOR3* minA, D3DXVECTOR3* maxA, 
					D3DXVECTOR3* minB, D3DXVECTOR3* maxB, 
					D3DXMATRIX* matrixA, D3DXMATRIX* matrixB )
{

	D3DXVECTOR3 vMinA, vMaxA;
	D3DXVECTOR3 vMinB, vMaxB;

	D3DXVec3TransformCoord( &vMinA, minA, matrixA);
	D3DXVec3TransformCoord( &vMaxA, maxA, matrixA);

	D3DXVec3TransformCoord( &vMinB, minB, matrixB);
	D3DXVec3TransformCoord( &vMaxB, maxB, matrixB);

	// These two lines where used in debugging... and would draw a line from
	// one corner to the other of each cube.
	//line(m_pD3DDevice, (float*)&vMinA, (float*)&vMaxA);
	//line(m_pD3DDevice, (float*)&vMinB, (float*)&vMaxB);


	if( ((vMinB.x > vMinA.x) && (vMinB.x < vMaxA.x)) ||
		((vMaxB.x > vMinA.x) && (vMaxB.x < vMaxA.x)) )
	{
		if( ((vMinB.y > vMinA.y) && (vMinB.y < vMaxA.y)) ||
			((vMaxB.y > vMinA.y) && (vMaxB.y < vMaxA.y)) )
		{
			if( ((vMinB.z > vMinA.z) && (vMinB.z < vMaxA.z)) ||
				((vMaxB.z > vMinA.z) && (vMaxB.z < vMaxA.z)) )
				{
					// There was a collision return true;
					return true;
				}

		}
	}
	
	// No Collison
	return false;
}