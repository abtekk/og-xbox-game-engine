/***************************************************************************/
/*                                                                         */
/*  File: collision.h                                                      */
/*                                                                         */
/*  Details: Camera class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#ifndef _XF_COLLISION_H
#define _XF_COLLISION_H

#include <xtl.h>

#include "../graphics_helpers/graphics_helpers.h"

#include "../collisionreaction/collisionreaction.h"

#include "collision_functions.h"

#define max_value(a, b)  (((a) > (b)) ? (a) : (b)) 

class CCollision
{
public:
	struct stTriangle{ D3DXVECTOR3 p1, p2, p3; };
	
	int m_NumFaces;

	stTriangle* pTriangles;

	D3DXMATRIX m_matWorld;

	D3DXVECTOR3 m_min;
	D3DXVECTOR3 m_max;

	D3DXVECTOR3 m_center;
	float		m_radius;

	IDirect3DDevice8* m_pD3DDevice;

public:
	void Create(IDirect3DDevice8** pD3DDevice, ID3DXMesh* pMesh, D3DXMATRIX* matWorld = NULL)
	{
		m_pD3DDevice = *pD3DDevice;

		m_min.x = m_min.y = m_min.z = 0.0f;
		m_max.x = m_max.y = m_max.z = 0.0f;

		m_center = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		m_radius = 0.0f;

		if( matWorld == NULL )
			D3DXMatrixTranslation(&m_matWorld, 0.0f, 0.0f, 0.0f);
		else
			m_matWorld = *matWorld;

		// Well first we only want the x,y,z of each trangle that make up our shape.
		// We do this using CloneMeshFVF(..)...why?  Well we can extract only the data
		// we need, e.g. just the x,y,z coords
		ID3DXMesh* pClone;
		pMesh->CloneMeshFVF(D3DXMESH_SYSTEMMEM, D3DFVF_XYZ, m_pD3DDevice, &pClone); 
		{
			int numVerts = pClone->GetNumVertices();
			int numFaces = pClone->GetNumFaces();

			struct stVert { float x, y, z; };
			stVert * pVert; 

			WORD* index;
			pClone->LockVertexBuffer(D3DLOCK_READONLY, (BYTE**)&pVert);
			pClone->LockIndexBuffer(D3DLOCK_READONLY,  (BYTE**)&index);
			{
				m_NumFaces = numFaces;
				pTriangles = new stTriangle[numFaces];

				for(int i=0; i<numFaces; i++)
				{
					D3DXVECTOR3* p1 = (D3DXVECTOR3*)&pVert[index[i*3+0]] ;
					D3DXVECTOR3* p2 = (D3DXVECTOR3*)&pVert[index[i*3+1]] ;
					D3DXVECTOR3* p3 = (D3DXVECTOR3*)&pVert[index[i*3+2]] ;

					pTriangles[i].p1 = *p1;
					pTriangles[i].p2 = *p2;
					pTriangles[i].p3 = *p3;

					D3DXVec3Minimize( &m_min,	&m_min,(D3DXVECTOR3*) p1);
					D3DXVec3Minimize( &m_min,	&m_min,(D3DXVECTOR3*) p2);
					D3DXVec3Minimize( &m_min,	&m_min,(D3DXVECTOR3*) p3);

					D3DXVec3Maximize( &m_max,	&m_max,(D3DXVECTOR3*) p1);
					D3DXVec3Maximize( &m_max,	&m_max,(D3DXVECTOR3*) p2);
					D3DXVec3Maximize( &m_max,	&m_max,(D3DXVECTOR3*) p3);


					/*
					sprintf(buf, "point1(%f, %f, %f) point2(%f,%f,%f) point3(%f, %f, %f)",
										p1->x, p1->y, p1->z,
										p2->x, p2->y, p2->z,
										p3->x, p3->y, p3->z);
					abc(buf);
					*/
				}
			}

			m_center.x = (m_max.x - m_min.x)/2.0f;
			m_center.y = (m_max.y - m_min.y)/2.0f;
			m_center.z = (m_max.z - m_min.z)/2.0f;

			m_radius = max_value( m_center.x, m_center.y );
			m_radius = max_value( m_radius  , m_center.z );
			
			m_center.x += m_min.x;
			m_center.y += m_min.y;
			m_center.z += m_min.z;
			

			pClone->UnlockIndexBuffer();
			pClone->UnlockVertexBuffer();
		}
		pClone->Release();
	};


	void SetWorldMatrix( D3DXMATRIX matWorld )
	{
		m_matWorld = matWorld;
	}

	bool Check(CCollision* pOther, D3DXVECTOR3* vDir=NULL)
	{

		
		//wire_cube(m_pD3DDevice, (float*)&pOther->m_min, (float*)&pOther->m_max, &pOther->m_matWorld);
		//wire_cube(m_pD3DDevice, (float*)&m_min, (float*)&m_max, &m_matWorld);

		//circle(m_pD3DDevice, m_center.x, m_center.y, m_center.z, m_radius, &m_matWorld);
		//circle(m_pD3DDevice, pOther->m_center.x, pOther->m_center.y, pOther->m_center.z, pOther->m_radius, &pOther->m_matWorld);

		/*
		
		bool bBox = BoundingBox( &m_min,			&m_max, 
								 &pOther->m_min,	&pOther->m_max, 
								&m_matWorld, &pOther->m_matWorld);
		if( bBox == false )
			return false;
		*/
		
		bool bCollision = false;
		
		// Do these setups here, before we go into the loop and check all the triangle's 
		//if it collides with our sphere...for optimisation.

		D3DXMATRIX* pMatrix = &pOther->m_matWorld;
		D3DXVECTOR3 vCenter = pOther->m_center;
		D3DXVec3TransformCoord( &vCenter, &vCenter, pMatrix);

		for( int i=0; i<m_NumFaces; i++)
		{
			D3DXVECTOR3  p1 =  pTriangles[i].p1; 
			D3DXVECTOR3  p2 =  pTriangles[i].p2; 
			D3DXVECTOR3  p3 =  pTriangles[i].p3; 

			D3DXVec3TransformCoord( &p1, &p1, &m_matWorld);
			D3DXVec3TransformCoord( &p2, &p2, &m_matWorld);
			D3DXVec3TransformCoord( &p3, &p3, &m_matWorld);
                                                            //  +--- Set to constant for this GAME ONLYE!!!!!!!! IMPROTANT
                                                            //  |
			                                                // \|/
			bCollision = SphereToTriangleCollision( &vCenter, 30.0f/*pOther->m_radius*/,  &p1,&p2,&p3);

			if(bCollision == true)
			{
				D3DXVECTOR3 vNewDir = CollisionReaction(*vDir, (float*)&p1, (float*)&p2, (float*)&p3, m_pD3DDevice);
				*vDir = vNewDir;

				arrow(m_pD3DDevice, D3DXVECTOR3(p1.x, 10.0f, p1.z), 
		                vNewDir);
			
				return true;
			}

		}

		return false;
	}

	void Release()
	{
		delete[] pTriangles;
	}

};




#endif // _XF_COLLISION_H