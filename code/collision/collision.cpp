/***************************************************************************/
/*                                                                         */
/*  File: collision.cpp                                                    */
/*                                                                         */
/*  Details: Camera class                                                  */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/