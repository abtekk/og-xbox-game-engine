/***************************************************************************/
/*                                                                         */
/*  graphics.cpp                                                           */
/*                                                                         */
/*                                                                         */
/*  Details: DirectX graphics Init container class                         */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/


#include "graphics.h"


// Settup and destroy parts, returns true (1) for all went okay, and 
// returns false (0) if there was a problem or error.

bool CGraphics::Create()
{
	
	// First of all, create the main D3D object. If it is created successfully we 
    // should get a pointer to an IDirect3D8 interface.
	m_pD3D = Direct3DCreate8(D3D_SDK_VERSION);


    // Create a structure to hold the settings for our device
	D3DPRESENT_PARAMETERS d3dpp; 
	ZeroMemory(&d3dpp, sizeof(d3dpp));


    // Fill the structure. 
    // Set fullscreen 640x480x32 mode
    d3dpp.BackBufferWidth = 640;
    d3dpp.BackBufferHeight = 480;
    d3dpp.BackBufferFormat = D3DFMT_LIN_X8R8G8B8; //D3DFMT_X8R8G8B8;
    d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;



    // Create one backbuffer
    d3dpp.BackBufferCount = 1;
	
	// Set up how the backbuffer is "presented" to the frontbuffer each time
	d3dpp.SwapEffect =  D3DSWAPEFFECT_DISCARD; // D3DSWAPEFFECT_COPY;
	
	//Create a Direct3D device.
	HRESULT e = m_pD3D->CreateDevice(0, D3DDEVTYPE_HAL, NULL, 
							D3DCREATE_HARDWARE_VERTEXPROCESSING, 
							&d3dpp, &m_pD3DDevice);


    //Turn on z-buffering
    m_pD3DDevice->SetRenderState(D3DRS_ZENABLE,  D3DZB_TRUE);
    m_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); //D3DCULL_CCW);

	return true; // all okay
}


bool CGraphics::Release()
{
	// Rule -1-, always tidy up after you've finished doing anything.
	m_pD3DDevice->Release();
    m_pD3D->Release();

	return true; // all okay
}

// This is our accessor member function - which we use to get a hold
// of our screen object.

bool CGraphics::GetScreen(IDirect3DDevice8 **pDevice)
{
	*pDevice = m_pD3DDevice;

	return true; // all okay
}