/***************************************************************************/
/*                                                                         */
/*  graphics.h                                                             */
/*                                                                         */
/*                                                                         */
/*  Details: DirectX graphics Init container class                         */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/




#ifndef _XF_GRAPHICS_H
#define _XF_GRAPHICS_H


#include <xtl.h>


class CGraphics
{
	IDirect3D8			*m_pD3D;					// DirectX Object
	IDirect3DDevice8    *m_pD3DDevice;				// Screen Object


public:
	// Settup and destroy parts.
	bool Create();
	bool Release();

public:
	// We may need to draw to the screen, so we'll need a way of getting to
	// the Screen Object (m_pD3DDevice), and this is what this function does.
	// Returns true (1) if all went okay.
	bool GetScreen(IDirect3DDevice8 **pDevice);

};











#endif // _XF_GRAPHICS_H