/***************************************************************************/
/*                                                                         */
/*  File: light.h                                                          */
/*                                                                         */
/*  Details: Light Class                                                   */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#ifndef _XF_LIGHT_H
#define _XF_LIGHT_H


#include <xtl.h>



class CLight
{
	D3DLIGHT8 d3dLight;

	IDirect3DDevice8* m_pD3DDevice;
public:

	bool Create(IDirect3DDevice8 **pDevice, int iLightNumber)
	{
	
	  m_pD3DDevice = *pDevice;

      //Initialize the light structure.
      ZeroMemory(&d3dLight, sizeof(D3DLIGHT8));

      //Set up a white point light at (0, 0, -10).
      d3dLight.Type = D3DLIGHT_POINT;
      // Actual colour of the light. (e.g. white).
      d3dLight.Diffuse.r = 1.0f;
      d3dLight.Diffuse.g = 1.0f;
      d3dLight.Diffuse.b = 1.0f;

      // Colour when no light hits the object.
      d3dLight.Ambient.r = 0.0f;
      d3dLight.Ambient.g = 0.0f;
      d3dLight.Ambient.b = 0.0f;

      // Shiny effect.
      d3dLight.Specular.r     = 0.0f;
      d3dLight.Specular.g     = 0.0f;
      d3dLight.Specular.b     = 0.0f;

      d3dLight.Position.x     = 0.0f;
      d3dLight.Position.y     = 0.0f;
      d3dLight.Position.z     = -5.0f;

 
      d3dLight.Attenuation0 = 1.0f; 
      d3dLight.Attenuation1 = 0.0f; 
      d3dLight.Attenuation2 = 0.0f; 
      d3dLight.Range = 1000.0f;

      m_pD3DDevice->SetLight(0, &d3dLight);
      m_pD3DDevice->LightEnable(0,TRUE);
      //Turn on lighting
      m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
      m_pD3DDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(20,20,20));
	
	  return true;
	}

	void Move(float x, float y, float z)
	{
		
		d3dLight.Position.x     = x;
		d3dLight.Position.y     = y;
		d3dLight.Position.z     = z;

		m_pD3DDevice->SetLight(0, &d3dLight);
		m_pD3DDevice->LightEnable(0,TRUE);
		
	}

	void Release()
	{
	}
};






#endif // _XF_LIGHT_H