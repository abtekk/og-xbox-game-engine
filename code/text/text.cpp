/***************************************************************************/
/*                                                                         */
/*  text.h                                                                 */
/*                                                                         */
/*                                                                         */
/*  Details: CText class                                                   */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/
/*
IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT

  Default fontbitmap file required, and is located at 

  "D:\\media\\font\\arial18normal.bmf"

  {case sensitive}

  If you do not include this, the function will fail!

  You can also pass the an alternative file location in the Create function call

  HOW TO USE:

  -1-
  #include "code/text/text.h"

  -2-
  CText tt;

  -3- Create our font and pass in the file location of our font file, if
      different from the default one.
  tt.Create( pD3DDevice );

  -4- Render to the screen.

  tt.TextOut( "Hello World");

  -5- Always be a good boy and tidy up when you've finished.

  tt.Release();


*/
/***************************************************************************/


#include "text.h"


   


void CText::Create( IDirect3DDevice8 **pD, WCHAR* szFontFile )
{
	m_pd3dDevice = *pD;

	//InitialiseFonts
	m_pd3dDevice->GetBackBuffer(-1,D3DBACKBUFFER_TYPE_MONO,&g_pScreenBuffer);

	DWORD dwFontCacheSize = 16 * 1024;

	if( szFontFile == NULL )
	{ 	
		//Load our font in - have to sepcify its loacation
		XFONT_OpenBitmapFont( L"D:\\media\\font\\arial18normal.bmf",
						dwFontCacheSize, &m_pBitmapFont );
	}
	else
	{
		XFONT_OpenBitmapFont( szFontFile,
						dwFontCacheSize, &m_pBitmapFont );
	}

}

void CText::TextOut(WCHAR string[200], float xpos, float ypos, int r, int g, int b)
{

	WCHAR szbuff[200] = {0};

    wsprintfW(szbuff, L"%s", string);

	//Set out text colour
	m_pBitmapFont->SetTextColor(D3DCOLOR_XRGB(r,g,b));
	//Display our text.
    m_pBitmapFont->TextOut( g_pScreenBuffer, szbuff, -1, (long)xpos, (long)ypos );

}

void CText::Release()
{
	//Release our TextBuffers
    m_pBitmapFont->Release();

    g_pScreenBuffer->Release();
}