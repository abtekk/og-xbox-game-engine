/***************************************************************************/
/*                                                                         */
/*  text.h                                                                 */
/*                                                                         */
/*                                                                         */
/*  Details: CText class                                                   */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/
/*
IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT

  Default fontbitmap file required, and is located at 

  "D:\\media\\font\\arial18normal.bmf"

  If you do not include this, the function will fail!

  You can also pass the an alternative file location in the Create function call

  HOW TO USE:

  -1-
  #include "code/text/text.h"

  -2-
  CText tt;

  -3- Create our font and pass in the file location of our font file, if
      different from the default one.
  tt.Create( pD3DDevice );

  -4- Render to the screen.

  tt.TextOut( "Hello World");

  -5- Always be a good boy and tidy up when you've finished.

  tt.Release();


*/
/***************************************************************************/


#ifndef _XF_TEXT_H
#define _XF_TEXT_H




#include <xtl.h>
#include <xfont.h>


class CText
{
	LPDIRECT3DDEVICE8 m_pd3dDevice;

	XFONT*      m_pBitmapFont;    // Pointer to the Arial18Normal Bitmap font 

	LPDIRECT3DSURFACE8 g_pScreenBuffer;

public:

	void Create( IDirect3DDevice8 **pD, WCHAR* szFontFile = NULL );

	void TextOut(WCHAR string[200], float xpos=20.0f, float ypos=30.0f, int r=0xFF, int g=0xFF, int b=0);

	void Release();
};






#endif // _XF_TEXT_H