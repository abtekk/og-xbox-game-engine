/***************************************************************************/
/*                                                                         */
/*  File:    cube.cpp                                                      */
/*                                                                         */
/*  Details: Cube class - allows us to create cubes, and render then all   */
/*           over our 3D world                                             */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#include "cube.h"


bool CCube::Create(IDirect3DDevice8 *pDevice)
{
	m_pD3DDevice = pDevice;

	// Lets set the cubes initial position to 0,0,0 the center of our world!
	m_x = 0.0f;
	m_y = 0.0f;
	m_z = 0.0f;


	// Set up our 3D Data storage are on the graphics card
	m_vertex_format = D3DFVF_XYZ|D3DFVF_DIFFUSE;

	// Here I'm going to define my cube, its x,y,z and colour components for
	// all the sides.
    stCustomVertex cvVertices[] =
    {
		// x        y      z            colour
		
		// Front of the cube
		{  0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255,  0  ),}, // top left
		{ -0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255,  0  ),}, // triangle
		{ -0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255,  0  ),}, // (green)

		{  0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255,  0  ),}, // bottom right
		{  0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255,  0  ),}, // triangle
		{ -0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255,  0  ),}, //

		// Back of our cube
		{ -0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB(255, 255,  0  ),}, //
		{ -0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(255, 255,  0  ),}, //
		{  0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB(255, 255,  0  ),}, // (yellow)

		{ -0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(255, 255,  0  ),}, //
		{  0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(255, 255,  0  ),}, //
		{  0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB(255, 255,  0  ),}, //

		// Top of our cube
		{  0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB(255, 255, 255 ),}, //
		{ -0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB(255, 255, 255 ),}, //
		{ -0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB(255, 255, 255 ),}, // (white)

		{  0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB(255, 255, 255 ),}, //
		{  0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB(255, 255, 255 ),}, //
		{ -0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB(255, 255, 255 ),}, //

		// Bottom of our cube
		{ -0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(255,  0,  255 ),}, //
		{ -0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB(255,  0,  255 ),}, //
		{  0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(255,  0,  255 ),}, // (blue)

		{ -0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB(255,  0,  255 ),}, //
		{  0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB(255,  0,  255 ),}, //
		{  0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(255,  0,  255 ),}, //

		// Left of our cube
		{ -0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB( 0,  255, 255 ),}, //
		{ -0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255, 255 ),}, //
		{ -0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB( 0,  255, 255 ),}, //

		{ -0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB( 0,  255, 255 ),}, //
		{ -0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255, 255 ),}, //
		{ -0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 0,  255, 255 ),}, //

		// Right of our cube
		{  0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB( 255, 0,  255 ),}, //
		{  0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 255, 0,  255 ),}, //
		{  0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB( 255, 0,  255 ),}, //

		{  0.5f, -0.5f,-0.5f, D3DCOLOR_XRGB( 255, 0,  255 ),}, //
		{  0.5f,  0.5f,-0.5f, D3DCOLOR_XRGB( 255, 0,  255 ),}, //
		{  0.5f,  0.5f, 0.5f, D3DCOLOR_XRGB( 255, 0,  255 ),}, //
    };

	//Create the vertex buffer from our device
    m_pD3DDevice->CreateVertexBuffer( sizeof(cvVertices),
										0, 
										m_vertex_format, // D3DFVF_XYZRHW|D3DFVF_DIFFUSE
										D3DPOOL_DEFAULT, &m_pVertexBuffer);

	// Temporary pointer.
	VOID* pVertices;
	//Get a pointer to the vertex buffer vertices and lock the vertex buffer
    m_pVertexBuffer->Lock(0, sizeof(cvVertices), (BYTE**)&pVertices, 0); 

    //Copy our stored vertices values into the vertex buffer
    memcpy(pVertices, cvVertices, sizeof(cvVertices));

    //Unlock the vertex buffer
    m_pVertexBuffer->Unlock();

	return 1; // all okay
}


bool CCube::Release()
{
	m_pVertexBuffer->Release();

	return 1; // all okay
}


// Moving and Rendering our Cube is a matter of 2 other functions....

void CCube::Move( float x, float y, float z)
{
	m_x = x;
	m_y = y;
	m_z = z;
}


void CCube::Render()
{
	// -A1- Start - These few lines set our rendering to turn lighting off,
	// or we'll not see our cube if there's no light!... Also made sure ZBuffer
	// is on, so we can draw shapes into the 3D world!
	// Finally - turn Culling on, so we dont' draw back of triangles.
	// Turn lighting off, as where only using rgb values
	m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, false);
    //Turn on z-buffering
    m_pD3DDevice->SetRenderState(D3DRS_ZENABLE,  D3DZB_TRUE);
	// Our shape is draw with each triangle clock wise, so we cull, or
	// should I say not draw counterclockwise triangles.
    m_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	// -A1- End
	
	// -B1- Start - these lines move our 3D cube to its position in the 3D
	// world, as we set the D3DTS_WORLD with out tranMatrix.
	D3DXMATRIX matWorld;
	D3DXMATRIX trans_matrix;   //Our translation matrix

	// set our matWorld matrix to an identiy matrix...e.g. set it to 1
	D3DXMatrixIdentity( &matWorld );

	//Set up the translation matrix and move our cube where its suppose to go.
	D3DXMatrixTranslation(&trans_matrix, m_x, m_y ,m_z);
	//Combine out matrices
	D3DXMatrixMultiply(&matWorld,&matWorld, &trans_matrix);
	//Set our World Matrix
	m_pD3DDevice->SetTransform(D3DTS_WORLD, &matWorld );
	// -B1- End


	// Rendering our cube
    m_pD3DDevice->SetStreamSource(0, m_pVertexBuffer, sizeof(stCustomVertex));
    m_pD3DDevice->SetVertexShader(m_vertex_format); // D3DFVF_XYZ|D3DFVF_DIFFUSE

	int iTriangles = 12;
    m_pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, iTriangles);
}