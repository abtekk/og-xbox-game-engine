/***************************************************************************/
/*                                                                         */
/*  File:    cube.h                                                        */
/*                                                                         */
/*  Details: Cube class - allows us to create cubes, and render then all   */
/*           over our 3D world                                             */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

// How would you use this amazing CCube class?  Well it can't be any simplier
// just create an instance of it e.g.:
// CCube c;
// Then call create an pass the IDirect3DDevice8* pointer.
// c.Create( pDevice );
// Now in your Render scene, during the ->begin and ->end drawing of you
// 3D world, call the Render member funciton

// c.Render();

// And in the end, when you've finished, just tidy up by calling Release()
// c.Release();

// Quick review:

// -1- (once at start)
// CCube c;
// c.Create( pDevice );

// -2- (everytime you render)
// c.Render();

// -3- At the end
// c.Release();

/***************************************************************************/

#ifndef _XF_CUBE_H
#define _XF_CUBE_H

// Well if you want to use the IDirect3DDevice, and other DirectX3D functions
// and definitions you have to include xtl.h for the xbox

#include <xtl.h>

class CCube
{
protected:
	IDirect3DDevice8  *m_pD3DDevice;

	// The cubes position in our 3D world!
	float m_x;
	float m_y;
	float m_z;

protected:
	
	IDirect3DVertexBuffer8  *m_pVertexBuffer;

	struct stCustomVertex
	{
		FLOAT x, y, z; // The transformed position for the vertex.
		DWORD colour;       // The vertex colour.
	};

	DWORD m_vertex_format;


	// These are our setup member funtions, call once at the beginning, once at
	// the end.
public:
	bool Create( IDirect3DDevice8* pDevice );
	bool Release();


	// Now for our two user functions, just incase we want to move our cube around
	// and actually render it to the screen.
	
	void Move(float x, float y, float z);
	void Render();

};




#endif // _XF_CUBE_H