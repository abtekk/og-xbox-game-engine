/***************************************************************************/
/*                                                                         */
/*  xloader.h                                                              */
/*                                                                         */
/*                                                                         */
/*  Details: CXLoader Class                                                */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/
/*
Well after much thinking, instead of just having one class do all the
rendering of our world and our characters..it just seemed to put to much 
burden on one class.  So the XLoader will simply load our 3D world, or any
other non-animated .x files, and render them to our world.

It will hold simple collsion detection and movement.  It will not do any
animation...just simple rendering.


*/
/***************************************************************************/


#ifndef _XF_XLOADER_H
#define _XF_XLOADER_H

#include <xtl.h>
#include <string.h>

#include "../../debug.h"
#include "../collision/collision.h"

class CXLoader
{
protected:

	LPD3DXMESH              m_pMesh           ; // Our mesh object in sysmem
	D3DMATERIAL8*           m_pMeshMaterials  ; // Materials for our mesh
	LPDIRECT3DTEXTURE8*     m_pMeshTextures   ; // Textures for our mesh
	DWORD                   m_dwNumMaterials  ; // Number of mesh materials

	IDirect3DDevice8*		m_pD3DDevice	  ; // Screen Object

	D3DXMATRIX				m_matRot;
	D3DXMATRIX				m_matPos;
	D3DXMATRIX				m_matWorld;
public:

	CCollision				m_collision		  ; // Collision class

public:
	CXLoader()
	{
		m_pMesh          = NULL; // Our mesh object in sysmem
		m_pMeshMaterials = NULL; // Materials for our mesh
		m_pMeshTextures  = NULL; // Textures for our mesh
		m_dwNumMaterials = 0L;   // Number of mesh materials

		m_pD3DDevice	 = NULL;

		D3DXMatrixIdentity(&m_matRot);
		D3DXMatrixTranslation(&m_matPos, 0.0f, 0.0f, 0.0f);

	}


public:
	void Create(IDirect3DDevice8** pD3DDevice, char *szFilename, char *TexturePath = ".\\")
	{
		m_pD3DDevice = *pD3DDevice;

		LPD3DXBUFFER pD3DXMtrlBuffer;

		// Load the mesh from the specified file
		D3DXLoadMeshFromX( szFilename, D3DXMESH_SYSTEMMEM, 
									   m_pD3DDevice, NULL, 
									   &pD3DXMtrlBuffer, &m_dwNumMaterials, 
									   &m_pMesh );

		// We need to extract the material properties and texture names from the 
		// pD3DXMtrlBuffer
		D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)pD3DXMtrlBuffer->GetBufferPointer();
		m_pMeshMaterials = new D3DMATERIAL8[m_dwNumMaterials];
		m_pMeshTextures  = new LPDIRECT3DTEXTURE8[m_dwNumMaterials];

		
		for( DWORD i=0; i<m_dwNumMaterials; i++ )
		{
			// Copy the material
			m_pMeshMaterials[i] = d3dxMaterials[i].MatD3D;

			// Set the ambient color for the material (D3DX does not do this)
			m_pMeshMaterials[i].Ambient = m_pMeshMaterials[i].Diffuse;
     
			m_pMeshTextures[i] = NULL;

			if(d3dxMaterials[i].pTextureFilename != NULL)
			{
				char szTextureFile[256];
				strcpy(szTextureFile, TexturePath);
				strcat(szTextureFile, d3dxMaterials[i].pTextureFilename);

				// Create the texture
				if( FAILED( D3DXCreateTextureFromFile( m_pD3DDevice, 
													   szTextureFile, 
													   &m_pMeshTextures[i] ) ) )
				{
					m_pMeshTextures[i] = NULL;
				}
			}
		}
		

		// Done with the material buffer
		pD3DXMtrlBuffer->Release();

		m_collision.Create(&m_pD3DDevice, m_pMesh);
	}



	void Render()
	{

		m_matWorld = m_matRot;
		D3DXMatrixMultiply(&m_matWorld, &m_matWorld, &m_matPos);
		m_pD3DDevice->SetTransform( D3DTS_WORLD, &m_matWorld);


		// Turn on ambient lighting 
		//m_pD3DDevice->SetRenderState( D3DRS_AMBIENT, 0xffffffff );
		m_pD3DDevice->SetRenderState( D3DRS_AMBIENT, 0x00f0f0f0 );
		//m_pD3DDevice->SetRenderState( D3DRS_AMBIENT, 0x00606060 );

		// Meshes are divided into subsets, one for each material. Render them in
		// a loop
		
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

		m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		
		for( DWORD i=0; i<m_dwNumMaterials; i++ )
		{
			// Set the material and texture for this subset
			m_pD3DDevice->SetMaterial( &m_pMeshMaterials[i] );

			if( m_pMeshTextures[i] != NULL )
			{
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
				m_pD3DDevice->SetTexture( 0, m_pMeshTextures[i] );
			}
			else
			{
				m_pD3DDevice->SetTexture( 0, NULL );
				m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE );//D3DTA_SPECULAR); //D3DTA_DIFFUSE
			}
        
				// Draw the mesh subset
				m_pMesh->DrawSubset( i );
		}

		m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);


		m_pD3DDevice->SetTexture( 0, NULL );
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	}


	void Release()
	{
		if( m_pMeshMaterials != NULL ) 
			delete[] m_pMeshMaterials;

		if( m_pMeshTextures )
		{
			for( DWORD i = 0; i < m_dwNumMaterials; i++ )
			{
				if( m_pMeshTextures[i] )
					m_pMeshTextures[i]->Release();
			}
			delete[] m_pMeshTextures;
		}
		if( m_pMesh != NULL )
			m_pMesh->Release();

		m_collision.Release();
	}


	void Move(float x, float y, float z)
	{
		D3DXMatrixTranslation(&m_matPos, x, y, z);
	}

	void Rotate(float x_rot, float y_rot, float z_rot)
	{
		D3DXMATRIX mX, mY, mZ;
		D3DXMatrixRotationX(&mX, x_rot);
		D3DXMatrixRotationY(&mY, y_rot);
		D3DXMatrixRotationZ(&mZ, z_rot);

		m_matRot = mX;
		D3DXMatrixMultiply(&m_matRot, &m_matRot, &mY);
		D3DXMatrixMultiply(&m_matRot, &m_matRot, &mZ);
	}

};



#endif // _XF_XLOADER_H

