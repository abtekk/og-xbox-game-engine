/***************************************************************************/
/*                                                                         */
/*  File: object.cpp                                                       */
/*  URL: www.xfactordev.net                                                */
/*  Author: Ben_3D@xfactordev.net                                          */
/*  version: 1.0.0                                                         */
/*                                                                         */
/***************************************************************************/
/*                                                                         */
/* This is responsible for the loading and rendering of the .x file        */
/* its the first version, and is quiet simple to follow its workings       */
/*                                                                         */
/***************************************************************************/
/*                                                                         */
/* How would we use such a fantastic piece of code?  Simple...using these  */
/* few lines of code                                                       */
/*  ======================= Call at Creation ===========================   */
/*  CMesh m;                                                               */
/*	m.Load( g_pD3DDevice, "warrior.x" );                                   */
/*                                                                         */
/*	CObject o;                                                             */
/*	o.Create(g_pD3DDevice, &m );                                           */
/*                                                                         */
/*	CAnimation a;                                                          */
/*	a.Load("warrior.x", &m);                                               */
/*	a.SetLoop(TRUE, "Walk");                                               */
/*                                                                         */
/*	// Swing, Hurt, Walk, Die, Spell, Idle                                 */
/*	o.SetAnimation(&a, "Walk", 0);                                         */
/*                                                                         */
/*  ======================== Render Function ===========================   */
/*                                                                         */
/*	static float fl=0;                                                     */
/*	fl = fl + 0.03f;                                                       */
/*	                                                                       */
/*	o.UpdateAnimation(fl, TRUE);                                           */
/*	o.Render();                                                            */
/*                                                                         */
/*  ======================= CleanUp Function ===========================   */
/*                                                                         */
/*  m.Release();                                                           */
/*  o.Release();                                                           */
/*  a.Release();                                                           */
/*                                                                         */
/***************************************************************************/

// Note if you dont have an animation .x file, you can always just do this:
// CMesh m;                                                               
// m.Load( g_pD3DDevice, "box.x" );                                     
// CObject o;                               
// o.Create(g_pD3DDevice, &m );  
//  
// Inside the Render funtion
// o.Render();
//
// Tidy-Up
// m.Release();
// o.Release();

#include <rmxftmpl.h> // D3DRM_XTEMPLATES definition
//#include "Core_Global.h"
//#include "rmxftmpl.h"
//#include "rmxfguid.h"

#include "object.h"






/***************************************************************************/
/*                                                                         */
/*  CMesh Code                                                             */
/*                                                                         */
/***************************************************************************/

CMesh::CMesh()
{
  m_pD3DDevice = NULL;
  m_NumMeshes = 0;		m_Meshes = NULL;
  m_NumFrames = 0;		m_Frames = NULL;
}


BOOL CMesh::Load(IDirect3DDevice8** pD3DDevice, char *Filename, char *TexturePath)
{
  IDirectXFile           *pDXFile = NULL;
  IDirectXFileEnumObject *pDXEnum = NULL;
  IDirectXFileData       *pDXData = NULL;


  m_pD3DDevice = *pD3DDevice;

  DirectXFileCreate(&pDXFile);

  // Register the templates
  pDXFile->RegisterTemplates((LPVOID)D3DRM_XTEMPLATES, D3DRM_XTEMPLATE_BYTES);


  // Create an enumeration object
  pDXFile->CreateEnumObject((LPVOID)Filename, DXFILELOAD_FROMFILE, &pDXEnum);

  // Create a temporary frame
  m_Frames = new stFrame();
  m_Frames->m_Name = new char[7];
  strcpy(m_Frames->m_Name, "%ROOT%");

  // Loop through all objects looking for the frames and meshes
  while(SUCCEEDED(pDXEnum->GetNextDataObject(&pDXData))) 
  {
    ParseXFileData(pDXData, m_Frames, TexturePath);
    ReleaseCOM(pDXData);
  }

  // Release used COM objects
  ReleaseCOM(pDXEnum);
  ReleaseCOM(pDXFile);


  // Match frames to bones (for matrices)
  MapFramesToBones(m_Frames);

  return TRUE;
}

void CMesh::ParseXFileData(IDirectXFileData *pDataObj, stFrame *ParentFrame, char *TexturePath)
{
  IDirectXFileObject *pSubObj  = NULL;
  IDirectXFileData   *pSubData = NULL;
  IDirectXFileDataReference *pDataRef = NULL;
  const GUID *Type = NULL;
  char       *Name = NULL;
  DWORD       Size;
  stFrame     *SubFrame = NULL;
  char        Path[MAX_PATH];

  stFrame     *Frame = NULL;
  D3DXMATRIX *FrameMatrix = NULL;

  stMesh         *Mesh = NULL;
  ID3DXBuffer   *MaterialBuffer = NULL;
  D3DXMATERIAL  *Materials = NULL;
  ID3DXBuffer   *Adjacency = NULL;
  DWORD         *AdjacencyIn = NULL;
  DWORD         *AdjacencyOut = NULL;

  DWORD i;
  

  // Get the template type
  if(FAILED(pDataObj->GetType(&Type)))
    return;

  // Get the template name (if any)
  if(FAILED(pDataObj->GetName(NULL, &Size)))
    return;
  if(Size) {
    if((Name = new char[Size]) != NULL)
      pDataObj->GetName(Name, &Size);
  }

  // Give template a default name if none found
  if(Name == NULL) {
    if((Name = new char[9]) == NULL)
      return;
    strcpy(Name, "$NoName$");
  }

  // Set sub frame
  SubFrame = ParentFrame;

  // Process the templates

//=========================================================================//
//                          Frame Section                                  //
//=========================================================================//
  // Frame
  if(*Type == TID_D3DRMFrame) 
  {

    // Create a new frame structure
    Frame = new stFrame();

    // Store the name
    Frame->m_Name = Name;
    Name = NULL;

    // Add to parent frame
    Frame->m_Parent = ParentFrame;
    Frame->m_Sibling = ParentFrame->m_Child;
    ParentFrame->m_Child = Frame;

    // Increase frame count
    m_NumFrames++;

    // Set sub frame parent
    SubFrame = Frame;
  }

//=========================================================================//
//           Frame Transformation Matrix Section                           //
//=========================================================================//
  // Frame transformation matrix
  if(*Type == TID_D3DRMFrameTransformMatrix) 
  {
    pDataObj->GetData(NULL, &Size, (PVOID*)&FrameMatrix);
    ParentFrame->m_matOriginal = *FrameMatrix;
  }

//=========================================================================//
//                         Mesh Section                                    //
//=========================================================================//
  // Mesh
  if(*Type == TID_D3DRMMesh) 
  {

    // See if mesh already loaded...could be the first mesh!
    if(m_Meshes == NULL || m_Meshes->FindMesh(Name) == NULL) {
      // Create a new mesh structure
      Mesh = new stMesh();

      // Store the name
      Mesh->m_Name = Name;
      Name = NULL;


      // Load mesh data
      D3DXLoadSkinMeshFromXof(pDataObj, 0,
                  m_pD3DDevice, 
                  &Adjacency,
                  &MaterialBuffer, &Mesh->m_NumMaterials, 
                  &Mesh->m_BoneNames, &Mesh->m_BoneTransforms,
                  &Mesh->m_SkinMesh);


      // Convert to regular mesh if no bones
      if(!(Mesh->m_NumBones = Mesh->m_SkinMesh->GetNumBones())) 
	  {
		// Convert to a regular mesh
		Mesh->m_SkinMesh->GetOriginalMesh(&Mesh->m_Mesh);
		ReleaseCOM(Mesh->m_SkinMesh);
      }
	  else 
	  {
			// Create the bone matrix array and clear it out
			Mesh->m_Matrices = new D3DXMATRIX[Mesh->m_NumBones];
			for(i=0;i<Mesh->m_NumBones;i++)
				D3DXMatrixIdentity(&Mesh->m_Matrices[i]);

			// Create the frame mapping matrix array and clear out
			Mesh->m_FrameMatrices = new D3DXMATRIX*[Mesh->m_NumBones];
			// Set it to NULL just incase ;)
			for(i=0;i<Mesh->m_NumBones;i++)
				Mesh->m_FrameMatrices[i] = NULL;

        // Get a pointer to bone matrices
        Mesh->m_BoneMatrices = (D3DXMATRIX*)Mesh->m_BoneTransforms->GetBufferPointer();

        AdjacencyIn  = (DWORD*)Adjacency->GetBufferPointer();
        AdjacencyOut = new DWORD[Mesh->m_SkinMesh->GetNumFaces() * 3];

        // Generate the skin mesh object
		Mesh->m_SkinMesh->GenerateSkinnedMesh(
                    D3DXMESH_WRITEONLY, 0.0f, 
                    AdjacencyIn, AdjacencyOut,
					// For version 8.1 and later add these two lines
					//NULL, NULL, /*exclude these 2 from xbox version 8.0*/
                    &Mesh->m_Mesh);

        delete [] AdjacencyOut;
        ReleaseCOM(Adjacency);
      }

      // Load materials or create a default one if none
      if(!Mesh->m_NumMaterials) {
        // Create a default one
        Mesh->m_Materials = new D3DMATERIAL8[1];
        Mesh->m_Textures  = new LPDIRECT3DTEXTURE8[1];

        ZeroMemory(Mesh->m_Materials, sizeof(D3DMATERIAL8));
        Mesh->m_Materials[0].Diffuse.r = 1.0f;
        Mesh->m_Materials[0].Diffuse.g = 1.0f;
        Mesh->m_Materials[0].Diffuse.b = 1.0f;
        Mesh->m_Materials[0].Diffuse.a = 1.0f;
        Mesh->m_Materials[0].Ambient   = Mesh->m_Materials[0].Diffuse;
        Mesh->m_Materials[0].Specular  = Mesh->m_Materials[0].Diffuse;
        Mesh->m_Textures[0] = NULL;

        Mesh->m_NumMaterials = 1;
      } 
	  else 
	  {
        // Load the materials
        Materials = (D3DXMATERIAL*)MaterialBuffer->GetBufferPointer();
        Mesh->m_Materials = new D3DMATERIAL8[Mesh->m_NumMaterials];
        Mesh->m_Textures  = new LPDIRECT3DTEXTURE8[Mesh->m_NumMaterials];

        for(i=0;i<Mesh->m_NumMaterials;i++) {
          Mesh->m_Materials[i] = Materials[i].MatD3D;
          Mesh->m_Materials[i].Ambient = Mesh->m_Materials[i].Diffuse;
       
          // Build a texture path and load it
          sprintf(Path, "%s%s", TexturePath, Materials[i].pTextureFilename);

          if(FAILED(D3DXCreateTextureFromFile(m_pD3DDevice, Path, &Mesh->m_Textures[i])))
		  {  Mesh->m_Textures[i] = NULL;  }
        }
      }
      ReleaseCOM(MaterialBuffer);
	
      // link in mesh
      Mesh->m_Next = m_Meshes;
      m_Meshes = Mesh;
      m_NumMeshes++;
    } else {
      // Find mesh in list
      Mesh = m_Meshes->FindMesh(Name);
    }

    // Add mesh to frame 
    if(Mesh != NULL)
      ParentFrame->AddMesh(Mesh);

  }

//=========================================================================//
//                      Animation Section                                  //
//=========================================================================//
  // Skip animation sets and animations
  if(*Type == TID_D3DRMAnimationSet || *Type == TID_D3DRMAnimation || *Type == TID_D3DRMAnimationKey) {
    delete [] Name;
    return;
  }

  // Release name buffer
  delete [] Name;

//=========================================================================//
//                      Nested Sections                                    //
//=========================================================================//
  // Scan for embedded templates
  while(SUCCEEDED(pDataObj->GetNextObject(&pSubObj))) {

    // Process embedded references
    if(SUCCEEDED(pSubObj->QueryInterface(IID_IDirectXFileDataReference, (void**)&pDataRef))) {
      if(SUCCEEDED(pDataRef->Resolve(&pSubData))) {
        ParseXFileData(pSubData, SubFrame, TexturePath);
        ReleaseCOM(pSubData);
      }
      ReleaseCOM(pDataRef);
    }

    // Process non-referenced embedded templates
    if(SUCCEEDED(pSubObj->QueryInterface(IID_IDirectXFileData, (void**)&pSubData))) {
      ParseXFileData(pSubData, SubFrame, TexturePath);
      ReleaseCOM(pSubData);
    }
    ReleaseCOM(pSubObj);
  }

  return;
}

void CMesh::MapFramesToBones(stFrame *Frame)
{
  stMesh *Mesh;
  DWORD i;
  char **Name;

  // Return if no more frames to map
  if(Frame == NULL || Frame->m_Name == NULL)
    return;

  // Scan through meshes looking for bone matches
  Mesh = m_Meshes;
  while(Mesh != NULL) {
    if(Mesh->m_NumBones && Mesh->m_BoneNames != NULL && Mesh->m_Matrices != NULL && Mesh->m_FrameMatrices != NULL) {
      Name = (char**)Mesh->m_BoneNames->GetBufferPointer();
      for(i=0;i<Mesh->m_NumBones;i++) {
        if(!strcmp(Frame->m_Name, Name[i])) {
          Mesh->m_FrameMatrices[i] = &Frame->m_matCombined;
          break;
        }
      }
    }
    Mesh = Mesh->m_Next;
  }

  // Scan through child frames
  MapFramesToBones(Frame->m_Child);

  // Scan through sibling frames
  MapFramesToBones(Frame->m_Sibling);
}

BOOL CMesh::Release()
{
  m_pD3DDevice = NULL;
 
  m_NumMeshes = 0;
  delete m_Meshes;
  m_Meshes = NULL;

  m_NumFrames = 0;
  delete m_Frames;
  m_Frames = NULL;

  return TRUE;
}



/***************************************************************************/
/*                                                                         */
/*  CObject Code                                                           */
/*                                                                         */
/***************************************************************************/

CObject::CObject()
{
  m_pD3DDevice = NULL;
  m_Mesh           = NULL;
  m_AnimationSet   = NULL;

  D3DXMatrixTranslation(&matWorld,0,0,0); 
  D3DXMatrixIdentity( &matRotation );

}


BOOL CObject::Create(IDirect3DDevice8** pD3DDevice, CMesh *Mesh)
{
  if( pD3DDevice == NULL)
    return FALSE;
  m_pD3DDevice = *pD3DDevice;
  m_Mesh = Mesh;


  m_collision.Create(&m_pD3DDevice, m_Mesh->m_Meshes->m_Mesh, &matPos);

  return TRUE;
}

BOOL CObject::Release()
{
  m_pD3DDevice = NULL;
  m_Mesh         = NULL;
  m_AnimationSet = NULL;

  m_collision.Release();

  return TRUE;
}




BOOL CObject::SetAnimation(CAnimation *Animation, char *Name, unsigned long StartTime)
{
  m_StartTime = StartTime;
  if(Animation == NULL)
    m_AnimationSet = NULL;
  else
    m_AnimationSet = Animation->GetAnimationSet(Name);

  return TRUE;
}



BOOL CObject::UpdateAnimation(unsigned long Time, BOOL Smooth)
{
  if(m_AnimationSet != NULL) 
  {
    m_Mesh->m_Frames->ResetMatrices();
	 // m_Mesh->m_Frames;
    m_AnimationSet->Update(Time - m_StartTime, Smooth);

	static float fUpdate = 0.0f;
	fUpdate += 0.01f;
	if( fUpdate > 1.0f )
	{
		fUpdate = 0.0f;

		//m_collision.Release();
		//m_collision.Create(&m_pD3DDevice, m_Mesh->m_Meshes->m_Mesh, &matWorld);
	}

  }

  return TRUE;
}

BOOL CObject::AnimationComplete(unsigned long Time)
{
  if(m_AnimationSet == NULL)
    return TRUE;
  if((Time - m_StartTime) >= m_AnimationSet->m_Length)
    return TRUE;
  return FALSE;
}

BOOL CObject::Render()
{
  D3DXMATRIX Matrix;
  
  // Error checking
  if(m_pD3DDevice == NULL || m_Mesh == NULL || m_Mesh->m_Frames == NULL || m_Mesh->m_Meshes == NULL)
    return FALSE;


  D3DXMatrixIdentity( &matWorld );
  D3DXMatrixMultiply( &matWorld, &matRotation, &matPos );

  m_pD3DDevice->SetTransform( D3DTS_WORLD, &matWorld);
  m_collision.SetWorldMatrix( matWorld );

  // Update the frame matrices
  D3DXMatrixIdentity(&Matrix);
  UpdateFrame(m_Mesh->m_Frames, &Matrix);

  // Copy frame matrices to bone matrices
  m_Mesh->m_Meshes->CopyFrameToBoneMatrices();

  m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
  m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
  m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE );

  // Draw all frame meshes
  DrawFrame(m_Mesh->m_Frames);

  return TRUE;
}

void CObject::UpdateFrame(stFrame *Frame, D3DXMATRIX *Matrix)
{
  // Return if no more frames
  if(Frame == NULL)
    return;

  // Calculate frame matrix based on animation or not
  if(m_AnimationSet == NULL)
    D3DXMatrixMultiply(&Frame->m_matCombined, &Frame->m_matOriginal, Matrix);
  else
    D3DXMatrixMultiply(&Frame->m_matCombined, &Frame->m_matTransformed, Matrix);

  // Update child frames
  UpdateFrame(Frame->m_Child, &Frame->m_matCombined);

  // Update sibling frames
  UpdateFrame(Frame->m_Sibling, Matrix);
}

void CObject::DrawFrame(stFrame *Frame)
{
  stFrameMeshList *List;
  stMesh          *Mesh;
  D3DXMATRIX      matWorld;
  DWORD           i;

  if(Frame == NULL)
    return;

  if((List = Frame->m_MeshList) != NULL) {
    while(List != NULL) {

      // See if there's a mesh to draw
      if((Mesh = List->m_Mesh) != NULL) {

        // Generate the mesh if using bones and set world matrix
        if(Mesh->m_NumBones && Mesh->m_SkinMesh != NULL) 
		{
		  // Difference in version 8.0 and 8.1 of directX and later )()()()()()()()()()()()()()()()(

          Mesh->m_SkinMesh->UpdateSkinnedMesh(Mesh->m_Matrices/*,add NULL for windows in xbox version*/, Mesh->m_Mesh);

		}

        // Loop through materials and draw the mesh
        for(i=0;i<Mesh->m_NumMaterials;i++) {
          // Don't draw materials with no alpha (0.0)
          if(Mesh->m_Materials[i].Diffuse.a != 0.0f) {
            m_pD3DDevice->SetMaterial(&Mesh->m_Materials[i]);
            m_pD3DDevice->SetTexture(0, Mesh->m_Textures[i]);

            // Enabled alpha blending based on material alpha
            if(Mesh->m_Materials[i].Diffuse.a != 1.0f)
			{
				m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
				m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCCOLOR);
				m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
			}
            Mesh->m_Mesh->DrawSubset(i);

			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
          }
        }
      }

      // Next mesh in list
      List = List->m_Next;
    }
  }

  // Next child mesh
  DrawFrame(Frame->m_Child);
  DrawFrame(Frame->m_Sibling);
}


void CObject::Move(float x, float y, float z)
{
	D3DXMatrixTranslation(&matPos,x,y,z);
	  
	D3DXMatrixIdentity( &matWorld );
	D3DXMatrixMultiply( &matWorld, &matRotation, &matPos );
	m_collision.SetWorldMatrix( matWorld );
}
void CObject::Rotate(float x, float y, float z)
{
	D3DXMATRIX mX, mY, mZ;
	D3DXMatrixRotationX( &mX, x );
	D3DXMatrixRotationY( &mY, y );
	D3DXMatrixRotationZ( &mZ, z );

	D3DXMatrixIdentity( &matRotation );
	D3DXMatrixMultiply( &matRotation, &matRotation, &mX );
	D3DXMatrixMultiply( &matRotation, &matRotation, &mY );
	D3DXMatrixMultiply( &matRotation, &matRotation, &mZ );

	D3DXMatrixIdentity( &matWorld );
	D3DXMatrixMultiply( &matWorld, &matRotation, &matPos );
	m_collision.SetWorldMatrix( matWorld );

}

/***************************************************************************/
/*                                                                         */
/*  CAnimation Code                                                        */
/*                                                                         */
/***************************************************************************/

CAnimation::CAnimation()
{
  m_NumAnimations = 0;
  m_AnimationSet  = NULL;
}


BOOL CAnimation::Load(char *Filename, CMesh *MapMesh)
{
  IDirectXFile           *pDXFile = NULL;
  IDirectXFileEnumObject *pDXEnum = NULL;
  IDirectXFileData       *pDXData = NULL;
  

  // Create the file object
  DirectXFileCreate(&pDXFile);

  // Register the templates
  pDXFile->RegisterTemplates((LPVOID)D3DRM_XTEMPLATES, D3DRM_XTEMPLATE_BYTES);

  // Create an enumeration object
  HRESULT e = pDXFile->CreateEnumObject((LPVOID)Filename, DXFILELOAD_FROMFILE, &pDXEnum);

  // Loop through all objects looking for the animation
  while(SUCCEEDED(pDXEnum->GetNextDataObject(&pDXData))) {
    ParseXFileData(pDXData, NULL, NULL);
    ReleaseCOM(pDXData);
  }

  ReleaseCOM(pDXEnum);
  ReleaseCOM(pDXFile);

  // Map the animation to the supplied mesh (if any)
  if(MapMesh != NULL)
    MapToMesh(MapMesh);

  return TRUE;
}

void CAnimation::ParseXFileData(IDirectXFileData *pDataObj, stAnimationSet *ParentAnim, stAnimation *CurrentAnim)
{
  IDirectXFileObject *pSubObj  = NULL;
  IDirectXFileData   *pSubData = NULL;
  IDirectXFileDataReference *pDataRef = NULL;
  const GUID      *Type = NULL;
  char            *Name = NULL;
  DWORD            Size;
  PBYTE           *DataPtr;

  DWORD            i;

  DWORD              KeyType, NumKeys, Time;
  sXFileRotateKey   *RotKey;
  sXFileScaleKey    *ScaleKey;
  sXFilePositionKey *PosKey;
  sXFileMatrixKey   *MatKey;

  stAnimationSet   *SubAnimSet = NULL;
  stAnimation      *SubAnim    = NULL;

  stAnimation      *Anim = NULL;
  stAnimationSet   *AnimSet = NULL;
  
  // Get the template type
  if(FAILED(pDataObj->GetType(&Type)))
    return;

  // Get the template name (if any)
  if(FAILED(pDataObj->GetName(NULL, &Size)))
    return;
  if(Size) {
    if((Name = new char[Size]) != NULL)
      pDataObj->GetName(Name, &Size);
  }

  // Give template a default name if none found
  if(Name == NULL) {
    if((Name = new char[9]) == NULL)
      return;
    strcpy(Name, "$NoName$");
  }

  // Set sub frame parent
  SubAnimSet = ParentAnim;
  SubAnim    = CurrentAnim;

  // Process the templates

  // Process an animation set
  if(*Type == TID_D3DRMAnimationSet) {
    // Create a animation set structure
    if((AnimSet = new stAnimationSet()) == NULL)
      return;

    // Set the name
    AnimSet->m_Name = Name;
    Name = NULL;

    // Link into the animation set list
    AnimSet->m_Next = m_AnimationSet;
    m_AnimationSet = AnimSet;

    // Set as new parent
    SubAnimSet = AnimSet;
  }

  // Process an animation
  if(*Type == TID_D3DRMAnimation && ParentAnim != NULL) {
    // Create an animation structure
    if((Anim = new stAnimation()) == NULL)
      return;

    // Set the name
    Anim->m_Name = Name;
    Name = NULL;

    // Link into the animation list
    Anim->m_Next = ParentAnim->m_Animation;
    ParentAnim->m_Animation = Anim;

    SubAnim = Anim;
  }

  // Process an animation key
  if(*Type == TID_D3DRMAnimationKey && CurrentAnim != NULL) {
    // Load in this animation's key data
    if(FAILED(pDataObj->GetData(NULL, &Size, (PVOID*)&DataPtr)))
      return;

    KeyType = ((DWORD*)DataPtr)[0];
    NumKeys = ((DWORD*)DataPtr)[1];

    switch(KeyType) {
      case 0:
        delete [] CurrentAnim->m_RotateKeys;
        if((CurrentAnim->m_RotateKeys = new sRotateKey[NumKeys]) == NULL)
          return;
        CurrentAnim->m_NumRotateKeys = NumKeys;
        RotKey = (sXFileRotateKey*)((char*)DataPtr + (sizeof(DWORD) * 2));
        for(i=0;i<NumKeys;i++) {
          CurrentAnim->m_RotateKeys[i].Time = RotKey->Time;
          CurrentAnim->m_RotateKeys[i].Quaternion.x = -RotKey->x;
          CurrentAnim->m_RotateKeys[i].Quaternion.y = -RotKey->y;
          CurrentAnim->m_RotateKeys[i].Quaternion.z = -RotKey->z;
          CurrentAnim->m_RotateKeys[i].Quaternion.w =  RotKey->w;

          if(RotKey->Time > ParentAnim->m_Length)
            ParentAnim->m_Length = RotKey->Time;

          RotKey += 1;
        }

        break;

      case 1: 
        delete [] CurrentAnim->m_ScaleKeys;
        if((CurrentAnim->m_ScaleKeys = new sScaleKey[NumKeys]) == NULL)
          return;
        CurrentAnim->m_NumScaleKeys = NumKeys;
        ScaleKey = (sXFileScaleKey*)((char*)DataPtr + (sizeof(DWORD) * 2));
        for(i=0;i<NumKeys;i++) {
          CurrentAnim->m_ScaleKeys[i].Time  = ScaleKey->Time;
          CurrentAnim->m_ScaleKeys[i].Scale = ScaleKey->Scale;

          if(ScaleKey->Time > ParentAnim->m_Length)
            ParentAnim->m_Length = ScaleKey->Time;

          ScaleKey += 1;
        }

        // Calculate the interpolation values
        if(NumKeys > 1) {
          for(i=0;i<NumKeys-1;i++) {
            CurrentAnim->m_ScaleKeys[i].ScaleInterpolation = CurrentAnim->m_ScaleKeys[i+1].Scale - CurrentAnim->m_ScaleKeys[i].Scale;
            Time = CurrentAnim->m_ScaleKeys[i+1].Time - CurrentAnim->m_ScaleKeys[i].Time;
            if(!Time)
              Time = 1;
            CurrentAnim->m_ScaleKeys[i].ScaleInterpolation /= (float)Time;
          }
        }

        break;
    
      case 2:
        delete [] CurrentAnim->m_PositionKeys;
        if((CurrentAnim->m_PositionKeys = new sPositionKey[NumKeys]) == NULL)
          return;
        CurrentAnim->m_NumPositionKeys = NumKeys;
        PosKey = (sXFilePositionKey*)((char*)DataPtr + (sizeof(DWORD) * 2));
        for(i=0;i<NumKeys;i++) {
          CurrentAnim->m_PositionKeys[i].Time = PosKey->Time;
          CurrentAnim->m_PositionKeys[i].Pos  = PosKey->Pos;

          if(PosKey->Time > ParentAnim->m_Length)
            ParentAnim->m_Length = PosKey->Time;

          PosKey += 1;
        }

        // Calculate the interpolation values
        if(NumKeys > 1) {
          for(i=0;i<NumKeys-1;i++) {
            CurrentAnim->m_PositionKeys[i].PosInterpolation = CurrentAnim->m_PositionKeys[i+1].Pos - CurrentAnim->m_PositionKeys[i].Pos;
            Time = CurrentAnim->m_PositionKeys[i+1].Time - CurrentAnim->m_PositionKeys[i].Time;
            if(!Time)
              Time = 1;
            CurrentAnim->m_PositionKeys[i].PosInterpolation /= (float)Time;
          }
        }

        break;

      case 4:
        delete [] CurrentAnim->m_MatrixKeys;
        if((CurrentAnim->m_MatrixKeys = new sMatrixKey[NumKeys]) == NULL)
          return;
        CurrentAnim->m_NumMatrixKeys = NumKeys;
        MatKey = (sXFileMatrixKey*)((char*)DataPtr + (sizeof(DWORD) * 2));
        for(i=0;i<NumKeys;i++) {
          CurrentAnim->m_MatrixKeys[i].Time   = MatKey->Time;
          CurrentAnim->m_MatrixKeys[i].Matrix = MatKey->Matrix;

          if(MatKey->Time > ParentAnim->m_Length)
            ParentAnim->m_Length = MatKey->Time;

          MatKey+=1;
        }

        // Calculate the interpolation matrices
        if(NumKeys > 1) {
          for(i=0;i<NumKeys-1;i++) {
            CurrentAnim->m_MatrixKeys[i].MatInterpolation = CurrentAnim->m_MatrixKeys[i+1].Matrix - CurrentAnim->m_MatrixKeys[i].Matrix;
            Time = CurrentAnim->m_MatrixKeys[i+1].Time - CurrentAnim->m_MatrixKeys[i].Time;
            if(!Time)
              Time = 1;
            CurrentAnim->m_MatrixKeys[i].MatInterpolation /= (float)Time;
          }
        }
        break;
    }
  }

  // Process animation options
  if(*Type == TID_D3DRMAnimationOptions && CurrentAnim != NULL) {
    // Load in this animation's options
    if(FAILED(pDataObj->GetData(NULL, &Size, (PVOID*)&DataPtr)))
      return;

    // Process looping information
    if(!((DWORD*)DataPtr)[0])
      CurrentAnim->m_Loop = TRUE;
    else
      CurrentAnim->m_Loop = FALSE;

    // Process linear information
    if(!((DWORD*)DataPtr)[1])
      CurrentAnim->m_Linear = FALSE;
    else
      CurrentAnim->m_Linear = TRUE;
  }

  // Process a frame reference
  if(*Type == TID_D3DRMFrame && CurrentAnim != NULL) {
    CurrentAnim->m_FrameName = Name;
    Name = NULL;

    // Don't enumerate child templates
    return;
  }

  // Release name buffer
  delete [] Name;

  // Scan for embedded templates
  while(SUCCEEDED(pDataObj->GetNextObject(&pSubObj))) {

    // Process embedded references
    if(SUCCEEDED(pSubObj->QueryInterface(IID_IDirectXFileDataReference, (void**)&pDataRef))) {
      if(SUCCEEDED(pDataRef->Resolve(&pSubData))) {
        ParseXFileData(pSubData, SubAnimSet, SubAnim);
        ReleaseCOM(pSubData);
      }
      ReleaseCOM(pDataRef);
    }

    // Process non-referenced embedded templates
    if(SUCCEEDED(pSubObj->QueryInterface(IID_IDirectXFileData, (void**)&pSubData))) {
      ParseXFileData(pSubData, SubAnimSet, SubAnim);
      ReleaseCOM(pSubData);
    }
    ReleaseCOM(pSubObj);
  }
}

BOOL CAnimation::Release()
{
  delete m_AnimationSet;
  m_AnimationSet  = NULL;
  m_NumAnimations = 0;

  return TRUE;
}


BOOL CAnimation::MapToMesh(CMesh *Mesh)
{
  stAnimationSet *AnimSet;
  stAnimation    *Anim;

  // Make sure there's a mesh to work with
  if(Mesh == NULL)
    return FALSE;

  // Assign links to frames by name
  if((AnimSet = m_AnimationSet) == NULL)
    return FALSE;

  // Scan through all animation sets
  while(AnimSet != NULL) {
    // Scan through all animations
    Anim = AnimSet->m_Animation;
    while(Anim != NULL) {
      // Find the matching frame from Mesh
      //Anim->m_Frame = Mesh->GetFrame(Anim->m_FrameName);
		//m_Frames->FindFrame(Name);
		Anim->m_Frame = Mesh->m_Frames->FindFrame(Anim->m_FrameName);
      Anim = Anim->m_Next;
    }
    AnimSet = AnimSet->m_Next;
  }

  return TRUE;
}



stAnimationSet *CAnimation::GetAnimationSet(char *Name)
{
  if(m_AnimationSet == NULL)
    return NULL;
  return m_AnimationSet->FindSet(Name);
}

BOOL CAnimation::SetLoop(BOOL ToLoop, char *Name)
{
  stAnimationSet *AnimSet;
  stAnimation    *Anim;

  if((AnimSet = GetAnimationSet(Name)) == NULL)
    return FALSE;

  Anim = AnimSet->m_Animation;
  while(Anim != NULL) {
    Anim->m_Loop = ToLoop;
    Anim = Anim->m_Next;
  }

  return TRUE;
}



