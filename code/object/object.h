/***************************************************************************/
/*                                                                         */
/*  File: object.h                                                         */
/*  URL: www.xfactordev.net                                                */
/*  Author: Ben_3D@xfactordev.net                                          */
/*  version: 1.0.0                                                         */
/*                                                                         */
/***************************************************************************/

#ifndef _XF_OBJECT_H_
#define _XF_OBJECT_H_

// Ben_3D@xfactordev.net

// .x file loader creator

//----------------------------------
#include <xtl.h>

#include <stdio.h>

// Very strange...but you need this or you get a _TID_D3DRMFrame link error!
#define INITGUID

//#include <d3dx8.h>

//#pragma comment(lib, "D3dxof.lib")
//#include <Dxfile.h>

#include "../collision/collision.h"

#include <rmxfguid.h>


#include <D3dx8mesh.h>
#pragma comment(lib, "D3dx8.lib")
//----------------------------------

#define ReleaseCOM(x) if(x) { x->Release(); x = NULL; }


class CMesh;
class CObject;
class CAnimation;

struct stMesh;
struct stFrame;
struct stAnimation;
struct stAnimationSet;

/***************************************************************************/
/*                                                                         */
/*  structure for Mesh                                                     */
/*                                                                         */
/***************************************************************************/


typedef struct stMesh
{
  char               *m_Name;            // Name of mesh

  ID3DXMesh          *m_Mesh;            // Mesh object
  ID3DXSkinMesh      *m_SkinMesh;        // Skin mesh object

  DWORD               m_NumMaterials;    // # materials in mesh
  D3DMATERIAL8        *m_Materials;       // Array of materials
  IDirect3DTexture8   **m_Textures;        // Array of textures

  DWORD               m_NumBones;        // # of bones
  ID3DXBuffer        *m_BoneNames;       // Names of bones
  ID3DXBuffer        *m_BoneTransforms;  // Internal transformations

  D3DXMATRIX         *m_Matrices;        // Bone matrices
  D3DXMATRIX        **m_FrameMatrices;   // Pointers to frame matrices
  D3DXMATRIX         *m_BoneMatrices;    // X file bone matrices

  stMesh              *m_Next;            // Next mesh in list

  stMesh()
  { 
    m_Name           = NULL;
    m_Mesh           = NULL;
    m_SkinMesh       = NULL;
    m_NumMaterials   = 0;		
	m_Materials      = NULL;
    m_Textures       = NULL;
    m_NumBones       = 0;		
	m_BoneNames      = NULL;
    m_BoneTransforms = NULL;
    m_Matrices       = NULL;
    m_FrameMatrices  = NULL;

    m_Next           = NULL;
  }

  ~stMesh()
  {
    delete [] m_Name;
    ReleaseCOM(m_Mesh);
    ReleaseCOM(m_SkinMesh);
    delete [] m_Materials;
    if(m_Textures != NULL) {
      for(DWORD i=0;i<m_NumMaterials;i++) {
        ReleaseCOM(m_Textures[i]);
      }
      delete [] m_Textures;
    }
    ReleaseCOM(m_BoneNames);
    ReleaseCOM(m_BoneTransforms);
    delete [] m_Matrices;
    delete [] m_FrameMatrices;
    delete m_Next;
  }

  stMesh *FindMesh(char *Name)
  {
    stMesh *Mesh;

    // Return first instance if name == NULL
    if(Name == NULL)
      return this;

    // Compare names and return if exact match
    if(m_Name != NULL && !strcmp(Name, m_Name))
      return this;

    // Search next in list
    if(m_Next != NULL) {
      if((Mesh = m_Next->FindMesh(Name)) != NULL)
        return Mesh;
    }

    return NULL;
  }

  void CopyFrameToBoneMatrices()
  {
    DWORD i;

    // Copy all matrices from frames to local bone matrix array
    if(m_NumBones && m_Matrices != NULL && m_FrameMatrices != NULL) {
      for(i=0;i<m_NumBones;i++) {
        if(m_FrameMatrices[i] != NULL)
          D3DXMatrixMultiply(&m_Matrices[i], &m_BoneMatrices[i], m_FrameMatrices[i]);
        else
          D3DXMatrixIdentity(&m_Matrices[i]);
      }
    }

    // Process next in list
    if(m_Next != NULL)
      m_Next->CopyFrameToBoneMatrices();
  }
} stMesh;



/***************************************************************************/
/*                                                                         */
/*  structure for FrameList                                                */
/*                                                                         */
/***************************************************************************/

typedef struct stFrameMeshList
{
  stMesh          *m_Mesh;
  stFrameMeshList *m_Next;

  stFrameMeshList()
  {
    m_Mesh = NULL;
    m_Next = NULL;
  }

  ~stFrameMeshList()
  {
    delete m_Next;
  }
} stFrameMeshList;

/***************************************************************************/
/*                                                                         */
/*  structure for Frame                                                    */
/*                                                                         */
/***************************************************************************/

typedef struct stFrame
{
  char       *m_Name;

  stFrameMeshList *m_MeshList;   // List of meshes attached to this frame

  D3DXMATRIX  m_matCombined;    // Combined transformation matrix
  D3DXMATRIX  m_matTransformed; // Currently transformed matrix
  D3DXMATRIX  m_matOriginal;    // Original .X file matrix
  
  stFrame     *m_Parent;         // Parent Frame
  stFrame     *m_Child;          // Child frame
  stFrame     *m_Sibling;        // Sibling frame

  stFrame() 
  {
    m_Name = NULL;
    m_MeshList = NULL;
    D3DXMatrixIdentity(&m_matCombined);
    D3DXMatrixIdentity(&m_matTransformed);
    D3DXMatrixIdentity(&m_matOriginal);
    m_Parent = m_Sibling = m_Child = NULL;
  }

  ~stFrame()
  {
    delete m_Name;
    delete m_MeshList;
    delete m_Child;
    delete m_Sibling;
  }

  stFrame *FindFrame(char *Name)
  {
    stFrame *Frame;

    // Return this instance if name == NULL
    if(Name == NULL)
      return this;
    
    // Compare names and return if exact match
    if(m_Name != NULL && !strcmp(Name, m_Name))
      return this;

    // Search child lists
    if(m_Child != NULL) {
      if((Frame = m_Child->FindFrame(Name)) != NULL)
        return Frame;
    }

    // Search sibling lists
    if(m_Sibling != NULL) {
      if((Frame = m_Sibling->FindFrame(Name)) != NULL)
        return Frame;
    }

    return NULL;
  }

  void ResetMatrices()
  {
    m_matTransformed = m_matOriginal;

    if(m_Child != NULL)
      m_Child->ResetMatrices();
    if(m_Sibling != NULL)
      m_Sibling->ResetMatrices();      
  }

  void AddMesh(stMesh *Mesh)
  {
    stFrameMeshList *List;

    List = new stFrameMeshList();
    List->m_Mesh = Mesh;
    List->m_Next = m_MeshList;
    m_MeshList = List;
  }
} stFrame;


/***************************************************************************/
/*                                                                         */
/*  container class for Mesh                                               */
/*                                                                         */
/***************************************************************************/

class CMesh
{
  public:
	IDirect3DDevice8* m_pD3DDevice;

	// Parent Mesh...the top of the tree..the ROOT of all meshes
    long       m_NumMeshes;
    stMesh     *m_Meshes;

	// The ROOT of all frames...the big parent...the top!
    long       m_NumFrames;
    stFrame    *m_Frames;

    void ParseXFileData(IDirectXFileData *pData, stFrame *ParentFrame, char *TexturePath);
    void MapFramesToBones(stFrame *Frame);

  public:
    CMesh();


	BOOL    Load(IDirect3DDevice8** pD3DDevice, char *Filename, char *TexturePath = ".\\");
    BOOL    Release();
};

/***************************************************************************/
/*                                                                         */
/*  Use-able class                                                         */
/*                                                                         */
/***************************************************************************/

class CObject
{
  protected:
	IDirect3DDevice8  *m_pD3DDevice;
    CMesh             *m_Mesh;
    stAnimationSet    *m_AnimationSet;

	D3DXMATRIX		  matPos;
	D3DXMATRIX		  matRotation;
	D3DXMATRIX		  matWorld;

    unsigned long  m_StartTime;

    void UpdateFrame(stFrame *Frame, D3DXMATRIX *Matrix);
    void DrawFrame(stFrame *Frame);
    void DrawMesh(stMesh *Mesh);
  
  public:

	CCollision m_collision;

  public:
    CObject();

	BOOL Create(IDirect3DDevice8** pD3DDevice, CMesh *Mesh = NULL);
    BOOL Release();

    BOOL SetAnimation(CAnimation *Animation, char *Name = NULL, unsigned long StartTime = 0);


    BOOL UpdateAnimation(unsigned long Time, BOOL Smooth = TRUE);
    // Stops animation.
	BOOL AnimationComplete(unsigned long Time);
 
    BOOL Render();

	void Move( float x, float y, float z );
	void Rotate( float x, float y, float z);
};


/***************************************************************************/
/*                                                                         */
/*  structures used in CAnimation class                                    */
/*                                                                         */
/***************************************************************************/

typedef struct 
{
	DWORD Time;
  DWORD Floats;
	float	w;
  float x;
  float y;
  float z;
} sXFileRotateKey;

typedef struct 
{
	DWORD	      Time;
  DWORD       Floats;	
	D3DXVECTOR3	Scale;	
} sXFileScaleKey;

typedef struct
{
	DWORD       Time;
  DWORD       Floats;	
	D3DXVECTOR3	Pos;	
} sXFilePositionKey;

typedef struct
{
	DWORD	      Time;
  DWORD       Floats;	
	D3DXMATRIX	Matrix;
} sXFileMatrixKey;

typedef struct
{
	DWORD          Time;
	D3DXQUATERNION Quaternion;
} sRotateKey;

typedef struct
{
	DWORD     	Time;
	D3DXVECTOR3	Pos;
	D3DXVECTOR3	PosInterpolation;
} sPositionKey;

typedef struct
{
	DWORD	      Time;
	D3DXVECTOR3	Scale;
	D3DXVECTOR3	ScaleInterpolation;
} sScaleKey;

typedef struct
{
	DWORD	     Time;
	D3DXMATRIX Matrix;
  D3DXMATRIX MatInterpolation;
} sMatrixKey;

/***************************************************************************/
/*                                                                         */
/*  structure for AnimationData                                            */
/*                                                                         */
/***************************************************************************/

typedef struct stAnimation
{
  char         *m_Name;
  char         *m_FrameName;
  stFrame       *m_Frame;

  BOOL          m_Loop;
  BOOL          m_Linear;

  DWORD           m_NumPositionKeys;
  sPositionKey *m_PositionKeys;

  DWORD           m_NumRotateKeys;
  sRotateKey   *m_RotateKeys;

  DWORD           m_NumScaleKeys;
  sScaleKey    *m_ScaleKeys;

  DWORD           m_NumMatrixKeys;
  sMatrixKey     *m_MatrixKeys;

  stAnimation   *m_Next;

  stAnimation()
  {
    m_Name            = NULL;
    m_FrameName       = NULL;
    m_Frame           = NULL;
    m_Loop            = FALSE;
    m_Linear          = TRUE;
    m_NumPositionKeys = 0;
    m_PositionKeys    = NULL;
    m_NumRotateKeys   = 0;
    m_RotateKeys      = NULL;
    m_NumScaleKeys    =0;
    m_ScaleKeys       = NULL;
    m_NumMatrixKeys   = 0;
    m_MatrixKeys      = NULL;
    m_Next            = NULL;
  }

  ~stAnimation()
  {
    delete [] m_Name;
    delete [] m_FrameName;
    delete [] m_PositionKeys;
    delete [] m_RotateKeys;
    delete [] m_ScaleKeys;
    delete [] m_MatrixKeys;
    delete m_Next;
  }

  void Update(DWORD Time, BOOL Smooth)
  {
    unsigned long i, Key;
    DWORD TimeDiff, IntTime;
    D3DXMATRIX     Matrix, MatTemp;
    D3DXVECTOR3    Vector;
    D3DXQUATERNION Quat;
    
    if(m_Frame == NULL)
      return;

    // update rotation, scale, and position keys
    if(m_NumRotateKeys || m_NumScaleKeys || m_NumPositionKeys) {
      D3DXMatrixIdentity(&Matrix);

      if(m_NumRotateKeys && m_RotateKeys != NULL) {
        // Find the key that fits this time
        Key = 0;
        for(i=0;i<m_NumRotateKeys;i++) {
          if(m_RotateKeys[i].Time <= Time)
            Key = i;
          else
            break;
        }

        // If it's the last key or non-smooth animation,
        // then just set the key value.
        if(Key == (m_NumRotateKeys-1) || Smooth == FALSE) {
          Quat = m_RotateKeys[Key].Quaternion;
        } else {
          // Calculate the time difference and 
          // interpolate time.
          TimeDiff = m_RotateKeys[Key+1].Time - m_RotateKeys[Key].Time;
          IntTime  = Time - m_RotateKeys[Key].Time;

          // Get the quaternion value
          D3DXQuaternionSlerp(&Quat, &m_RotateKeys[Key].Quaternion, &m_RotateKeys[Key+1].Quaternion, ((float)IntTime / (float)TimeDiff));
        }

        // Combine with the new matrix
        D3DXMatrixRotationQuaternion(&MatTemp, &Quat);
        D3DXMatrixMultiply(&Matrix, &Matrix, &MatTemp);
      }

      if(m_NumScaleKeys && m_ScaleKeys != NULL) {
        // Find the key that fits this time
        Key = 0;
        for(i=0;i<m_NumScaleKeys;i++) {
          if(m_ScaleKeys[i].Time <= Time)
            Key = i;
          else
            break;
        }

        // If it's the last key or non-smooth animation,
        // then just set the key value.
        if(Key == (m_NumScaleKeys-1) || Smooth == FALSE) {
          Vector = m_ScaleKeys[Key].Scale;
        } else {
          // Calculate the time difference and 
          // interpolate time.
          IntTime  = Time - m_ScaleKeys[Key].Time;

          // Get the interpolated vector value
          Vector = m_ScaleKeys[Key].Scale + m_ScaleKeys[Key].ScaleInterpolation * (float)IntTime;
        }

        // Combine with the new matrix
        D3DXMatrixScaling(&MatTemp, Vector.x, Vector.y, Vector.z);
        D3DXMatrixMultiply(&Matrix, &Matrix, &MatTemp);
      }

      if(m_NumPositionKeys && m_PositionKeys != NULL) {
        // Find the key that fits this time
        Key = 0;
        for(i=0;i<m_NumPositionKeys;i++) {
          if(m_PositionKeys[i].Time <= Time)
            Key = i;
          else
            break;
        }

        // If it's the last key or non-smooth animation,
        // then just set the key value.
        if(Key == (m_NumPositionKeys-1) || Smooth == FALSE) {
          Vector = m_PositionKeys[Key].Pos;
        } else {
          // Calculate the time difference and 
          // interpolate time.
          IntTime  = Time - m_PositionKeys[Key].Time;

          // Get the interpolated vector value
          Vector = m_PositionKeys[Key].Pos + m_PositionKeys[Key].PosInterpolation * (float)IntTime;
        }

        // Combine with the new matrix
        D3DXMatrixTranslation(&MatTemp, Vector.x, Vector.y, Vector.z);
        D3DXMatrixMultiply(&Matrix, &Matrix, &MatTemp);
      }

      // Set the new matrix
      m_Frame->m_matTransformed = Matrix;
    }

    // update matrix keys
    if(m_NumMatrixKeys && m_MatrixKeys != NULL) {
      // Find the key that fits this time
      Key = 0;
      for(i=0;i<m_NumMatrixKeys;i++) {
        if(m_MatrixKeys[i].Time <= Time)
          Key = i;
        else
          break;
      }

      // If it's the last key or non-smooth animation,
      // then just set the matrix.
      if(Key == (m_NumMatrixKeys-1) || Smooth == FALSE) {
        m_Frame->m_matTransformed = m_MatrixKeys[Key].Matrix;
      } else {
        // Calculate the time difference and 
        // interpolate time.
        IntTime  = Time - m_MatrixKeys[Key].Time;

        // Set the new interpolation matrix
        Matrix = m_MatrixKeys[Key].MatInterpolation * (float)IntTime;
        m_Frame->m_matTransformed = Matrix + m_MatrixKeys[Key].Matrix;
      }
    }
  }
} stAnimation;

/***************************************************************************/
/*                                                                         */
/*  structure for AnimationSet                                             */
/*                                                                         */
/***************************************************************************/

typedef struct stAnimationSet
{
  char          *m_Name;
  stAnimation    *m_Animation;

  unsigned long  m_Length;

  stAnimationSet *m_Next;

  stAnimationSet()
  {
    m_Name      = NULL;
    m_Animation = NULL;
    m_Length    = 0;
    m_Next      = NULL;
  }

  ~stAnimationSet()
  {
    delete [] m_Name;
    delete m_Animation;
    delete m_Next;
  }

  stAnimationSet *FindSet(char *Name)
  {
    stAnimationSet *AnimSet;

    // return first instance if name=NULL
    if(Name == NULL)
      return this;

    // Compare names and return if exact match
    if(m_Name != NULL && !strcmp(Name, m_Name))
      return this;

    // Search next in list
    if(m_Next != NULL) {
      if((AnimSet = m_Next->FindSet(Name)) != NULL)
        return AnimSet;
    }

    return NULL;
  }

  void Update(DWORD Time, BOOL Smooth)
  {
    stAnimation *Anim;

    Anim = m_Animation;
    while(Anim != NULL) {
      if(!m_Length)
        Anim->Update(0, FALSE);
      else
      if(Time >= m_Length && Anim->m_Loop == FALSE)
        Anim->Update(Time, FALSE);
      else
        Anim->Update((Time % m_Length), Smooth);

      Anim = Anim->m_Next;
    }
  }

} stAnimationSet;

/***************************************************************************/
/*                                                                         */
/*  container class CAnimation where it all comes together                 */
/*                                                                         */
/***************************************************************************/

class CAnimation
{
  protected:
    long           m_NumAnimations;
    stAnimationSet *m_AnimationSet;

    void ParseXFileData(IDirectXFileData *DataObj, stAnimationSet *ParentAnim, stAnimation *CurrentAnim);

  public:
    CAnimation();


    stAnimationSet *GetAnimationSet(char *Name = NULL);

    BOOL Load(char *Filename, CMesh *MapMesh = NULL);
    BOOL Release();

    BOOL MapToMesh(CMesh *Mesh);

    BOOL SetLoop(BOOL ToLoop, char *Name = NULL);
};



#endif // _XF_OBJECT_H_
