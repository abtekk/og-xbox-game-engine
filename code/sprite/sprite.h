/**********************************************************************************/
/*                                                                                */
/*    File: sprite.h                                                              */
/*    Details: CSprite                                                            */
/*    Auth: Ben_3D                                                                */
/*    Ben_3D@xfactordev.net                                                       */
/*    version: 0.0.1                                                              */
/*                                                                                */
/**********************************************************************************/
/*
  <Why What?>
  What does it does?  Why would we want a CSprite class... well with very little
  effort you can now create some stunning explosions...animated characters etc...
  ..how?...well we create a rectangle and apply a texture to it...then with a 
  bit of timing we jsut change the texture after a small delay...so in effect your
  getting the appearance of animation!..WOW!

  Yup, you can now animate an Explosion, a simple tree blowing ...or a 3D charater
  in the distance...which because its in the distance you coudl use a bitmap and
  animate it...when it gets closer go back to hte 3D charcter :)

  Its all about tricks...and how you use them :)

  <USING IT?>
  CSprite s;
  s.Create( pDevice, "explosion_", 8, "C:\\images", ".bmp");

  // Default it at origin anyhow!
  s.Move(0.0f, 0.0f, 0.0f);

  s.Update(time);
  s.Render();

  s.Release();

  <IMPORTANT!>
  Textures are in the form...
  texturename+texturenumber+filetype...for example...if you say you've got 3 textures, with
  the name "explosion" and type ".bmp"
  The textures are loaded like this:
  explosion01.bmp
  explosion02.bmp
  explosion03.bmp

  Make sure you remember that!

  <Maybe?>
  Well if you have the time, and prefair it, you could do an Add(..) texutre member
  function instead of doing it like I did in the Create(..) function..

*/
/**********************************************************************************/

#pragma once

#include <xtl.h>
#include <stdio.h>

class CSprite
{
	IDirect3DDevice8* m_pD3DDevice;
	int m_iNumTextures;
	IDirect3DTexture8* m_pTextures[20]; // Set a maximum of 20 textures
	char m_szFolder[256];

	IDirect3DVertexBuffer8* m_pVertexBuffer;
	int m_iCurrentTexture;

	float m_fCurrentTime;
	
	float m_xpos, m_ypos, m_zpos;
	float m_fScale;

	D3DXMATRIX m_matWorld;

	UINT D3DFVF_CUSTOMVERTEX;

	struct CUSTOMVERTEX
	{
		float x, y, z; // The transformed position for the vertex.
		float tu, tv;  // The vertex colour.
	};

public:
	void Create(IDirect3DDevice8** pDevice, char* szBaseName, int iNumTextures, char* szFolder=".\\", char* szFileFormat=".bmp")
	{
		m_pD3DDevice = *pDevice;
		m_iNumTextures = iNumTextures;

		m_fCurrentTime = 0.0f;
		
		D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ|D3DFVF_TEX1;

		ZeroMemory(m_pTextures, sizeof(m_pTextures));

		// Lets its default location to the middle of our world
		m_xpos = 0.0f;
		m_ypos = 0.0f;
		m_zpos = 0.0f;
		m_fScale = 1.0f;

		// Our first texture for our sprite will be 0
		m_iCurrentTexture = 0;

		// This is where we craete our vertices
		m_pD3DDevice->CreateVertexBuffer(6 * sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,
										D3DPOOL_DEFAULT, &m_pVertexBuffer);
		CUSTOMVERTEX pVertices[6] = 
		{ 
			{ -0.5f, -0.5f, 0.0f,  0.0f, 0.0f},
			{ -0.5f, 0.5f, 0.0f,  0.0f, 1.0f},
			{ 0.5f, 0.5f, 0.0f,  1.0f, 1.0f},

			{ 0.5f, 0.5f, 0.0f,  1.0f, 1.0f},
			{ 0.5f, -0.5f, 0.0f,  1.0f, 0.0f},
			{ -0.5f, -0.5f, 0.0f,  0.0f, 0.0f}
		};

		void* p;
		m_pVertexBuffer->Lock(0, 6 * sizeof(CUSTOMVERTEX), (BYTE**)&p, 0);
		memcpy(p, pVertices, sizeof(pVertices));
		m_pVertexBuffer->Unlock();

		// Load all our textures
		
		for(int i=0; i<m_iNumTextures; i++)
		{
			int current_image = i;
			current_image = (current_image % m_iNumTextures); // e.g. 1 to 8...not 0 to 7
			current_image++;
			char szFileName[200];
			//sprintf(szFileName, "explosion_%.2d.bmp", current_image);
			sprintf(szFileName, "%s%.2d%s", szBaseName, current_image, szFileFormat);
			// szFileName should now contain for example 'explosion_''01''.bmp'
			// without the comma's

			// Lets add the folder location to the file name:
			char szFile[256];
			strcpy( szFile, szFolder );
			strcat( szFile, szFileName );

			// You could use the basic texture loading code, BUT! it would only make your
			// sprite/explosion transparent, if your transparent colour is black...
			// as we are adding the background to the current texture...and as you know
			// black is 0xff000000 ..we are adding nothing to the current background.
			//D3DXCreateTextureFromFile(m_pD3DDevice, szFile, &(m_pTextures[i]));

			// This method, allows us to set the background transparent texture colour...
			// of course I've just set it to 0xff000000 (e.g. black)..but it allows you
			// to improve it or change it here.
			D3DXCreateTextureFromFileEx(m_pD3DDevice, //Our D3D Device
                                  szFile,    //Filename of our texture
                                  D3DX_DEFAULT, //Width:D3DX_DEFAULT = Take from file 
                                  D3DX_DEFAULT, //Height:D3DX_DEFAULT = Take from file
                                  1,            //MipLevels
                                  0,            //Usage, Is this to be used as a Render Target? 0 == No
                                  D3DFMT_A8R8G8B8, //32-bit with Alpha, everything should support this
                                  D3DPOOL_MANAGED,//Pool, let D3D Manage our memory
                                  D3DX_DEFAULT, //Filter:Default filtering
                                  D3DX_DEFAULT, //MipFilter, used for filtering mipmaps
                                  //0,            //Disable ColourKey
								  0xff000000,
                                  NULL,         //SourceInfo, returns extra info if we want it (we don't)
                                  NULL,         //Palette:We're not using one
                                   &(m_pTextures[i]));  // Our texture goes here.


		}
		

	}
	void Release()
	{
		for(int i=0; i<m_iNumTextures; i++)
		{
			if( m_pTextures[i] )
				m_pTextures[i]->Release();
		}
	}
public:
	// Returns true if its completed a complete cycle once
	bool Update(float t)
	{
		if( t > m_fCurrentTime + 1.0f )
		{
			m_fCurrentTime = t;

			m_iCurrentTexture ++;
			m_iCurrentTexture = m_iCurrentTexture % m_iNumTextures;  // e.g. 1 to 8...not 0 to 7

			if( m_iCurrentTexture == 0 )
				return true;

		}

		return false;
	}

	void Move(float xpos, float ypos, float zpos)
	{
		m_xpos = xpos;
		m_ypos = ypos;
		m_zpos = zpos;
	}
	// Uniform scalling in all the direction
	void Scale( float s)
	{
		m_fScale = s;

	}

	void Render()
	{

		D3DXMATRIX matLocation;
		D3DXMatrixTranslation(&matLocation, m_xpos, m_ypos, m_zpos);
		D3DXMATRIX matScale;
		D3DXMatrixScaling(&matScale, m_fScale, m_fScale, 1.0f);
		//matLocation = matScale * matLocation;

		// <Billboarding start> These few lines do an amazing trick, it basically 
		// makes it so that our sprite is always facing the front..
		D3DXMATRIX matView;
		m_pD3DDevice->GetTransform(D3DTS_VIEW,&matView);
		m_matWorld = matView;
		D3DXMATRIX matTranspose;
		D3DXMatrixTranspose(&matTranspose, &m_matWorld);
		D3DXMatrixIdentity(&m_matWorld);

		m_matWorld._11 = matTranspose._11;
		m_matWorld._12 = matTranspose._12;
		m_matWorld._13 = matTranspose._13;
		m_matWorld._21 = matTranspose._21;
		m_matWorld._22 = matTranspose._22;
		m_matWorld._23 = matTranspose._23;
		m_matWorld._31 = matTranspose._31;
		m_matWorld._32 = matTranspose._32;
		m_matWorld._33 = matTranspose._33;
		

		/*
		m_matWorld._41 = matLocation._41;
		m_matWorld._42 = matLocation._42;
		m_matWorld._43 = matLocation._43;		
		*/
		// <Billboarding end>
		m_matWorld = matScale * m_matWorld * matLocation;
		m_pD3DDevice->SetTransform( D3DTS_WORLD, &m_matWorld );
		//m_pD3DDevice->SetTransform( D3DTS_WORLD, &matLocation );
		

		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,true);		  //alpha blending enabled
		m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);	  //source blend factor
		m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_DESTALPHA); //destination blend factor

		m_pD3DDevice->SetTextureStageState(0,D3DTSS_ALPHAARG1,D3DTA_TEXTURE);	//alpha from texture

		m_pD3DDevice->SetTexture(0,m_pTextures[m_iCurrentTexture]);

		m_pD3DDevice->SetRenderState(D3DRS_ZENABLE, false);

		m_pD3DDevice->SetStreamSource(0, m_pVertexBuffer, sizeof(CUSTOMVERTEX));
		m_pD3DDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
		m_pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

		m_pD3DDevice->SetRenderState(D3DRS_ZENABLE, true);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,false);		  //turn off

		m_pD3DDevice->SetTexture(0,NULL);

		m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
        m_pD3DDevice->SetTextureStageState(0,D3DTSS_COLORARG1, D3DTA_DIFFUSE);
		

	}
};
// End of our amazing..faboulour...spectacular CSprite class.






/**********************************************************************************/
/*
Bugs/Fixes:

*/
/**********************************************************************************/