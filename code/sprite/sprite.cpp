/**********************************************************************************/
/*                                                                                */
/*    File: sprite.cpp                                                            */
/*    Details: CSprite                                                            */
/*    Auth: Ben_3D                                                                */
/*    Ben_3D@xfactordev.net                                                       */
/*    version: 0.0.1                                                              */
/*                                                                                */
/**********************************************************************************/
/*
  <Why What?>
  What does it does?  Why would we want a CSprite class... well with very little
  effort you can now create some stunning explosions...animated characters etc...
  ..how?...well we create a rectangle and apply a texture to it...then with a 
  bit of timing we jsut change the texture after a small delay...so in effect your
  getting the appearance of animation!..WOW!

  Yup, you can now animate an Explosion, a simple tree blowing ...or a 3D charater
  in the distance...which because its in the distance you coudl use a bitmap and
  animate it...when it gets closer go back to hte 3D charcter :)

  Its all about tricks...and how you use them :)

  <USING IT?>
  CSprite s;
  s.Create( pDevice, "explosion_", 8, "C:\\images");

  // Default it at origin anyhow!
  s.Move(0.0f, 0.0f, 0.0f);

  s.Update(time);
  s.Render();

  s.Release();

  <IMPORTANT!>
  Textures are in the form...
  texturename+texturenumber...for example...if you say you've got 3 textures, with
  the name "explosion"
  The textures are loaded like this:
  explosion01.bmp
  explosion02.bmp
  explosion03.bmp

  Make sure you remember that!


*/
/**********************************************************************************/

#include "sprite.h"









/**********************************************************************************/
/*
Bugs/Fixes:

*/
/**********************************************************************************/

