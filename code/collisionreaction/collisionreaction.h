/***************************************************************************/
/*                                                                         */
/*  File: collisionreaction.h                                              */
/*                                                                         */
/*  Details: Collision Reaction Code                                       */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/





#ifndef _XF_COLLISIONREACTION_H
#define _XF_COLLISIONREACTION_H


#include <xtl.h>


#include "../graphics_helpers/graphics_helpers.h"

#include "../text/text.h"

D3DXVECTOR3 CollisionReaction(D3DXVECTOR3 vDirection, float* p1, float* p2, float* p3, IDirect3DDevice8 *pD=NULL);


D3DXVECTOR3 CollisionHeightReaction(D3DXVECTOR3 vDirection, float* p1, float* p2, float* p3, IDirect3DDevice8 *pD);






#endif // _XF_COLLISIONREACTION_H