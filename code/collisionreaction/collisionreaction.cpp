/***************************************************************************/
/*                                                                         */
/*  File: collisionreaction.cpp                                            */
/*                                                                         */
/*  Details: Collision Reaction Code                                       */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           Ben_3D@xfactordev.net or contact@xfactordev.net               */
/*                                                                         */
/***************************************************************************/

#include "collisionreaction.h"


#include <stdio.h>

extern CText g_TextDebug;


D3DXVECTOR3 CollisionReaction(D3DXVECTOR3 vDirection, float* p1, float* p2, float* p3, IDirect3DDevice8 *pD)
{

	D3DXVECTOR3 vA = D3DXVECTOR3(p1[0],p1[1],p1[2]);
	D3DXVECTOR3 vB = D3DXVECTOR3(p2[0],p2[1],p2[2]);
	D3DXVECTOR3 vC = D3DXVECTOR3(p3[0],p3[1],p3[2]);

	D3DXVECTOR3 v1, v2;
	v1 = vB-vA;
	v2 = vC-vA;

	D3DXVECTOR3 vN;
	D3DXVec3Cross(&vN, &v1, &v2);


	// Debug Code
	if(pD != NULL)
	{
		arrow(pD, vA, vN);
	}

	D3DXVec3Normalize(&vN, &vN);
	D3DXVec3Normalize(&vDirection, &vDirection);

	D3DXMATRIX RotY;
	D3DXMatrixRotationY(&RotY, 3.14f/2.0f);
	D3DXVec3TransformCoord(&vDirection, &vDirection, &RotY);

	float fUpOrDown = D3DXVec3Dot( &vN, &vDirection );


	float fSpeed = 1.0f;

	D3DXVECTOR3 vWallDirection;

	D3DXVECTOR3 vUp = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXVec3Cross(&vWallDirection, &vN, &vUp);

	D3DXVec3Normalize(&vWallDirection, &vWallDirection);


	D3DXVECTOR3 vNewDir = fSpeed * fUpOrDown * vWallDirection;
	vNewDir += vN*0.13f;



	// Used In Debugging
	//wchar_t buf[200];
	//swprintf(buf, L"Clsn Reaction: fUpDown %.2f", fUpOrDown);
	//g_TextDebug.TextOut( buf, 100.0f, 100.0f);
	//pD->SetRenderState(D3DRS_ZENABLE, true);



	return vNewDir;
}



//======================================================================
// Well the floor...ground hight may change, and our collisionreactin function
// at present only does x and z...not reacting to height...so we'll fix that
// here, by adding a height direction upon reaction.

// Quick reminder of how it works..to be details later.
// We use the triangles three points...and a line...which we approx as +1000 and -1000 in the y
// above and below our point...then we discover the point of intersection with our triangle.

// Uses the principle of a line-triangle intersection.


D3DXVECTOR3 CollisionHeightReaction(D3DXVECTOR3 vDirection, float* p1, float* p2, float* p3, IDirect3DDevice8 *pD)
{


	vDirection.y = 1000.0f;

	// Get our collision triangles normal
	D3DXVECTOR3 vA = D3DXVECTOR3(p1[0],p1[1],p1[2]);
	D3DXVECTOR3 vB = D3DXVECTOR3(p2[0],p2[1],p2[2]);
	D3DXVECTOR3 vC = D3DXVECTOR3(p3[0],p3[1],p3[2]);

	D3DXVECTOR3 v1, v2;
	v1 = vB-vA;
	v2 = vC-vA;

	D3DXVECTOR3 vN;
	D3DXVec3Cross(&vN, &v1, &v2);


	// use vPoly[0] as the reference for x,y,z in the equation
	float DistanceFromOrigin = (-vN.x*vA.x - vN.y*vA.y - vN.z*vA.z);


	D3DXVECTOR3 vP1 = vDirection;
	D3DXVECTOR3 vP2 = vDirection;
	vP2.y -= 1000.0f;
		
	D3DXVECTOR3 vLineDirection = vP2 - vP1;
	D3DXVec3Normalize(&vLineDirection, &vLineDirection);

	float Numerator = -(vN.x*vP1.x + vN.y*vP1.y + vN.z*vP1.z + DistanceFromOrigin);

	float Denominator = D3DXVec3Dot( &vN, &vLineDirection );

	// There doesn't seem to be a triangle below us, so we just drop!
	if( Denominator == 0.0f )
	{
		return D3DXVECTOR3(0,-1.0f,0); // no triangles below...so fall..fallll!!

	}

	float dist = Numerator / Denominator;

	D3DXVECTOR3 vPoint;
	vPoint.x = vP1.x + (vLineDirection.x*dist);
	vPoint.y = vP1.y + (vLineDirection.y*dist);
	vPoint.z = vP1.z + (vLineDirection.z*dist);


	//DEBUGGING
	arrow(pD, vPoint, vN);

	
	D3DXVECTOR3 vNewDir = vPoint;
	return vNewDir;
}




