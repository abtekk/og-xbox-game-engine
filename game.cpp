/***************************************************************************/
/*                                                                         */
/*  game.cpp                                                               */
/*                                                                         */
/***************************************************************************/
/***************************************************************************/
/*                                                                         */
/*  File: game.cpp                                                         */
/*                                                                         */
/*  Details: Game Start Code                                               */
/*                                                                         */
/*  Auth:    x-factor development (http://www.xfactordev.net)              */
/*           contact@xfactordev.net                                        */
/*                                                                         */
/***************************************************************************/


#include "game.h"


CText g_TextDebug;

// Initilisation called only once in the constructor
CGame::CGame()
{
	m_g.Create();
	m_g.GetScreen( &m_pD3DDevice );

	m_camera.Create( &m_pD3DDevice );
	m_camera.Move(0,30, -200);
	m_camera.SetPoint(0, 30, -200,   0.0f, 0.0f, 0.0f);

	textA.Create( &m_pD3DDevice );
	textB.Create( &m_pD3DDevice );
	g_TextDebug.Create(&m_pD3DDevice);

	textFPS.Create( &m_pD3DDevice );

	m_gamepad.InitGamePad();

	// This is our animated character
	// e.g. Swing, Hurt, Walk, Die, Spell, Idle

	mymesh.Load( &m_pD3DDevice, "D:\\media\\model\\warrior.x", "D:\\media\\model\\");
	myobject.Create( &m_pD3DDevice, &mymesh);

	myanimation.Load("D:\\media\\model\\warrior.x", &mymesh);
	myanimation.SetLoop(TRUE, "Walk");
	myobject.SetAnimation(&myanimation, "Walk", 0);

	// Old none octree world loader
	//m_world.Create(&m_pD3DDevice, "D:\\media\\world\\world1.x", "D:\\media\\world\\");

	m_Bullet.Create(&m_pD3DDevice, "D:\\media\\model\\bullet1.x", "D:\\media\\model\\");

	m_explosion.Create(&m_pD3DDevice, "explosion_", 8,  "D:\\media\\explosion\\", ".bmp");

	m_Light.Create(&m_pD3DDevice, 0);

	m_OctreeWorld.Create(&m_pD3DDevice, "D:\\media\\world\\demo_world.3ds", "D:\\media\\world\\");

}


// abs_f and GetFacingDirection are global functions....yet to be decided where to be 
// put...e.g. UserFunctions.cpp/.h maybe?  

float abs_f(float p)
{
	if(p > 0.0f)
		return p;
	if(p < 0.0f)
		return -p;
	return 0.0f;
}

/* which direction our character faces

	/|\      | 	     pi/2
     | pi    | 0    ---->    <----        
     |      \|/                3pi/2
*/

float GetFacingDirection(float zz, float xx)
{
	static float yangle = 0.0f;

	if( ( abs_f(xx) > 0.1f ) && ( abs_f(zz) > 0.1f ) )
	{
		if( (xx>0) && (zz>0) ) //top right
			yangle = (float)( -atan(xx/zz))+ 3.14f/2.0f;

		if( (xx>0) && (zz<0) )  //bottom right
			yangle = 3.14f + (float)atan( abs_f((xx/zz)) )+ 3.14f/2.0f;

		if( (xx<0) && (zz<0) )  //bottom left
				yangle =(float)( (3.14f) -atan((xx/zz) ))+ 3.14f/2.0f;

		if( (xx<0) && (zz>0) )  //top left
			yangle = (float)( -atan((xx/zz)) )+ 3.14f/2.0f;
	}
	else if ( abs_f(xx) > 0.5f  )
	{
		
		if(xx > 0.5f)// up
		{
			yangle = 0.0f;
		}
		else if (xx < 0.5f)
		{
			yangle = 3.14f;
		}
		
	}
	else if( abs_f(zz) > 0.5f )
	{
		if(zz > 0.5f) // right
		{
			yangle = 3.14f/2.0f;
		}
		else if( zz < 0.5f )
		{
			yangle = (3.0f*3.14f)/2.0f;
		}
	}


	return yangle;
}




// GameLoop, gets called repeatedly while it returns 1;
bool CGame::GameLoop()
{

	static float fps = 0.0f;
	static float fps_max = 0.0f;
	if( timerFPS.GetTime() > 1000 )
	{
		fps_max = fps;
		fps=0.0f;
		timerFPS.Reset();
	}
	fps++;

	WCHAR szwBuf[200];
	wsprintfW(szwBuf, L"FPS: %.3f", 2 * fps_max); // double it so you get a nice number for hte audience ;)
	textFPS.TextOut(szwBuf, 20.0f, 45.0f);

	// This will update all our timers, 
	textA.TextOut(L"www.xfactordev.net");

	timerA.UpdateAll();

	static float xpos = 0.0f;
	static float ypos = 10.0f;
	static float zpos = 0.0f;

	m_gamepad.GetInput();

	float speed = 0.0f; // set by the gamepad
	float scale = 3.0; // speed variable on how fast our man moves

	// default is stand doing nothing
	float horz = 0.0f;
	float vert  = 0.0f;

	D3DXVECTOR3 vDirection = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if( m_gamepad.iRightStickUp() )
	{
		vert = m_gamepad.iRightStickUp();

		timerA.Reset();
		myobject.SetAnimation(&myanimation, "Walk", 0);
		//zpos += vert * scale;
		vDirection.z = vert*scale;
	}
	if( m_gamepad.iRightStickDown() )
	{
		vert = m_gamepad.iRightStickDown();

		timerA.Reset();
		myobject.SetAnimation(&myanimation, "Walk", 0);
		//zpos += vert * scale;
		vDirection.z = vert*scale;
	}
	if( m_gamepad.iRightStickRight() )
	{
		horz = m_gamepad.iRightStickRight();

		timerA.Reset();
		myobject.SetAnimation(&myanimation, "Walk", 0);
		//xpos += horz * scale;
		vDirection.x = horz*scale;
	}
	if( m_gamepad.iRightStickLeft() )
	{
		horz = m_gamepad.iRightStickLeft();

		timerA.Reset();
		myobject.SetAnimation(&myanimation, "Walk", 0);
		//xpos += horz * scale;
		vDirection.x = horz*scale;
	}

	if( m_gamepad.bAButtonPressed() )
	{
		myobject.SetAnimation(&myanimation, "Swing", 0);
		myanimation.SetLoop(TRUE, "Swing");
		timerA.Reset();
	}




	speed = abs_f(vert) + abs_f(horz);

	static float camera_y_angle = 0.0f;
	static float camera_x_angle = -0.3f;


	// vDirection is the new movement.
	D3DXMATRIX rot_y;
	D3DXMatrixRotationY(&rot_y, -camera_y_angle);

	D3DXVECTOR3 newDirection;
	D3DXVec3TransformCoord(&newDirection, &vDirection, &rot_y);

	float temp_xpos = xpos + newDirection.x*1.0f;
	float temp_zpos = zpos + newDirection.z*1.0f;


	D3DXVECTOR3 vCharFacing = D3DXVECTOR3(horz, 0.0f, vert);
	D3DXVec3TransformCoord(&vCharFacing, &vCharFacing, &rot_y);

	horz = vCharFacing.x;
	vert = vCharFacing.z;


	textB.TextOut(L"Collision", 30.0f, 60.0f);

	static bool bCollision = true;

	unsigned char c = 0;
	if( bCollision == true )
		c = 255;

	m_pD3DDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(c, 0, 255), 1.0f, 0);
	// Begin the scene
	m_pD3DDevice->BeginScene();

	// 60 frames per second...1000millisconds in a second
	// 1000/100 = 10 times a second
	
	static unsigned long d=0;
	if( timerA.GetTime() > 400 )
	{
		myobject.SetAnimation(&myanimation, "Idle", 0);
		myanimation.SetLoop(TRUE, "Idle");
		//timerA.Reset();
	}
	
	
	D3DXMATRIX matWorld;
	D3DXMatrixTranslation( &matWorld, 0.0f, 0.0f, 0.0f);
	m_pD3DDevice->SetTransform( D3DTS_WORLD, &matWorld );

	//m_world.Render();
	m_OctreeWorld.Render();
	

	static float f1 = 0.0f;
	f1 += 0.3f + ( abs_f((speed/2.0f)));
	
	// Determine which direction where facing
	static float y_angle = 0.0f;

/* which direction our character faces

	/|\      | 	     pi/2
     | pi    | 0    ---->    <----        
     |      \|/                3pi/2
*/
	

	y_angle = GetFacingDirection( horz, vert );

	myobject.Move(temp_xpos, ypos, temp_zpos);
	myobject.Rotate(0.0f,  y_angle , 0.0f );
	myobject.UpdateAnimation((unsigned long)f1, TRUE);


	static bool bExplosion = false;
	static D3DXVECTOR3 vDirExplsn;

	if( m_gamepad.bXButtonPressed() )
	{
		D3DXMATRIX matY;
		D3DXMatrixRotationY(&matY, y_angle);
		D3DXVECTOR3 vPosition  = D3DXVECTOR3(xpos, ypos+67, zpos);
		D3DXVECTOR3 vDirection = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

		D3DXVec3TransformCoord(&vDirection, &vDirection, &matY );
		D3DXVec3Normalize(&vDirection, &vDirection);

		m_Bullet.Add( vPosition, vDirection, 60.0f );
		
		vDirection *= 50.0f;
		D3DXVec3Add(&vDirExplsn, &vPosition, &vDirection);
		bExplosion = true;
	}

	// very messy way of doing the explosion...but its only here to demonstrate taht
	// it works, and that it can be improved etc.
	static float cc = 0.0f;
	static float min_delay = 0.0f;
	if( bExplosion )
	{
		min_delay+=0.2f;
		if( min_delay > 1.0f ) // delay so the explosion doesnot start straignt away
		{
			cc += 0.14f;

			m_explosion.Scale(50.0f);
			m_explosion.Move(vDirExplsn.x, vDirExplsn.y, vDirExplsn.z);

			if(m_explosion.Update(cc)==true) // its finished a single cycle so reset
			{
				bExplosion = false;
				min_delay = 0.0f;
			}
			else
				m_explosion.Render();
		}
	}
	

	
	D3DXMATRIX matYYY;
	D3DXMatrixRotationY(&matYYY, y_angle);
	D3DXVECTOR3 vDirTempA = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	D3DXVec3TransformCoord(&vDirTempA, &vDirTempA, &matYYY );
	D3DXVec3Normalize(&vDirTempA, &vDirTempA);


	arrow(m_pD3DDevice, D3DXVECTOR3(xpos, 90.0f, zpos), 
		                vDirTempA);


	D3DXVECTOR3 vDirGravity = vDirTempA;
	m_OctreeWorld.UpdateHeight( &myobject.m_collision, &vDirGravity );
	ypos = vDirGravity.y;

	bCollision = m_OctreeWorld.CollisionCheck( &myobject.m_collision, &vDirTempA );
	
	if( !bCollision )
	{

		xpos += newDirection.x;
		zpos += newDirection.z;
		myobject.Move(xpos, ypos, zpos);
	}
	else
	{
		
		D3DXVECTOR3 vTemp;
		
		myobject.Move(xpos+vDirTempA.x, ypos, zpos+vDirTempA.z);

		if( ! m_OctreeWorld.CollisionCheck( &myobject.m_collision, &vTemp ) )
		{
			xpos += vDirTempA.x;
			zpos += vDirTempA.z;
		}
		else
		{
			myobject.Move(xpos, ypos, zpos);
		}
		
	}
	


	m_Light.Move(xpos, ypos+20, zpos);//zpos-80.0f);

	m_Bullet.Update(5.0f);
	m_Bullet.Render();
	
	// Uncomment to watch the sky change colour when there is a collision.
	bCollision = false;

    //myobject.Move(xpos, ypos, zpos);
	//myobject.Rotate(0.0f,  y_angle , 0.0f );
	//myobject.UpdateAnimation((unsigned long)f1, TRUE);
    myobject.Render();
		


	if( m_gamepad.bWhiteButtonPressed() && m_gamepad.iLeftTrigger() && m_gamepad.iRightTrigger() )
	{
		screenshot( m_pD3DDevice, "D:\\demo.bmp" );
	}




	////////////////////////////////////////////////////////////////////////////////////////
	//<CAMERA>// -start-
	// These few lines of code..upto the -end- is for the camera movement....the angle can be
	// compensated with so it move gradually to allign up with the back of the character as he
	// moves around later on.
	
	// Camera movement
	if( m_gamepad.iAnalogStickLeft() )
		camera_y_angle += 0.1f;
	if( m_gamepad.iAnalogStickRight() )
		camera_y_angle -= 0.1f;

	if( m_gamepad.iAnalogStickUp() )
		if(camera_x_angle > -1.0f)
			camera_x_angle -= 0.1f;
	if( m_gamepad.iAnalogStickDown() )
		if(camera_x_angle < 0.0f)
			camera_x_angle += 0.1f;

	
	//m_camera.Move(xpos, 30+ypos, -200 + zpos);
	//m_camera.SetPoint(0, 30, -200, xpos, ypos, zpos);
	m_camera.NewPos( xpos, ypos, zpos, camera_x_angle, camera_y_angle, 0.0f);
	m_camera.SetPoint(0.0f, 70.0f, -250,   xpos, ypos+70, zpos);
	m_camera.UpdatePos(0.01f);

	//<CAMERA>// -end-
	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\



	// End the scene
	m_pD3DDevice->EndScene();
	// Filp the back and front buffers so that whatever has been rendered on the 
	// back buffer will now be visible on screen (front buffer).
	m_pD3DDevice->Present(NULL, NULL, NULL, NULL);

	// Return 1 for the game to continue, 0 to terminate.
	return 1;
}

// Cleanup process, all done in our destructor
CGame::~CGame()
{
	m_g.Release();


	textA.Release();
	textB.Release();
	g_TextDebug.Release();

	m_Bullet.Release();

	m_OctreeWorld.Release();
}



/***************************************************************************/







