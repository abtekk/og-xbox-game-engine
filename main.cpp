/*                                                                     *
  *                                                                   *
    *                                                               *
      *                                                           *
	    *                                                       *
	      *                                                   *
		    *                  ENTRY POINT                  *
		      *                                           *
			    *                                       *
			      *                                   *
				    *                               *
				      *                           *
					    *                       *
					      *                   *
						    *               *
						      *           *
							    *       *
							      *   *
								    */
/***************************************************************************/
/*                                                                         */
/*  main.cpp                                                               */
/*                                                                         */
/***************************************************************************/


#include "main.h"

// Lets include our debug!
#include "debug.h"

void main()
{
	    /*
		-1- Initilize Any Main Functions
		*/
		CGame *pGame = new CGame();
		


		/*
		-2- Go InTo GameLoop
		*/
		while(true)
		{
			// Continue calling GameLoop until it returns zero.
			if( pGame->GameLoop() == 0 )
				break;
		}
		

		/*
		-3- Clean Up Anything And Say Goodbye :)
		*/
		delete pGame;
}